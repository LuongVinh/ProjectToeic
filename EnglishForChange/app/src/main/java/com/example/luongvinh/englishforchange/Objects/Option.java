package com.example.luongvinh.englishforchange.Objects;

/**
 * Created by LUONGVINH on 4/4/2017.
 */
public class Option {
    String name_option;

    public Option(String name_option) {
        this.name_option = name_option;
    }

    public String getName_option() {
        return name_option;
    }

    public void setName_option(String name_option) {
        this.name_option = name_option;
    }
}
