package com.example.luongvinh.englishforchange.Objects;

/**
 * Created by LUONGVINH on 4/4/2017.
 */
public class Part {
    private int icon;
    private String name_part,detail_part,type_part;

    public Part(int icon, String name_part, String detail_part, String type_part) {
        this.icon = icon;
        this.name_part = name_part;
        this.detail_part = detail_part;
        this.type_part = type_part;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public String getName_part() {
        return name_part;
    }

    public void setName_part(String name_part) {
        this.name_part = name_part;
    }

    public String getDetail_part() {
        return detail_part;
    }

    public void setDetail_part(String detail_part) {
        this.detail_part = detail_part;
    }

    public String getType_part() {
        return type_part;
    }

    public void setType_part(String type_part) {
        this.type_part = type_part;
    }
}
