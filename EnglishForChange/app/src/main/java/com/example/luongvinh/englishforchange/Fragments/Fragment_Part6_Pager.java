package com.example.luongvinh.englishforchange.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.luongvinh.englishforchange.Activities.Content_Part_Activity;
import com.example.luongvinh.englishforchange.Adapters.ViewPagerPart1Adapter;
import com.example.luongvinh.englishforchange.Objects.Question_Part6;
import com.example.luongvinh.englishforchange.R;
import com.example.luongvinh.englishforchange.Until.Constan;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

/**
 * Created by LUONGVINH on 5/20/2017.
 */

public class Fragment_Part6_Pager extends Fragment {
    private ViewPager pager;
    private FirebaseDatabase firebaseDatabase;
    private View root;
    private Fragment_Part1_Pager.AnswerChoise answerChoise;
    public int position, type;
    private Context mContext;
    private ViewPagerPart1Adapter pagerAdapter;
    private ArrayList<String> arr_answer_key;
    private DatabaseReference myDATA;
    private ArrayList<Question_Part6> arrayList;
    private Boolean full = false;

    public static Fragment_Part6_Pager newInstance(int position, int type) {
        Fragment_Part6_Pager fragment = new Fragment_Part6_Pager();
        Bundle bundle = new Bundle();
        bundle.putInt(Constan.POSITION_OF_TEST, position);
        bundle.putInt(Constan.TYPE, type);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getContext();
        arrayList = new ArrayList<>();
        arr_answer_key = new ArrayList<>();
        position = getArguments().getInt(Constan.POSITION_OF_TEST);
        type = getArguments().getInt(Constan.TYPE);
        full = ((Content_Part_Activity) mContext).getFull();

    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (root == null) {
            root = LayoutInflater.from(mContext).inflate(R.layout.fragment_part6_pager, container, false);
            pager = (ViewPager) root.findViewById(R.id.view_pager);
            getDataFromServer();
            setTextTitleBar(0);

        }

        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {


                if (full == true) {
                    answerChoise.setAnswerChoise(140 + position);
                    setTextTitleBar(position );
                } else {
                    answerChoise.setAnswerChoise(position);
                    setTextTitleBar(position );
                }


            }


            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        return root;

    }

    private void getDataFromServer() {
        firebaseDatabase = FirebaseDatabase.getInstance();
        myDATA = firebaseDatabase.getReference("Easy/1");
        myDATA.child("Part6").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Question_Part6 question = null;
                for (DataSnapshot data : dataSnapshot.getChildren()) {
                    question = data.getValue(Question_Part6.class);
                    arrayList.add(question);
                }
                Log.e("vinh5", arrayList.size() + "");
                int i;
                for (i = 0; i < 12; i++) {
                    Question_Part6 q = arrayList.get(i);
                    String a = q.getAnswer();
                    arr_answer_key.add(a);

                }


                //gán arraylist vừa tải về cho arraylist của activity để có thể sử dụng cho các fragment khác cùng activity
                ((Content_Part_Activity) mContext).setListQuestionPart6(arrayList);

                getDataByCache(type);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void getDataByCache(int type) {
        pagerAdapter = new ViewPagerPart1Adapter(getChildFragmentManager(), 1, type, null);
        pager.setAdapter(pagerAdapter);
    }

    private void setTextTitleBar(int current) {
        if(full==false){
            ((Content_Part_Activity) mContext).setTextTitleBar(current+1 + "/12");

        }else {
            ((Content_Part_Activity) mContext).setTextTitleBar(current+141 + "/200");

        }
    }


    public void setAnswerChoise(Fragment_Part1_Pager.AnswerChoise answerChoise) {
        this.answerChoise = answerChoise;
    }


}
