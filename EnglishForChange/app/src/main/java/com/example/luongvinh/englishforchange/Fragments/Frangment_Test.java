package com.example.luongvinh.englishforchange.Fragments;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.luongvinh.englishforchange.Activities.Content_Part_Activity;
import com.example.luongvinh.englishforchange.Activities.MainActivity;
import com.example.luongvinh.englishforchange.Adapters.ListTest_Adapter;
import com.example.luongvinh.englishforchange.Objects.Data_Point_Test;
import com.example.luongvinh.englishforchange.Objects.Test;
import com.example.luongvinh.englishforchange.R;
import com.example.luongvinh.englishforchange.Until.Constan;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by LUONGVINH on 4/4/2017.
 */
public class Frangment_Test extends Fragment {
    private ListTest_Adapter adapter;
    private ListView lv_test;
    private ArrayList<Test> testArrayList;
    private ArrayList<Data_Point_Test> arr_Data_point_tests;
    private int pos = 0, point_listening = 0, point_reading = 0, level=1;
    private Dialog dialog;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        arr_Data_point_tests = new ArrayList<>();

    }

    @Override
    public void onResume() {
        super.onResume();
        ((MainActivity) getContext()).setTitleActionBar("Thi Toeic");
        /*SharedPreferences pre = getContext().getSharedPreferences("my_data", MODE_PRIVATE);
        level = pre.getInt("LEVEL", 1);*/
        testArrayList.clear();
        addData();
        setChangeResultTest();

    }

    private void setChangeResultTest() {

        SharedPreferences pre = getContext().getSharedPreferences("my_data", MODE_PRIVATE);
        String jArray = pre.getString("data_point_test", null);
        if (jArray != null) {
            Type type1 = new TypeToken<ArrayList<Data_Point_Test>>() {
            }.getType();
            Gson gson = new Gson();
            arr_Data_point_tests = gson.fromJson(jArray, type1);
            for (int j = 0; j < arr_Data_point_tests.size(); j++) {
                Data_Point_Test p = arr_Data_point_tests.get(j);
                int mylevel = p.getLevel();
                if (mylevel == level) {
                    pos = p.getPos();
                    point_listening = parsePoint(p.getPoint_listening());
                    point_reading = parsePoint(p.getPoint_reading());
                    Test t = new Test("Test" + (pos + 1), point_listening + "", p.getPoint_listening() + "", point_reading + "", p.getPoint_reading() + "", point_listening + point_reading + "");
                    testArrayList.remove(pos);
                    testArrayList.add(pos, t);
                }

            }
            adapter = new ListTest_Adapter(getActivity(), R.layout.item_list_test, testArrayList);
            lv_test.setAdapter(adapter);
        }
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_test, container, false);
        ((MainActivity)getContext()).initActionbar(false);
        //((MainActivity)getContext()).ChooseLevel();
        ((MainActivity)getContext()).tvLever.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog = new Dialog(getContext());
                dialog.setContentView(R.layout.dialog_level);
                dialog.setTitle("Chọn Level");

                TextView tv_easy = (TextView) dialog.findViewById(R.id.tv_easy);
                TextView tv_normal = (TextView) dialog.findViewById(R.id.tv_normal);
                TextView tv_hard = (TextView) dialog.findViewById(R.id.tv_hard);
                // khai báo control trong dialog để bắt sự kiện
                tv_easy.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        level=1;
                        testArrayList.clear();
                        addData();
                        setChangeResultTest();
                        dialog.dismiss();
                    }
                });
                tv_normal.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        level=2;
                        testArrayList.clear();
                        addData();
                        setChangeResultTest();
                        dialog.dismiss();
                    }
                });
                tv_hard.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        level=3;
                        testArrayList.clear();
                        addData();
                        setChangeResultTest();
                        dialog.dismiss();
                    }
                });

                // bắt sự kiện cho nút đăng kí
                dialog.show();
                // hiển thị dialog
            }
        });
        lv_test = (ListView) view.findViewById(R.id.lv_test);
        testArrayList = new ArrayList<>();
        addData();
        adapter = new ListTest_Adapter(getActivity(), R.layout.item_list_test, testArrayList);
        lv_test.setAdapter(adapter);
        lv_test.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getContext(), Content_Part_Activity.class);
                Bundle bundle = new Bundle();
                bundle.putInt(Constan.POSITION_OF_TEST, position);
                bundle.putInt(Constan.TYPE, 1);
                bundle.putInt(Constan.LEVEL, level);
                bundle.putBoolean(Constan.FULL, true);
                intent.putExtra("DATA", bundle);
                startActivity(intent);
            }
        });
    //    addEvent();


        return view;

    }




    private int parsePoint(int point) {
        int p = 0;
        if (point < 100) {
            p = point * 5;
        } else if (point == 100) {
            p = 495;
        }
        return p;
    }

    private void addData() {
        testArrayList.clear();
        Test t1 = new Test("Test1", "0", "0", "0", "0", "0");
        Test t2 = new Test("Test2", "0", "0", "0", "0", "0");
        Test t3 = new Test("Test3", "0", "0", "0", "0", "0");
        Test t4 = new Test("Test4", "0", "0", "0", "0", "0");
        Test t5 = new Test("Test5", "0", "0", "0", "0", "0");
        Test t6 = new Test("Test6", "0", "0", "0", "0", "0");
        Test t7 = new Test("Test7", "0", "0", "0", "0", "0");
        Test t8 = new Test("Test8", "0", "0", "0", "0", "0");
        testArrayList.add(t1);
        testArrayList.add(t2);
        testArrayList.add(t3);
        testArrayList.add(t4);
        testArrayList.add(t5);
        testArrayList.add(t6);
        testArrayList.add(t7);
        testArrayList.add(t8);
    }

}
