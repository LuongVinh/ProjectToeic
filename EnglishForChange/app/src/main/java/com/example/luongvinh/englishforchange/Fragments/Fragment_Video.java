package com.example.luongvinh.englishforchange.Fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.luongvinh.englishforchange.Activities.MainActivity;
import com.example.luongvinh.englishforchange.R;
import com.example.luongvinh.englishforchange.controller.PlayYoutubeServiceControl;
import com.example.luongvinh.englishforchange.controller.YouTubeUriExtractor;
import com.example.luongvinh.englishforchange.controller.YtFile;
import com.example.luongvinh.englishforchange.dataVideo.adapter.VideoYoutubeAdapter;
import com.example.luongvinh.englishforchange.dataVideo.model.Item;
import com.example.luongvinh.englishforchange.dataVideo.model.ListVideoYoutube;
import com.example.luongvinh.englishforchange.dataVideo.remote.ApiUtils;
import com.example.luongvinh.englishforchange.dataVideo.remote.SOService;
import com.google.gson.Gson;

import java.net.URLEncoder;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by LUONGVINH on 4/4/2017.
 */
public class Fragment_Video extends Fragment {
    private RecyclerView recyclerView;
    private VideoYoutubeAdapter Yadapter;
    private SOService mService;
    private Intent serviceIntent;
    private Context mContext;
    private Toolbar toolbar;
    private ImageView imgBack, imv_clock;
    private TextView tv_part, tv_timer;
    private ArrayList<Item> arrListVideo =new ArrayList<>();
    private String nextPageToken="";
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_video, container, false);
        recyclerView=(RecyclerView)view.findViewById(R.id.re_youtube);
        RecyclerView.LayoutManager layoutManager=new LinearLayoutManager(mContext);
        ((MainActivity)mContext).initActionbar(true);
        recyclerView.setLayoutManager(layoutManager);

        Yadapter=new VideoYoutubeAdapter(recyclerView,mContext, new ArrayList<Item>(0), new VideoYoutubeAdapter.StreamItemListener() {
            @Override
            public void onPostClick(String videoId, int position, final String titleVideo) {
                Toast.makeText(mContext,"ID video: "+position,Toast.LENGTH_LONG).show();


                final int p=position;
                serviceIntent = new Intent(mContext, PlayYoutubeServiceControl.class);
                serviceIntent.setAction(PlayYoutubeServiceControl.ACTION_URL);
                serviceIntent.putExtra("position", p);
                mContext.startService(serviceIntent);


                String youtubeLink = "http://youtube.com/watch?v=" + videoId;

                YouTubeUriExtractor ytEx = new YouTubeUriExtractor(mContext) {

                    @Override
                    public void onUrisAvailable(String videoId, String videoTitle, SparseArray<YtFile> ytFiles) {
                        // TODO Auto-generated method stub
                        if (ytFiles != null) {
                            String downloadUrl = ytFiles.get(18).getUrl();
                            if (downloadUrl != null && downloadUrl.length() > 0) {
                                serviceIntent = new Intent(mContext, PlayYoutubeServiceControl.class);
                                serviceIntent.setAction(PlayYoutubeServiceControl.ACTION_PLAY);
                                serviceIntent.putExtra("url", downloadUrl);
                                serviceIntent.putExtra("TITLE", titleVideo);
                                serviceIntent.putExtra("data", new Gson().toJson(arrListVideo));

//                                serviceIntent.putParcelableArrayListExtra("data", (ArrayList<? extends Parcelable>) arrListVideo);
                                mContext.startService(serviceIntent);
                            }else {
                                downloadUrl = ytFiles.get(22).getUrl();
                                if (downloadUrl != null && downloadUrl.length() > 0) {
                                    serviceIntent = new Intent(mContext, PlayYoutubeServiceControl.class);
                                    serviceIntent.setAction(PlayYoutubeServiceControl.ACTION_PLAY);
                                    serviceIntent.putExtra("url", downloadUrl);
                                    serviceIntent.putExtra("TITLE", titleVideo);
                                    serviceIntent.putExtra("data", new Gson().toJson(arrListVideo));
//                                    serviceIntent.putParcelableArrayListExtra("data", (ArrayList<? extends Parcelable>) arrListVideo);
                                    mContext.startService(serviceIntent);
                                }
                            }
                        }
                    }
                };

                ytEx.execute(youtubeLink);
            }
        });

        recyclerView.setAdapter(Yadapter);
        recyclerView.setHasFixedSize(true);
        RecyclerView.ItemDecoration itemDecoration = new DividerItemDecoration(mContext, DividerItemDecoration.VERTICAL);
        recyclerView.addItemDecoration(itemDecoration);

        Yadapter.setOnLoadMoreListener(new VideoYoutubeAdapter.OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                if (arrListVideo.size() <= 50) {
                    arrListVideo.add(null);
                    Yadapter.notifyItemInserted(arrListVideo.size() - 1);
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            arrListVideo.remove(arrListVideo.size() - 1);
                            Yadapter.notifyItemRemoved(arrListVideo.size());
                            getDataByServer();


//                            Yadapter.notifyDataSetChanged();
                            Yadapter.setLoaded();
                        }
                    }, 400);
                }
            }
        });
        getDataByServer();



        return view;

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mService= ApiUtils.getSOService();
        mContext=getContext();

    }

    @Override
    public void onResume() {
        super.onResume();
        ((MainActivity)getContext()).setTitleActionBar("Video Toeic");
    }

    private void getDataByServer() {
        String location = "ToeicOnTHi";
        try {
            location = URLEncoder.encode(location, "UTF-8");
        } catch (Exception e) {
            // TODO: handle exception
        }
        mService.getAllvideos(location,nextPageToken).enqueue(new Callback<ListVideoYoutube>() {
            @Override
            public void onResponse(Call<ListVideoYoutube> call, Response<ListVideoYoutube> response) {
                if(response.isSuccessful()){
                    nextPageToken=response.body().getNextPageToken();
                    arrListVideo.addAll( response.body().getItems());
                    Yadapter.updateAnswers(response.body().getItems());


                }
            }

            @Override
            public void onFailure(Call<ListVideoYoutube> call, Throwable t) {

                Toast.makeText(mContext,t.toString(),Toast.LENGTH_LONG).show();

            }
        });
    }
}
