package com.example.luongvinh.englishforchange.controller;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.PixelFormat;
import android.graphics.Point;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.support.v7.app.NotificationCompat.Builder;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.SparseArray;
import android.view.Display;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout.LayoutParams;
import android.widget.RelativeLayout;
import android.widget.RemoteViews;
import android.widget.Toast;

import com.example.luongvinh.englishforchange.Activities.MainActivity;
import com.example.luongvinh.englishforchange.R;
import com.example.luongvinh.englishforchange.dataVideo.adapter.VideoYoutubeAdapter;
import com.example.luongvinh.englishforchange.dataVideo.model.Item;
import com.example.luongvinh.englishforchange.dataVideo.model.ListVideoYoutube;
import com.example.luongvinh.englishforchange.dataVideo.remote.ApiUtils;
import com.example.luongvinh.englishforchange.dataVideo.remote.SOService;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.io.IOException;
import java.lang.reflect.Type;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PlayYoutubeServiceControl extends Service implements SurfaceHolder.Callback,
        MediaPlayer.OnPreparedListener, VideoControllerView.MediaPlayerControl, OnCompletionListener {
    public static final String ACTION_PLAY = "com.example.luongvinh.englishforchange.controller.action.PLAY";

    public static final String ACTION_URL = "com.example.luongvinh.englishforchange.controller.action.URL";

    private static WindowManager windowManager;
    public static WindowManager.LayoutParams params;
    private static View popupView;
    public static View rlt_top;

    private static RelativeLayout rltLoading;
    private static RelativeLayout rltLoading_list_video;
    // private ProgressBar progressBar;
    private static final int SWIPE_MIN_DISTANCE = 120;
    private static final int SWIPE_MAX_OFF_PATH = 250;
    private static final int SWIPE_THRESHOLD_VELOCITY = 200;
    private GestureDetector gestureDetector;
    private OnTouchListener gestureListener;
    private Animation swipeRight;
    private SurfaceView videoSurface;
    private static MediaPlayer player;
    public static boolean isLoading = false;
    static View fm_play_video;
    static View fm_list_video;

    private static RecyclerView lv_videofill;
    static VideoControllerView controller;
    private static List<Item> arr;
    static VideoYoutubeAdapter adapter;
    static Context context;
    static String title;
    static String url;
    public static boolean IS_SERVICE_RUNNING = false;

    private static   ArrayList<Item> lstVideos;
    private static   int startIndex;
    private DisplayImageOptions options;
    private ImageLoader imageLoader;
    private Notification notification;
    private PendingIntent pendingIntent;
    private PendingIntent pRemoveIntent;
    private RemoteViews views;
    static Boolean checkFull = false;
    static Boolean loaded = false;
    static int x_Back = 0;
    static int y_back = 0;
    static LayoutParams lp;
    // receiver
    private HomeWatcher mHomeWatcher;
    private BroadcastReceiver mScreenReceiver;
    private boolean isFull = false;
    //delete popup
    private WindowManager.LayoutParams params_delete;
    private View delete_popup;
    private int viewWidth_video;
    private int viewHeight_video;
    private int viewWidth_img;
    private int viewHeight_img;
    private static String nextPageToken="";


    public static class FillVideo {

        private SOService mService;

        public FillVideo() {
            mService = ApiUtils.getSOService();
        }

        public Boolean getCheck() {
            return checkFull;
        }

        public Boolean getCheckload() {
            return loaded;
        }

        public void backSize() {
            if (checkFull == true) {
                // WindowManager.LayoutParams params = getFill();
                fm_list_video.setVisibility(View.GONE);

                // LinearLayout.LayoutParams fl = new
                // LinearLayout.LayoutParams((int) convertDpToPixel(300,
                // context), (int) convertDpToPixel(115, context));
                fm_play_video.setLayoutParams(lp);

                if (params.x == 0 && params.y == 0) {
                    params.x = 15;
                    params.y = 100;
                } else {
                    params.x = x_Back;
                    params.y = y_back;
                }

                windowManager.updateViewLayout(popupView, params);

                checkFull = false;
                loaded = true;
                controller.update_image_Full();
                controller.showSeekbar();
                params.flags = WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE;
                windowManager.updateViewLayout(popupView, params);
            }
        }

        private void setupRecyclerView() {

            arr = new ArrayList<>();
            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);

            lv_videofill.setLayoutManager(layoutManager);
            adapter = new VideoYoutubeAdapter(lv_videofill,context, arr, new VideoYoutubeAdapter.StreamItemListener() {
                @Override
                public void onPostClick(String idVideo, int position, String titleVideo) {
                    lstVideos.clear();
                    lstVideos.addAll(arr);
                    startIndex = position;
//				rltLoading.setVisibility(View.VISIBLE);
                    Item v = arr.get(position);
                    // loaded = false;

//				if (player != null) {
//					if (player.isPlaying())
//						player.stop();
//				}

                    if (lstVideos.size() > 0) {
                        playVideo(v);
                    }
                }
            });



            lv_videofill.setAdapter(adapter);
            lv_videofill.setHasFixedSize(true);
            RecyclerView.ItemDecoration itemDecoration = new DividerItemDecoration(context, DividerItemDecoration.VERTICAL);
            lv_videofill.addItemDecoration(itemDecoration);
            adapter.setOnLoadMoreListener(new VideoYoutubeAdapter.OnLoadMoreListener() {
                @Override
                public void onLoadMore() {

                    if (arr.size() <= 50) {
                        arr.add(null);
                        adapter.notifyItemInserted(arr.size() - 1);
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                arr.remove(arr.size() - 1);
                                adapter.notifyItemRemoved(arr.size());
                                getDataByServer();


//                            Yadapter.notifyDataSetChanged();
                                adapter.setLoaded();
                            }
                        }, 5000);
                    }
                }
            });
        }

        private void getDataByServer() {

            setupRecyclerView();
            String location = "HocToeic";
            try {
                location = URLEncoder.encode(location, "UTF-8");
            } catch (Exception e) {
                // TODO: handle exception
            }
            mService.getAllvideos(location,nextPageToken).enqueue(new Callback<ListVideoYoutube>() {
                @Override
                public void onResponse(Call<ListVideoYoutube> call, Response<ListVideoYoutube> response) {
                    if (response.isSuccessful()) {
                        nextPageToken=response.body().getNextPageToken();
                        arr.addAll(response.body().getItems());

                        adapter.updateAnswers(response.body().getItems());
                        rltLoading_list_video.setVisibility(View.GONE);

                    } else Log.e("toanle", "" + response.code());
                }

                @Override
                public void onFailure(Call<ListVideoYoutube> call, Throwable t) {


                }
            });
        }

//		public static void getList() {
//			arr = new ArrayList<>();
//			adapter = new PopularAdapter(context, R.layout.item_popular, arr, null, true);
//			lv_videofill.setAdapter(adapter);
//			String location = null;
//			try {
//				location = URLEncoder.encode(title, "UTF-8");
//			} catch (Exception e) {
//				// TODO: handle exception
//			}
//
//			url = MyApplication.popular_charge.replace(MyApplication.single, location);
//			new PopularAsynctask().execute(url.replace(MyApplication.nextpage, ""));
//
//		}

        public static   int getScreenWidth() {
            return Resources.getSystem().getDisplayMetrics().widthPixels;
        }

        public static int getScreenHeigh() {
            return Resources.getSystem().getDisplayMetrics().heightPixels;
        }

        public  WindowManager.LayoutParams getFill() {

            return params;
        }

    }

//	static class PopularAsynctask extends AsyncTask<String, Void, ArrayList<VideoPopular>> {
//
//		ArrayList<VideoPopular> arr2 = new ArrayList<>();
//
//		@Override
//		protected ArrayList<VideoPopular> doInBackground(String... params) {
//			// TODO Auto-generated method stub
//			String json = ReadJson.getJson(params[0]);
//			arr2 = OpenJson.getList(json);
//			return arr2;
//		}
//
//		@Override
//		protected void onPostExecute(ArrayList<VideoPopular> result) {
//			// TODO Auto-generated method stub
//			super.onPostExecute(result);
//			adapter.addListItemToAdapter(result);
//			if (isLoading == true) {
//				isLoading = false;
//			}
//			rltLoading_list_video.setVisibility(View.GONE);
//		}
//
//	}

    class MyGestureDetector extends SimpleOnGestureListener implements OnTouchListener {

        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {

            try {
                if (Math.abs(e1.getY() - e2.getY()) > SWIPE_MAX_OFF_PATH)
                    return false;
                // right to left swipe
                if (e1.getX() - e2.getX() > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
                    Toast.makeText(getApplicationContext(), "Left Swipe", Toast.LENGTH_SHORT).show();
                } else if (e2.getX() - e1.getX() > SWIPE_MIN_DISTANCE
                        && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
                    Toast.makeText(getApplicationContext(), "Right Swipe", Toast.LENGTH_SHORT).show();
                    swipeRight = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.swipe_right);
                    popupView.startAnimation(swipeRight);
                    stopSelf();

                }
            } catch (Exception e) {
                // nothing
            }
            return false;
        }

        public boolean onDown(MotionEvent e) {
            return true;
        }

        @Override
        public boolean onTouch(View arg0, MotionEvent arg1) {
            // TODO Auto-generated method stub
            return false;
        }

    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // TODO Auto-generated method stub
        String action = intent.getAction();
        if (action != null && action.length() > 0) {
            if (intent.getAction().equals(ACTION_URL)) {
                startIndex = intent.getIntExtra("position", 0);
                rltLoading.setVisibility(View.VISIBLE);
                this.loaded = false;

                if (player != null) {
                    if (player.isPlaying())
                        player.stop();
                }
            } else if (intent.getAction().equals(ACTION_PLAY)) {

                final String url = intent.getStringExtra("url");
                Type type = new TypeToken<ArrayList<Item>>() {
                }.getType();
                lstVideos = new Gson().fromJson(intent.getStringExtra("data"), type);
                title = intent.getStringExtra("TITLE");

                if (url != null) {
                    playVideo(Uri.parse(url));
                }
            } else if (intent.getAction().equals(Constants.ACTION.FULL_LIST_ACTION)) {
                full_list_Video(loaded);
            } else if (intent.getAction().equals(Constants.ACTION.STOPFOREGROUND_ACTION)) {
                // fm_list_video.setVisibility(View.VISIBLE);
                // stopForeground(true);
                stopSelf();
            } else if (intent.getAction().equals(Constants.ACTION.BACK_ACTION)) {
                new FillVideo().backSize();

				/*
                 * Intent i = new Intent(); i.setAction(Intent.ACTION_MAIN);
				 * i.addCategory(Intent.CATEGORY_LAUNCHER);
				 * i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK |
				 * Intent.FLAG_ACTIVITY_LAUNCHED_FROM_HISTORY); i.setComponent(
				 * new ComponentName(getApplicationContext().getPackageName(),
				 * MainActivity.class.getName())); startActivity(i);
				 */
                //
            }
        }
        return START_STICKY;
    }

    @SuppressLint("NewApi")
    private void showNotification() {
        // TODO Auto-generated method stub
        String url = lstVideos.get(startIndex).getSnippet().getThumbnails().getHigh().getUrl();
        Bitmap icon = imageLoader.loadImageSync(url);
        views = new RemoteViews(getPackageName(), R.layout.custom_notification);
        views.setImageViewBitmap(R.id.img_notifi_left, icon);
        views.setTextViewText(R.id.txt_title_notifi, lstVideos.get(startIndex).getSnippet().getTitle());

        Intent notificationIntent = new Intent(this, MainActivity.class);
        notificationIntent.setAction(Constants.ACTION.MAIN_ACTION);
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);

        Intent notificationRemove = new Intent(this, PlayYoutubeServiceControl.class);
        notificationRemove.setAction(Constants.ACTION.STOPFOREGROUND_ACTION);
        pRemoveIntent = PendingIntent.getService(this, 0, notificationRemove, 0);
        views.setOnClickPendingIntent(R.id.img_exit_notifi_right, pRemoveIntent);
        views.setOnClickPendingIntent(R.id.img_notifi_left, pendingIntent);
        views.setOnClickPendingIntent(R.id.txt_title_notifi, pendingIntent);

        Notification notification = new Notification.Builder(this).build();
        notification.contentView = views;
        notification.icon = R.mipmap.ic_launcher;
        notification.contentIntent = pendingIntent;
        startForeground(Constants.NOTIFICATION_ID.FOREGROUND_SERVICE, notification);
    }

    @SuppressWarnings("deprecation")
    @SuppressLint("NewApi")
    private void UpdateNotification(String title, String imgUrl, RemoteViews views) {
        // The PendingIntent to launch our activity if the user selects
        // this notification

        Bitmap icon = getBitmap(imgUrl);
        Bitmap finalBitmap = null;
        if (icon.getWidth() >= icon.getHeight()) {

            finalBitmap = Bitmap.createBitmap(icon, icon.getWidth() / 2 - icon.getHeight() / 2, 0, icon.getHeight(),
                    icon.getHeight());

        } else {

            finalBitmap = Bitmap.createBitmap(icon, 0, icon.getHeight() / 2 - icon.getWidth() / 2, icon.getWidth(),
                    icon.getWidth());

        }
        Builder builder = (Builder) new Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher).setContentIntent(pendingIntent).setContent(views).setAutoCancel(true);
        views.setImageViewBitmap(R.id.img_notifi_left, finalBitmap);
        views.setTextViewText(R.id.txt_title_notifi, title);
        NotificationManager notificationmanager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        // Build Notification with Notification Manager
        notificationmanager.notify(Constants.NOTIFICATION_ID.FOREGROUND_SERVICE, builder.build());

        // .setOngoing(true)
        // .addAction(android.R.drawable.ic_media_previous, "Previous",
        // ppreviousIntent)
        // .addAction(android.R.drawable.ic_media_play, "Play", pplayIntent)
        // .addAction(android.R.drawable.ic_media_next, "Next", pnextIntent)
    }

    /**
     * This is the method that can be called to update the Notification
     */
    public Bitmap getBitmap(String j) {
        return imageLoader.loadImageSync(j, options);
    }

    public static void playVideo(Uri uri) {
        try {
            player.reset();
            player.setAudioStreamType(AudioManager.STREAM_MUSIC);
            player.setDataSource(context, uri);
            player.prepareAsync();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onCreate() {
        // TODO Auto-generated method stub
        super.onCreate();
        windowManager = (WindowManager) getSystemService(WINDOW_SERVICE);

//deleteVideo
        LayoutInflater layoutInflater2 = (LayoutInflater) getBaseContext().getSystemService(LAYOUT_INFLATER_SERVICE);
        delete_popup = layoutInflater2.inflate(R.layout.delete_popup, null);
        delete_popup.setVisibility(View.GONE);

        params_delete = new WindowManager.LayoutParams(WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.TYPE_PHONE,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE, PixelFormat.TRANSLUCENT);

        params_delete.gravity = Gravity.TOP | Gravity.LEFT;
        params_delete.x = Resources.getSystem().getDisplayMetrics().widthPixels;
        params_delete.y = Resources.getSystem().getDisplayMetrics().heightPixels;


        windowManager.addView(delete_popup, params_delete);
//set kich thuoc default cho video
        context = getApplicationContext();
        params = new WindowManager.LayoutParams(WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.TYPE_SYSTEM_ERROR,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
                        | WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD
                        | WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON
                        | WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON,
                PixelFormat.TRANSLUCENT);
        params.gravity = Gravity.TOP | Gravity.LEFT;
        params.x = 15;
        params.y = 100;

		/*
		 * Intent closeDialog = new Intent(Intent.ACTION_CLOSE_SYSTEM_DIALOGS);
		 * sendBroadcast(closeDialog);
		 */

        imageLoader = ImageLoader.getInstance();
        options = new DisplayImageOptions.Builder().cacheInMemory(true).cacheOnDisc(true).resetViewBeforeLoading(true)
                .showImageForEmptyUri(R.color.transparent).showImageOnFail(R.color.transparent).cacheInMemory(true)
                .cacheOnDisk(true).considerExifParams(true).showImageOnLoading(R.color.transparent).build();

        gestureDetector = new GestureDetector(this, new MyGestureDetector());
        gestureListener = new OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                return gestureDetector.onTouchEvent(event);
            }
        };
        // service dieu khien video next
        if (popupView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) getBaseContext().getSystemService(LAYOUT_INFLATER_SERVICE);
            popupView = layoutInflater.inflate(R.layout.activity_video_player, null);
            videoSurface = (SurfaceView) popupView.findViewById(R.id.videoSurface);
            rltLoading = (RelativeLayout) popupView.findViewById(R.id.rltLoading);
            rltLoading_list_video = (RelativeLayout) popupView.findViewById(R.id.rltLoading_list_video);
            // progressBar = (ProgressBar)
            // popupView.findViewById(R.id.progressBar2);
            lv_videofill = (RecyclerView) popupView.findViewById(R.id.lv_video);
            fm_play_video = popupView.findViewById(R.id.videoSurfaceContainer);
            fm_list_video = popupView.findViewById(R.id.fm_list_video);

            lp = (LayoutParams) fm_play_video.getLayoutParams();

            popupView.setKeepScreenOn(true);
            popupView.setOnTouchListener(gestureListener);
            windowManager.addView(popupView, params);

            // prepare Media Player
            SurfaceHolder videoHolder = videoSurface.getHolder();
            videoHolder.addCallback(this);
            player = new MediaPlayer();
            controller = new VideoControllerView(this, true);
            player.setOnPreparedListener(this);
            player.setOnCompletionListener(this);

            ViewTreeObserver view_videoSurface = videoSurface.getViewTreeObserver();
            if (view_videoSurface.isAlive()) {
                view_videoSurface.addOnGlobalLayoutListener(new OnGlobalLayoutListener() {

                    @SuppressLint("NewApi")
                    @Override
                    public void onGlobalLayout() {
                        // TODO Auto-generated method stub
                        videoSurface.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                        viewWidth_video = videoSurface.getWidth();
                        viewHeight_video = videoSurface.getHeight();
//						Log.e("Toan2", "" + viewHeight + "-" + viewWidth);
                    }
                });
            }
            final ImageView img = (ImageView) delete_popup.findViewById(R.id.img);
            ViewTreeObserver view_delete = img.getViewTreeObserver();
            if (view_delete.isAlive()) {
                view_delete.addOnGlobalLayoutListener(new OnGlobalLayoutListener() {

                    @SuppressLint("NewApi")
                    @Override
                    public void onGlobalLayout() {
                        // TODO Auto-generated method stub
                        img.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                        viewWidth_img = img.getWidth();
                        viewHeight_img = img.getHeight();
//						Log.e("Toan3", "" + viewHeight + "-" + viewWidth);
                    }
                });
            }


            controller.setPrevNextListeners(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    if (lstVideos != null && lstVideos.size() > 0) {
                        if (startIndex < lstVideos.size() - 1) {
                            startIndex++;
                            playVideo(lstVideos.get(startIndex));
                        } else {
                            playVideo(lstVideos.get(0));
                            startIndex = 0;
                        }
                    }
                    UpdateNotification(lstVideos.get(startIndex).getSnippet().getTitle(),
                            lstVideos.get(startIndex).getSnippet().getThumbnails().getHigh().getUrl(), views);
                }
            }, new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub

                    if (lstVideos != null && lstVideos.size() > 0) {
                        if (startIndex > 0) {
                            playVideo(lstVideos.get(startIndex - 1));
                            startIndex = startIndex - 1;
                        } else {
                            // play last song
                            playVideo(lstVideos.get(lstVideos.size() - 1));
                            startIndex = lstVideos.size() - 1;
                        }

                    }
                    UpdateNotification(lstVideos.get(startIndex).getSnippet().getTitle(),
                            lstVideos.get(startIndex).getSnippet().getThumbnails().getHigh().getUrl(), views);
                }
            });
        }

//		lv_videofill.setOnScrollListener(new OnScrollListener() {
//
//			@Override
//			public void onScrollStateChanged(AbsListView view, int scrollState) {
//				// TODO Auto-generated method stub
//
//			}
//
//			@Override
//			public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
//				// TODO Auto-generated method stub
//				if (view.getLastVisiblePosition() == totalItemCount - 1) {
//					if (lv_videofill.getCount() >= 10) {
//						if (isLoading == false) {
//
//							isLoading = true;
//
////							String s = getUrl(OpenJson.next.getNextPage());
////
////							new PopularAsynctask().execute(s);
//
//						}
//
//					}
//				}
//			}
//		});


//		lv_videofill.setOnItemClickListener(new OnItemClickListener() {
//
//			@Override
//			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//				// TODO Auto-generated method stub
//				lstVideos.clear();
//				lstVideos.addAll(arr);
//				startIndex = position;
////				rltLoading.setVisibility(View.VISIBLE);
//				Item v = arr.get(position);
//				// loaded = false;
//
////				if (player != null) {
////					if (player.isPlaying())
////						player.stop();
////				}
//
//				if (lstVideos.size() > 0) {
//					playVideo(v);
//				}
//
//			}
//		});
//xử lí sự kiện nhấn giữ video cũng như phân biệt click vs touch click
        popupView.setOnTouchListener(new OnTouchListener() {
            private int initialX;
            private int initialY;
            private float initialTouchX;
            private float initialTouchY;
            private static final int MAX_CLICK_DURATION = 200;
            private long startClickTime;
            private Boolean check = false;

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        initialX = params.x;
                        initialY = params.y;
                        initialTouchX = event.getRawX();
                        initialTouchY = event.getRawY();
                        startClickTime = Calendar.getInstance().getTimeInMillis();
                        delete_popup.setVisibility(View.VISIBLE);

                        // windowManager.addView(chatHead, params);
                        return true;
                    case MotionEvent.ACTION_UP:

                        long clickDuration = Calendar.getInstance().getTimeInMillis() - startClickTime;
                        if (clickDuration - startClickTime < 200) {
                            if (clickDuration < MAX_CLICK_DURATION) {
                                controller.show();

                            }

                        }
                        delete_popup.setVisibility(View.GONE);
                        Log.e("Toan", "" + y_back);
                        if (y_back + viewHeight_video > (Resources.getSystem().getDisplayMetrics().heightPixels) - viewHeight_img) {
//						Toast.makeText(getApplicationContext(), "OK", Toast.LENGTH_LONG).show();
                            stopSelf();
                            y_back = 0;
                        }


                        return true;
                    case MotionEvent.ACTION_MOVE:
                        x_Back = initialX + (int) (event.getRawX() - initialTouchX);
                        y_back = initialY + (int) (event.getRawY() - initialTouchY);

                        if (y_back + viewHeight_video >= (Resources.getSystem().getDisplayMetrics().heightPixels) - viewHeight_img) {
//						Toast.makeText(getApplicationContext(), "D đã tới", Toast.LENGTH_LONG).show();
//						delete.setBackground(getBaseContext().getResources().getColor(R.color.));
                            delete_popup.setBackgroundColor(getBaseContext().getResources().getColor(R.color.delete_video));
                        } else {
                            delete_popup.setBackgroundColor(getBaseContext().getResources().getColor(R.color.delete_video_default));


                        }

                        params.x = x_Back;
                        params.y = y_back;
                        windowManager.updateViewLayout(popupView, params);
                        return true;
                }
                return false;
            }
        });

        // start receiver
        mHomeWatcher = new HomeWatcher(getApplicationContext());
        mHomeWatcher.setOnHomePressedListener(new OnHomePressedListener() {
            @Override
            public void onHomePressed() {
                new FillVideo().backSize();
            }

            @Override
            public void onHomeLongPressed() {
                new FillVideo().backSize();
            }
        });
        mHomeWatcher.startWatch();

        IntentFilter filter = new IntentFilter(Intent.ACTION_SCREEN_ON);
        filter.addAction(Intent.ACTION_SCREEN_OFF);
        filter.addAction(Intent.ACTION_ANSWER);
        filter.addAction(Intent.ACTION_SCREEN_ON);
        filter.addAction(Intent.ACTION_USER_PRESENT);
        mScreenReceiver = new ScreenReceiver();
        registerReceiver(mScreenReceiver, filter);
    }
//show list video khi full màn hình
    public void full_list_Video(Boolean check) {
        FillVideo fill = null;
        if (check == false) {
            this.checkFull = true;
            this.loaded = true;

            controller.update_image_Full();
            controller.showSeekbar();
            rltLoading_list_video.setVisibility(View.VISIBLE);
            fill = new FillVideo();
            int y = fill.getScreenWidth();
            int x = fill.getScreenHeigh();
            int temp = y * 9 / 16;
            //xử lí để phù hợp vs toàn màn hình
            LayoutParams f2 = new LayoutParams(y, x - temp
                    - (int) Math.ceil(25 * getApplicationContext().getResources().getDisplayMetrics().density));
            fm_list_video.setLayoutParams(f2);
            params.x = 0;
            params.y = (int) Math.ceil(25 * getApplicationContext().getResources().getDisplayMetrics().density);
            LayoutParams fl = new LayoutParams(y, temp);
            fm_play_video.setLayoutParams(fl);
			fill.getDataByServer();
        } else if (check != false) {//co video
            // checkFull = true;
            this.checkFull = true;
            controller.update_image_Full();
            controller.showSeekbar();
            fm_list_video.setVisibility(View.VISIBLE);
            int y = fill.getScreenWidth();
            int x = fill.getScreenHeigh();
            int temp = y * 9 / 16;
            LayoutParams f2 = new LayoutParams(y, x - temp
                    - (int) Math.ceil(25 * getApplicationContext().getResources().getDisplayMetrics().density));
            fm_list_video.setLayoutParams(f2);
            params.x = 0;
            params.y = (int) Math.ceil(25 * getApplicationContext().getResources().getDisplayMetrics().density);
            LayoutParams fl = new LayoutParams(y, temp);
            fm_play_video.setLayoutParams(fl);
        }

        params.flags = WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN;
        windowManager.updateViewLayout(popupView, params);
    }

    public String getUrl(String nextPage) {
        // TODO Auto-generated method stub

        String url2 = null;
//		String url2 = url.replace(MyApplication.nextpage, nextPage);

        return url2;
    }
//dừng mọi hoạt động service
    @Override
    public void onDestroy() {
        super.onDestroy();
        if (popupView != null)
            windowManager.removeView(popupView);
        if (player != null) {
            try {
                player.stop();
                player.release();
            } catch (Exception e) {
                // TODO: handle exception
            }
        }
        stopForeground(true);
        stopSelf();
        mHomeWatcher.stopWatch();
        unregisterReceiver(mScreenReceiver);
        popupView = null;
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        player.setDisplay(holder);
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
    }

    @Override
    public void onPrepared(MediaPlayer mp) {
        controller.setMediaPlayer(this);
        controller.setAnchorView((FrameLayout) popupView.findViewById(R.id.videoSurfaceContainer));
        player.start();
        rltLoading.setVisibility(View.GONE);
        showNotification();

    }

    @Override
    public boolean canPause() {
        return true;
    }

    @Override
    public boolean canSeekBackward() {
        return true;
    }

    @Override
    public boolean canSeekForward() {
        return true;
    }

    @Override
    public int getBufferPercentage() {
        return 0;
    }

    @Override
    public int getCurrentPosition() {
        try {
            return player.getCurrentPosition();
        } catch (Exception e) {
            // TODO: handle exception
            return 0;
        }

    }

    @Override
    public int getDuration() {
        try {
            return player.getDuration();
        } catch (Exception e) {
            return 0;
        }
    }

    @Override
    public boolean isPlaying() {
        try {
            return player.isPlaying();
        } catch (Exception e) {
            return false;
        }

    }

    @Override
    public void pause() {
        try {
            player.pause();
        } catch (Exception e) {
            // TODO: handle exception
        }
    }

    @Override
    public void seekTo(int i) {
        try {
            player.seekTo(i);
        } catch (Exception e) {
            // TODO: handle exception
        }
    }

    @Override
    public void start() {
        player.start();
    }

    @Override
    public boolean isFullScreen() {
        return false;
    }

    @SuppressLint("NewApi")
    @Override
    public void toggleFullScreen() {
        // DisplayMetrics displayMetrics =
        // getApplicationContext().getResources().getDisplayMetrics();
        // int width = displayMetrics.widthPixels;
        // int height = displayMetrics.heightPixels;

        int measuredWidth = 0;
        int measuredHeight = 0;
        int z = (int) Math.ceil(25 * getApplicationContext().getResources().getDisplayMetrics().density);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            Point size = new Point();
            windowManager.getDefaultDisplay().getSize(size);
            measuredWidth = size.x;
            measuredHeight = size.y;
        } else {
            Display d = windowManager.getDefaultDisplay();
            measuredWidth = d.getWidth();
            measuredHeight = d.getHeight();
        }

        if (isFull == false) {

            fm_list_video.setVisibility(View.GONE);
            // videoSurface.getHolder().setFixedSize(measuredHeight,
            // measuredWidth);

            LayoutParams p = new LayoutParams(measuredHeight, measuredWidth);
            // p.setMargin(0,0,0,0);
            p.setMargins(0, 0, 0, 0);
            fm_play_video.setLayoutParams(p);

            params.screenOrientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE;
            windowManager.updateViewLayout(popupView, params);
            isFull = true;
        } else {
            // LinearLayout.LayoutParams p=new
            // LinearLayout.LayoutParams(250,150);
            // fm_play_video.setLayoutParams(lp);
            FillVideo full = new FillVideo();
            full.backSize();

            params.screenOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;
            windowManager.updateViewLayout(popupView, params);
            isFull = false;
        }

    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        // TODO Auto-generated method stub
        if (lstVideos != null && lstVideos.size() > 0) {
            if (startIndex < lstVideos.size() - 1) {
                startIndex++;
                playVideo(lstVideos.get(startIndex));
            } else {
                playVideo(lstVideos.get(0));
                startIndex = 0;
            }
        }
    }

    private static void playVideo(Item video) {
        // TODO Auto-generated method stub
        String youtubeLink = "http://youtube.com/watch?v=" + video.getId().getVideoId();

        rltLoading.setVisibility(View.VISIBLE);
        if (player != null) {
            if (player.isPlaying())
                player.stop();
        }
        YouTubeUriExtractor ytEx = new YouTubeUriExtractor(context) {

            @Override
            public void onUrisAvailable(String videoId, String videoTitle, SparseArray<YtFile> ytFiles) {
                // TODO Auto-generated method stub
                if (ytFiles != null) {
                    String downloadUrl = ytFiles.get(18).getUrl();
                    if (downloadUrl != null && downloadUrl.length() > 0) {
                        playVideo(Uri.parse(downloadUrl));
                    } else {
                        downloadUrl = ytFiles.get(22).getUrl();
                        if (downloadUrl != null && downloadUrl.length() > 0) {
                            playVideo(Uri.parse(downloadUrl));
                        } else {

                        }
                    }
                }
            }
        };

        ytEx.execute(youtubeLink);
    }

    //
}
