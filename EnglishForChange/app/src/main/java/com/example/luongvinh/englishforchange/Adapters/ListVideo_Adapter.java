package com.example.luongvinh.englishforchange.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.luongvinh.englishforchange.Objects.Video;
import com.example.luongvinh.englishforchange.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by LUONGVINH on 4/4/2017.
 */
public class ListVideo_Adapter extends ArrayAdapter<Video> {
    private ViewHolder holder;
    private ArrayList<Video> videoArrayList;
    private Context mcontext;

    public ListVideo_Adapter(Context context, int resource, List<Video> videoList) {
        super(context, resource, videoList);
        this.mcontext = context;
        this.videoArrayList = new ArrayList<>(videoList);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;

        holder = new ViewHolder();
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) mcontext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.item_list_video, null);

            holder.tv_num_video = (TextView) view.findViewById(R.id.tv_num_video);
            holder.imv_video = (ImageView) view.findViewById(R.id.imv_video);

            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();

        }
        Video video = videoArrayList.get(position);
        holder.tv_num_video.setText(video.getNum_video());
        holder.imv_video.setImageResource(video.getImv_video());

        return view;
    }

    static class ViewHolder {
        TextView tv_num_video;
        ImageView imv_video;

    }
}

