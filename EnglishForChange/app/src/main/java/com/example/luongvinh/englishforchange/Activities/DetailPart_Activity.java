package com.example.luongvinh.englishforchange.Activities;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.luongvinh.englishforchange.Objects.Data_Point_Part;
import com.example.luongvinh.englishforchange.R;
import com.example.luongvinh.englishforchange.Until.Constan;
import com.github.lzyzsd.circleprogress.ArcProgress;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

/**
 * Created by LUONGVINH on 4/14/2017.
 */
public class DetailPart_Activity extends AppCompatActivity {
    private Button btn_back;
    private TextView tv_title_detail_part, tv_timer;
    private Context mContext;
    private int type, pos = 0;
    private ArcProgress arcProgress1, arcProgress2, arcProgress3, arcProgress4, arcProgress5,
            arcProgress6, arcProgress7, arcProgress8, arcProgress9, arcProgress10;
    private Toolbar toolbar;
    private ImageView imgBack, imv_clock;
    private ArrayList<Data_Point_Part> arr__dataPointPart;
    private Dialog dialog;
    private int level=1;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detailpart_activity);
        arr__dataPointPart = new ArrayList<>();
        arcProgress1 = (ArcProgress) findViewById(R.id.arc_progress1);
        arcProgress2 = (ArcProgress) findViewById(R.id.arc_progress2);
        arcProgress3 = (ArcProgress) findViewById(R.id.arc_progress3);
        arcProgress4 = (ArcProgress) findViewById(R.id.arc_progress4);
        arcProgress5 = (ArcProgress) findViewById(R.id.arc_progress5);
        arcProgress6 = (ArcProgress) findViewById(R.id.arc_progress6);
        arcProgress7 = (ArcProgress) findViewById(R.id.arc_progress7);
        arcProgress8 = (ArcProgress) findViewById(R.id.arc_progress8);
        arcProgress9 = (ArcProgress) findViewById(R.id.arc_progress9);
        arcProgress10 = (ArcProgress) findViewById(R.id.arc_progress10);
        mContext = getBaseContext();
        initActionbar();
        Intent intent = getIntent();
        if (intent != null) {
            Bundle bundle = intent.getBundleExtra("DATA");
            type = bundle.getInt(Constan.TYPE);
        }
        setEvent();


        switch (type) {
            case 1:
                tv_title_detail_part.setText("Luyện Thi Part 1");
                break;
            case 2:
                tv_title_detail_part.setText("Luyện Thi Part 2");
                break;
            case 3:
                tv_title_detail_part.setText("Luyện Thi Part 3");
                break;
            case 4:
                tv_title_detail_part.setText("Luyện Thi Part 4");
                break;
            case 5:
                tv_title_detail_part.setText("Luyện Thi Part 5");
                break;
            case 6:
                tv_title_detail_part.setText("Luyện Thi Part 6");
                break;
            case 7:
                tv_title_detail_part.setText("Luyện Thi Part 7");
                break;

        }

    }

    private void setEvent() {

        arcProgress1.setOnClickListener(menuButtonListener);
        arcProgress2.setOnClickListener(menuButtonListener);
        arcProgress3.setOnClickListener(menuButtonListener);
        arcProgress4.setOnClickListener(menuButtonListener);
        arcProgress5.setOnClickListener(menuButtonListener);
        arcProgress6.setOnClickListener(menuButtonListener);
        arcProgress7.setOnClickListener(menuButtonListener);
        arcProgress8.setOnClickListener(menuButtonListener);
        arcProgress9.setOnClickListener(menuButtonListener);
        arcProgress10.setOnClickListener(menuButtonListener);
    }

    private View.OnClickListener menuButtonListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.arc_progress1:
                    Intent i1 = new Intent(DetailPart_Activity.this, Content_Part_Activity.class);
                    Bundle bundle1 = new Bundle();
                    bundle1.putInt(Constan.POSITION_OF_TEST, 1);
                    bundle1.putInt(Constan.TYPE, type);
                    bundle1.putInt(Constan.LEVEL,level);
                    i1.putExtra("DATA", bundle1);
                    startActivity(i1);

                    break;
                case R.id.arc_progress2:
                    Intent i2 = new Intent(DetailPart_Activity.this, Content_Part_Activity.class);
                    Bundle bundle2 = new Bundle();
                    bundle2.putInt(Constan.POSITION_OF_TEST, 2);
                    bundle2.putInt(Constan.TYPE, type);
                    bundle2.putInt(Constan.LEVEL,level);
                    i2.putExtra("DATA", bundle2);
                    startActivity(i2);
                    break;
                case R.id.arc_progress3:
                    Intent i3 = new Intent(DetailPart_Activity.this, Content_Part_Activity.class);
                    Bundle bundle3 = new Bundle();
                    bundle3.putInt(Constan.POSITION_OF_TEST, 3);
                    bundle3.putInt(Constan.TYPE, type);
                    bundle3.putInt(Constan.LEVEL,level);
                    i3.putExtra("DATA", bundle3);
                    startActivity(i3);
                    break;
                case R.id.arc_progress4:
                    Intent i4 = new Intent(DetailPart_Activity.this, Content_Part_Activity.class);
                    Bundle bundle4 = new Bundle();
                    bundle4.putInt(Constan.POSITION_OF_TEST, 4);
                    bundle4.putInt(Constan.TYPE, type);
                    bundle4.putInt(Constan.LEVEL,level);
                    i4.putExtra("DATA", bundle4);
                    startActivity(i4);
                    break;
                case R.id.arc_progress5:
                    Intent i5 = new Intent(DetailPart_Activity.this, Content_Part_Activity.class);
                    Bundle bundle5 = new Bundle();
                    bundle5.putInt(Constan.POSITION_OF_TEST, 5);
                    bundle5.putInt(Constan.TYPE, type);
                    bundle5.putInt(Constan.LEVEL,level);
                    i5.putExtra("DATA", bundle5);
                    startActivity(i5);
                    break;
                case R.id.arc_progress6:
                    Intent i6 = new Intent(DetailPart_Activity.this, Content_Part_Activity.class);
                    Bundle bundle6 = new Bundle();
                    bundle6.putInt(Constan.POSITION_OF_TEST, 6);
                    bundle6.putInt(Constan.TYPE, type);
                    bundle6.putInt(Constan.LEVEL,level);
                    i6.putExtra("DATA", bundle6);
                    startActivity(i6);
                    break;
                case R.id.arc_progress7:
                    Intent i7 = new Intent(DetailPart_Activity.this, Content_Part_Activity.class);
                    Bundle bundle7 = new Bundle();
                    bundle7.putInt(Constan.POSITION_OF_TEST, 7);
                    bundle7.putInt(Constan.TYPE, type);
                    bundle7.putInt(Constan.LEVEL,level);
                    i7.putExtra("DATA", bundle7);
                    startActivity(i7);
                    break;
                case R.id.arc_progress8:
                    Intent i8 = new Intent(DetailPart_Activity.this, Content_Part_Activity.class);
                    Bundle bundle8 = new Bundle();
                    bundle8.putInt(Constan.POSITION_OF_TEST, 8);
                    bundle8.putInt(Constan.TYPE, type);
                    bundle8.putInt(Constan.LEVEL,level);
                    i8.putExtra("DATA", bundle8);
                    startActivity(i8);
                    break;
                case R.id.arc_progress9:
                    Intent i9 = new Intent(DetailPart_Activity.this, Content_Part_Activity.class);
                    Bundle bundle9 = new Bundle();
                    bundle9.putInt(Constan.POSITION_OF_TEST, 9);
                    bundle9.putInt(Constan.TYPE, type);
                    bundle9.putInt(Constan.LEVEL,level);
                    i9.putExtra("DATA", bundle9);
                    startActivity(i9);
                    break;
                case R.id.arc_progress10:
                    Intent i10 = new Intent(DetailPart_Activity.this, Content_Part_Activity.class);
                    Bundle bundle10 = new Bundle();
                    bundle10.putInt(Constan.POSITION_OF_TEST, 10);
                    bundle10.putInt(Constan.TYPE, type);
                    bundle10.putInt(Constan.LEVEL,level);
                    i10.putExtra("DATA", bundle10);
                    startActivity(i10);
                    break;
            }
        }
    };

    @Override
    protected void onResume() {
        super.onResume();
        updateResultPractice();


    }

    private void updateResultPractice() {
        SharedPreferences pre = getSharedPreferences("my_data", MODE_PRIVATE);
        String jArray = pre.getString("data_percent", null);
        if (jArray != null) {
            Type type1 = new TypeToken<ArrayList<Data_Point_Part>>() {}.getType();
            Gson gson = new Gson();
            arr__dataPointPart = gson.fromJson(jArray, type1);
            for (int i = 0; i < arr__dataPointPart.size(); i++) {
                Data_Point_Part p = arr__dataPointPart.get(i);
                int mytype = p.getType();
                int mylevel=p.getLevel();
                if (mytype == type&& mylevel==level) {
                    int pos = p.getPos();
                    int per = p.getPercent();
                    switch (pos) {
                        case 1:
                            arcProgress1.setProgress(per);
                            break;
                        case 2:
                            arcProgress2.setProgress(per);
                            break;
                        case 3:
                            arcProgress3.setProgress(per);
                            break;
                        case 4:
                            arcProgress4.setProgress(per);
                            break;
                        case 5:
                            arcProgress5.setProgress(per);
                            break;
                        case 6:
                            arcProgress6.setProgress(per);
                            break;
                        case 7:
                            arcProgress7.setProgress(per);
                            break;
                        case 8:
                            arcProgress8.setProgress(per);
                            break;
                        case 9:
                            arcProgress9.setProgress(per);
                            break;
                        case 10:
                            arcProgress10.setProgress(per);
                            break;
                    }

                }

            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
private void clearPercent(){
    arcProgress1.setProgress(0);
    arcProgress2.setProgress(0);
    arcProgress3.setProgress(0);
    arcProgress4.setProgress(0);
    arcProgress5.setProgress(0);
    arcProgress6.setProgress(0);
    arcProgress7.setProgress(0);
    arcProgress8.setProgress(0);
    arcProgress9.setProgress(0);
    arcProgress10.setProgress(0);
}
    public void initActionbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        imgBack = (ImageView) findViewById(R.id.imgBtnBack);
        imv_clock = (ImageView) findViewById(R.id.imv_clock);
        tv_title_detail_part = (TextView) findViewById(R.id.tv_title_detail_part);
        tv_timer = (TextView) findViewById(R.id.tv_level);
        imv_clock.setVisibility(View.GONE);
        tv_timer.setVisibility(View.VISIBLE);
        tv_timer.setText("Level");
        tv_timer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog = new Dialog(DetailPart_Activity.this);
                dialog.setContentView(R.layout.dialog_level);
                dialog.setTitle("Chọn Level");

                TextView tv_easy = (TextView) dialog.findViewById(R.id.tv_easy);
                TextView tv_normal = (TextView) dialog.findViewById(R.id.tv_normal);
                TextView tv_hard = (TextView) dialog.findViewById(R.id.tv_hard);
                // khai báo control trong dialog để bắt sự kiện
                tv_easy.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        level=1;
                        clearPercent();
                        updateResultPractice();
                        dialog.dismiss();
                    }
                });
                tv_normal.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        level=2;
                        clearPercent();
                        updateResultPractice();
                        dialog.dismiss();
                    }
                });
                tv_hard.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        level=3;
                        clearPercent();
                        updateResultPractice();
                        dialog.dismiss();
                    }
                });

                // bắt sự kiện cho nút đăng kí
                dialog.show();
                // hiển thị dialog
            }
        });;

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                ((Content_Part_Activity) mContext).showDetailFragmentPart5();

                finish();
               /* onBackPressed();*/
            }
        });
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }
    }
}
