package com.example.luongvinh.englishforchange.Objects;

/**
 * Created by LUONGVINH on 6/4/2017.
 */

public class Data_Point_Test {
    private int pos,point_listening,point_reading,level;

    public Data_Point_Test(int pos, int point_listening, int point_reading, int level) {
        this.pos = pos;
        this.point_listening = point_listening;
        this.point_reading = point_reading;
        this.level = level;
    }

    public int getPos() {
        return pos;
    }

    public void setPos(int pos) {
        this.pos = pos;
    }

    public int getPoint_listening() {
        return point_listening;
    }

    public void setPoint_listening(int point_listening) {
        this.point_listening = point_listening;
    }

    public int getPoint_reading() {
        return point_reading;
    }

    public void setPoint_reading(int point_reading) {
        this.point_reading = point_reading;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }
}
