package com.example.luongvinh.englishforchange.controller;

public class Constants {
//Cac action de dieu khien service
	public interface ACTION {
		public static String MAIN_ACTION = "com.example.luongvinh.englishforchange.controller.action.main";
		public static String INIT_ACTION = "com.example.luongvinh.englishforchange.controller.action.init";
		public static String PREV_ACTION = "com.example.luongvinh.englishforchange.controller.action.prev";
		public static String PLAY_ACTION = "com.example.luongvinh.englishforchange.controller.action.play";
		public static String NEXT_ACTION = "com.example.luongvinh.englishforchange.controller.action.next";
		public static String BACK_ACTION = "com.example.luongvinh.englishforchange.controller.action.back";
		public static String FULL_LIST_ACTION = "com.example.luongvinh.englishforchange.controller.action.fulllist";
		public static String STARTFOREGROUND_ACTION = "com.example.luongvinh.englishforchange.controller.action.startforeground";
		public static String STOPFOREGROUND_ACTION = "com.example.luongvinh.englishforchange.controller.action.stopforeground";
	}

	public interface NOTIFICATION_ID {
		public static int FOREGROUND_SERVICE = 101;
	}
}
