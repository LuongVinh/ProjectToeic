package com.example.luongvinh.englishforchange.Objects;

/**
 * Created by LUONGVINH on 5/13/2017.
 */
public class Question_Part6 {
    private String answer,question_en,question_vi;

    public Question_Part6() {
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getQuestion_en() {
        return question_en;
    }

    public void setQuestion_en(String question_en) {
        this.question_en = question_en;
    }

    public String getQuestion_vi() {
        return question_vi;
    }

    public void setQuestion_vi(String question_vi) {
        this.question_vi = question_vi;
    }
}
