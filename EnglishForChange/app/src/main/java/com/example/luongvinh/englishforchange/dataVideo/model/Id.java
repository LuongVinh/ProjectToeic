package com.example.luongvinh.englishforchange.dataVideo.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by LeToan on 5/13/2017.
 */

public class Id implements Serializable{

    @SerializedName("videoId")
    @Expose
    private String videoId;



    public String getVideoId() {
        return videoId;
    }

    public void setVideoId(String videoId) {
        this.videoId = videoId;
    }
}
