package com.example.luongvinh.englishforchange.controller;

public interface OnHomePressedListener {
	public void onHomePressed();
    public void onHomeLongPressed();
}
