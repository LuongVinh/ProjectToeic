package com.example.luongvinh.englishforchange.Fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.luongvinh.englishforchange.Activities.Content_Part_Activity;
import com.example.luongvinh.englishforchange.Objects.Data_Point_Test;
import com.example.luongvinh.englishforchange.R;
import com.example.luongvinh.englishforchange.Until.Constan;
import com.github.lzyzsd.circleprogress.ArcProgress;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by LUONGVINH on 5/27/2017.
 */

public class Fragment_Show_Result_Fulltest extends Fragment {
    private View root;
    private Context mContext;
    private int num_point_listening=0, num_point_reading =0, position=0,level=1;
    private ArcProgress arcProgress;
    private Button btn_Result;
    private TextView tv_sum_fulltest, tv_sumlistening, tv_sum_reading;
    private RecyclerView rcv;
    private ArrayList<Data_Point_Test> arr_Data_point_tests;
    public static Fragment_Show_Result_Fulltest newInstance(int position, int point_listen, int point_reading) {
        Fragment_Show_Result_Fulltest fragment = new Fragment_Show_Result_Fulltest();
        Bundle bundle = new Bundle();
        bundle.putInt(Constan.POSITION_OF_TEST, position);
        bundle.putInt(Constan.POINT_LISTENING, point_listen);
        bundle.putInt(Constan.POINT_READING, point_reading);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        root = LayoutInflater.from(mContext).inflate(R.layout.fragment_show_result_fulltest, container, false);
        arcProgress = (ArcProgress) root.findViewById(R.id.arc_progress_result);
        btn_Result = (Button) root.findViewById(R.id.btn_result);
        tv_sum_fulltest = (TextView) root.findViewById(R.id.tv_sum_fulltest);
        tv_sumlistening = (TextView) root.findViewById(R.id.tv_sumlistening);
        tv_sum_reading = (TextView) root.findViewById(R.id.tv_sum_reading);
        btn_Result.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ((Content_Part_Activity)mContext).setType(1);
                ((Content_Part_Activity) mContext).showFragmentDetailResult();

            }
        });
        int a= num_point_listening + num_point_reading;
         arcProgress.setProgress((a * 100)/200);

        tv_sum_fulltest.setText(num_point_listening + num_point_reading + "");
        tv_sumlistening.setText(num_point_listening + "");
        tv_sum_reading.setText(num_point_reading + "");


        return root;
    }





    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getContext();
        num_point_listening = getArguments().getInt(Constan.POINT_LISTENING);
        num_point_reading = getArguments().getInt(Constan.POINT_READING);
        position= getArguments().getInt(Constan.POSITION_OF_TEST);
        arr_Data_point_tests=new ArrayList<>();
        ((Content_Part_Activity) mContext).setTextTitleBar("Kết Quả");

    }

    @Override
    public void onPause() {
        super.onPause();
        level=((Content_Part_Activity)mContext).getLevel();
        Data_Point_Test p =new Data_Point_Test(position,num_point_listening,num_point_reading,level);
        arr_Data_point_tests.add(position,p);
        Gson gson=new Gson();
        String jsArray=gson.toJson(arr_Data_point_tests);



        SharedPreferences pre=getContext().getSharedPreferences("my_data", MODE_PRIVATE);
        SharedPreferences.Editor edit=pre.edit();
        edit.putString("data_point_test",jsArray+"");
        edit.commit();
    }

    @Override
    public void onResume() {
        super.onResume();
        SharedPreferences pre = getContext().getSharedPreferences("my_data", MODE_PRIVATE);
        String jArray = pre.getString("data_point_test", null);
        if(jArray!=null){
            Type type1 = new TypeToken<ArrayList<Data_Point_Test>>() {}.getType();
            Gson gson = new Gson();
            arr_Data_point_tests = gson.fromJson(jArray, type1);
        }

    }
}
