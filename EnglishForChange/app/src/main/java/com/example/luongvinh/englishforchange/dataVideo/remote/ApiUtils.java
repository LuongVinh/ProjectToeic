package com.example.luongvinh.englishforchange.dataVideo.remote;

/**
 * Created by LeToan on 5/11/2017.
 */

public class ApiUtils {
    public static final String BASE_URL = "https://www.googleapis.com/youtube/v3/";

    public static SOService getSOService() {
        return RetrofitClient.getClient(BASE_URL).create(SOService.class);
    }
}
