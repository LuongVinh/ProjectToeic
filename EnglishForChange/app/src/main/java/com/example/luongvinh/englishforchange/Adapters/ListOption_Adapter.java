package com.example.luongvinh.englishforchange.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.luongvinh.englishforchange.Objects.Option;
import com.example.luongvinh.englishforchange.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by LUONGVINH on 4/4/2017.
 */
public class ListOption_Adapter extends ArrayAdapter<Option> {
    private ViewHolder holder;
    private ArrayList<Option> optionArrayList;
    private Context mcontext;
    public ListOption_Adapter(Context context, int resource, List<Option> optionList) {
        super(context, resource, optionList);
        this.mcontext=context;
        this.optionArrayList= new ArrayList<>(optionList);
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;

        holder = new ViewHolder();
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) mcontext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.item_list_option, null);

            holder.tv_name_option= (TextView) view.findViewById(R.id.tv_option);

            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();

        }
        Option option = optionArrayList.get(position);
        holder.tv_name_option.setText(option.getName_option());
        return view;
    }

    static class ViewHolder {
        TextView tv_name_option;


    }
}

