package com.example.luongvinh.englishforchange.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.luongvinh.englishforchange.Objects.Test;
import com.example.luongvinh.englishforchange.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by LUONGVINH on 4/4/2017.
 */
public class ListTest_Adapter extends ArrayAdapter<Test> {
    private ViewHolder holder;
    private ArrayList<Test> testArrayList;
    private  Context mcontext;
    public ListTest_Adapter(Context context, int resource, List<Test> testList) {
        super(context, resource, testList);
        this.mcontext=context;
        this.testArrayList= new ArrayList<>(testList);
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;

        holder = new ViewHolder();
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) mcontext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.item_list_test, null);

            holder.tv_num_test= (TextView) view.findViewById(R.id.tv_num_test);
            holder.tv_diem_nghe= (TextView) view.findViewById(R.id.tv_diem_nghe);
            holder.tv_cau_nghe= (TextView) view.findViewById(R.id.tv_cau_nghe);
            holder.tv_diem_doc= (TextView) view.findViewById(R.id.tv_diem_doc);
            holder.tv_cau_doc= (TextView) view.findViewById(R.id.tv_cau_doc);
            holder.tv_tong_diem= (TextView) view.findViewById(R.id.tv_tong_diem);

            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();

        }
        Test test = testArrayList.get(position);
        holder.tv_num_test.setText(test.getTv_num_test());
        holder.tv_cau_doc.setText(test.getTv_cau_doc());
        holder.tv_cau_nghe.setText(test.getTv_cau_nghe());
        holder.tv_diem_doc.setText(test.getTv_diem_doc());
        holder.tv_diem_nghe.setText(test.getTv_diem_nghe());
        holder.tv_tong_diem.setText(test.getTv_tong_diem());

        return view;
    }

    static class ViewHolder {
        TextView tv_num_test;
        TextView tv_diem_nghe;
        TextView tv_cau_nghe;
        TextView tv_diem_doc;
        TextView tv_cau_doc;
        TextView tv_tong_diem;

    }
}

