package com.example.luongvinh.englishforchange.controller;

import android.content.Context;
import android.content.Intent;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.widget.LinearLayout;

import java.lang.ref.WeakReference;

public class CustomView extends LinearLayout {
	public CustomView(Context context, AttributeSet attrs) {
		super(context, attrs);
		mContext = new WeakReference<Context>(context);
	}

	private WeakReference<Context> mContext;

	@Override
	public boolean dispatchKeyEvent(KeyEvent event) {
		// TODO Auto-generated method stub
		if (event != null && event.getKeyCode() == KeyEvent.KEYCODE_BACK) {
			Log.d("khanhduy.le", "back button pressed");

			if (mContext != null && mContext.get() != null) {
				Intent mInten = new Intent(mContext.get(), PlayYoutubeServiceControl.class);
				mInten.setAction(Constants.ACTION.BACK_ACTION);
				mContext.get().startService(mInten);
			}

			return true;
		}

		return super.dispatchKeyEvent(event);
	}
}
