package com.example.luongvinh.englishforchange.Objects;

/**
 * Created by LUONGVINH on 5/19/2017.
 */

public class Question_Part4 {
    private String Answer;
    private String Con_en;
    private String Con_vi;
    private String Que_en;
    private String Que_vi;

    public Question_Part4() {
    }

    public String getAnswer() {
        return Answer;
    }

    public void setAnswer(String answer) {
        Answer = answer;
    }

    public String getCon_en() {
        return Con_en;
    }

    public void setCon_en(String con_en) {
        Con_en = con_en;
    }

    public String getCon_vi() {
        return Con_vi;
    }

    public void setCon_vi(String con_vi) {
        Con_vi = con_vi;
    }

    public String getQue_en() {
        return Que_en;
    }

    public void setQue_en(String que_en) {
        Que_en = que_en;
    }

    public String getQue_vi() {
        return Que_vi;
    }

    public void setQue_vi(String que_vi) {
        Que_vi = que_vi;
    }
}
