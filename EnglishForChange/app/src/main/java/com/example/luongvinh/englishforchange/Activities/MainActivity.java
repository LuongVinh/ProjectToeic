package com.example.luongvinh.englishforchange.Activities;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentTabHost;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.luongvinh.englishforchange.Fragments.Fragment_More;
import com.example.luongvinh.englishforchange.Fragments.Fragment_Part;
import com.example.luongvinh.englishforchange.Fragments.Fragment_Video;
import com.example.luongvinh.englishforchange.Fragments.Frangment_Test;
import com.example.luongvinh.englishforchange.R;

import java.security.MessageDigest;

public class MainActivity extends AppCompatActivity {

    private FragmentTabHost mTabHost;
    private Toolbar toolbar;
    public TextView tv_part, tvLever;
    private ImageView imgBack, imv_clock;
    private Boolean bool = false;
    public Dialog dialog;
    public int level=1;
    private int MyVersion;
    private  int OVERLAY_PERMISSION_REQ_CODE = 1234;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        MyVersion = Build.VERSION.SDK_INT;
        if (MyVersion > Build.VERSION_CODES.LOLLIPOP_MR1) {
            if (!checkIfAlreadyhavePermission()) {
                requestForSpecificPermission();
            }
            if (!Settings.canDrawOverlays(this)) {
                Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                        Uri.parse("package:" + getPackageName()));
                startActivityForResult(intent, OVERLAY_PERMISSION_REQ_CODE);
            }
        }

        initActionbar(bool);
        initTab();
        changeColorStatusBar(this);

        try {
            PackageInfo packageInfo = getPackageManager().getPackageInfo("com.example.luongvinh.englishforchange", PackageManager.GET_SIGNATURES);
            for (Signature signature : packageInfo.signatures) {
                MessageDigest messageDigest = MessageDigest.getInstance("SHA");
                messageDigest.update(signature.toByteArray());
                Log.d("KeyHash", Base64.encodeToString(messageDigest.digest(), Base64.DEFAULT));
            }
        } catch (Exception e) {
        }


    }

    private boolean checkIfAlreadyhavePermission() {
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) + ContextCompat.checkSelfPermission(this,Manifest.permission.SYSTEM_ALERT_WINDOW ) + ContextCompat.checkSelfPermission(this,Manifest.permission.WAKE_LOCK) +  ContextCompat.checkSelfPermission(this,Manifest.permission.DISABLE_KEYGUARD) +  ContextCompat.checkSelfPermission(this,Manifest.permission.SYSTEM_ALERT_WINDOW ) +  ContextCompat.checkSelfPermission(this,Manifest.permission.READ_EXTERNAL_STORAGE );
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private void requestForSpecificPermission() {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.SYSTEM_ALERT_WINDOW, Manifest.permission.WAKE_LOCK, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.DISABLE_KEYGUARD, Manifest.permission.SYSTEM_ALERT_WINDOW}, 101);
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case 101:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //granted
                    Toast.makeText(getApplicationContext(),"SUCCSESS!",Toast.LENGTH_LONG).show();
                } else {
                    //not granted
                    Toast.makeText(getApplicationContext(),"NO SUCCSESS!",Toast.LENGTH_LONG).show();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == OVERLAY_PERMISSION_REQ_CODE) {
            if (!Settings.canDrawOverlays(this)) {
                // SYSTEM_ALERT_WINDOW permission not granted...
                Toast.makeText(getApplicationContext(),"NOT SUCCESS",Toast.LENGTH_LONG).show();
            }else Toast.makeText(getApplicationContext(),"SUCCESS",Toast.LENGTH_LONG).show();
        }
    }

    public void ChooseLevel() {
        tvLever.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog = new Dialog(MainActivity.this);
                dialog.setContentView(R.layout.dialog_level);
                dialog.setTitle("Chọn Level");

                TextView tv_easy = (TextView) dialog.findViewById(R.id.tv_easy);
                TextView tv_normal = (TextView) dialog.findViewById(R.id.tv_normal);
                TextView tv_hard = (TextView) dialog.findViewById(R.id.tv_hard);
                // khai báo control trong dialog để bắt sự kiện
                tv_easy.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        level=1;
                        /*SharedPreferences pre=getSharedPreferences("my_data", MODE_PRIVATE);
                        SharedPreferences.Editor edit=pre.edit();
                        edit.putInt("LEVEL",level);
                        edit.commit();*/
                        dialog.dismiss();
                    }
                });
                tv_normal.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        level=2;
                        /*SharedPreferences pre=getSharedPreferences("my_data", MODE_PRIVATE);
                        SharedPreferences.Editor edit=pre.edit();
                        edit.putInt("LEVEL",level);
                        edit.commit();*/
                        dialog.dismiss();
                    }
                });
                tv_hard.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        level=3;
                        /*SharedPreferences pre=getSharedPreferences("my_data", MODE_PRIVATE);
                        SharedPreferences.Editor edit=pre.edit();
                        edit.putInt("LEVEL",level);
                        edit.commit();*/
                        dialog.dismiss();
                    }
                });

                // bắt sự kiện cho nút đăng kí
                dialog.show();
                // hiển thị dialog
            }
        });

    }

    private void initTab() {
        mTabHost = (FragmentTabHost) findViewById(android.R.id.tabhost);
        mTabHost.setup(this, getSupportFragmentManager(), android.R.id.tabcontent);
        mTabHost.getTabWidget().setDividerDrawable(null);

        mTabHost.addTab(
                mTabHost.newTabSpec("Part").setIndicator(getTabIndicator(mTabHost.getContext(), "Part", R.drawable.icon_part)),
                Fragment_Part.class, null);
        mTabHost.addTab(
                mTabHost.newTabSpec("Test").setIndicator(getTabIndicator(mTabHost.getContext(), "Test", R.drawable.icon_test)),
                Frangment_Test.class, null);
        mTabHost.addTab(
                mTabHost.newTabSpec("Video").setIndicator(getTabIndicator(mTabHost.getContext(), "Video", R.drawable.icon_video)),
                Fragment_Video.class, null);
        mTabHost.addTab(
                mTabHost.newTabSpec("More").setIndicator(getTabIndicator(mTabHost.getContext(), "More", R.drawable.icon_more)),
                Fragment_More.class, null);
    }

    public void initActionbar(Boolean bool) {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        imgBack = (ImageView) toolbar.findViewById(R.id.imgBtnBack);
        imv_clock = (ImageView) toolbar.findViewById(R.id.imv_clock);
        imv_clock.setVisibility(View.GONE);

        tv_part = (TextView) toolbar.findViewById(R.id.tv_title_detail_part);
        tvLever = (TextView) toolbar.findViewById(R.id.tv_level);
        if (bool == false) {
            tvLever.setVisibility(View.VISIBLE);

        } else {
            tvLever.setVisibility(View.GONE);

        }
        tv_part.setText("Luyện Thi Tiêu Chuẩn");
        imgBack.setImageDrawable(getResources().getDrawable(R.drawable.ic_cart));
        tvLever.setText("Level");
//
//
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


            }
        });
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }
    }

    public void setTitleActionBar(String titleActionBar) {
        tv_part.setText(titleActionBar);
    }

    private View getTabIndicator(Context context, String title, int icon) {
        View view = LayoutInflater.from(context).inflate(R.layout.tab_home, null);
        ImageView iv = (ImageView) view.findViewById(R.id.imv_tab_icon);
        iv.setImageResource(icon);
        TextView tv = (TextView) view.findViewById(R.id.tv_tabname);
        tv.setText(title);
        return view;
    }


    public static void changeColorStatusBar(Activity activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = activity.getWindow();
            // clear FLAG_TRANSLUCENT_STATUS flag:
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            //  add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            //finally change the color
            window.setStatusBarColor(activity.getResources().getColor(R.color.color));
        }
    }

}