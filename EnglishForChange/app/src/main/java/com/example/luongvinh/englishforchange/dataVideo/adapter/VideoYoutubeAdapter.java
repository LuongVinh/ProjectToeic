package com.example.luongvinh.englishforchange.dataVideo.adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.luongvinh.englishforchange.R;
import com.example.luongvinh.englishforchange.dataVideo.model.Item;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by LeToan on 5/13/2017.
 */

public class VideoYoutubeAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<Item> mItems;
    private Context mContext;
    private StreamItemListener mItemListener;
    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;
    private int visibleThreshold = 5;
    private int lastVisibleItem, totalItemCount;
    private boolean isLoading;

    public class ItemView extends RecyclerView.ViewHolder {

        public TextView titleTv,authorTv;
        private TextView tv_Upload;
        private ImageView img;
        private CardView cardView;



        public ItemView(View itemView) {
            super(itemView);
            titleTv=(TextView)itemView.findViewById(R.id.txtTitlePopular);
            authorTv=(TextView)itemView.findViewById(R.id.txtAuthorPopular);
            img=(ImageView)itemView.findViewById(R.id.img_popular);
            tv_Upload=(TextView)itemView.findViewById(R.id.txtDatePopular);
            cardView=(CardView)itemView.findViewById(R.id.item_video);
        }
    }
    public class LoadingViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public LoadingViewHolder(View view) {
            super(view);
            progressBar = (ProgressBar) view.findViewById(R.id.progressBar1);
        }
    }

    public VideoYoutubeAdapter(RecyclerView recyclerView,Context context, List<Item> posts, StreamItemListener itemListener) {
        mItems = posts;
        mContext = context;
        mItemListener = itemListener;
        final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if(linearLayoutManager == null){
                    return;
                }
                totalItemCount = linearLayoutManager.getItemCount();
                lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                if (!isLoading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                    if (onLoadMoreListener != null) {
                        onLoadMoreListener.onLoadMore();
                    }
                    isLoading = true;
                }
            }
        });
    }

    @Override
    public int getItemViewType(int position) {
        return mItems.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View view = LayoutInflater.from(mContext).inflate(R.layout.item_video, parent, false);
            return new ItemView(view);
        } else if (viewType == VIEW_TYPE_LOADING) {
            View view = LayoutInflater.from(mContext).inflate(R.layout.item_loading, parent, false);
            return new LoadingViewHolder(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        if(holder instanceof ItemView){
            final Item item = mItems.get(position);
            ItemView itemView=(ItemView)holder;
            itemView.tv_Upload.setText(item.getSnippet().getPublishedAt().split("T")[0]);
            itemView.titleTv.setText(item.getSnippet().getTitle());
            itemView.authorTv.setText(item.getSnippet().getChannelTitle()+ ":");
            ImageView imageView=itemView.img;
            Picasso.with(mContext)
                    .load(item.getSnippet().getThumbnails().getHigh().getUrl())

                    .into(imageView);
            itemView.cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(mItemListener !=null){
                        mItemListener.onPostClick(item.getId().getVideoId()+"",position,item.getSnippet().getTitle());
                    }
                }
            });
        }else if(holder instanceof LoadingViewHolder){
            LoadingViewHolder loadingViewHolder = (LoadingViewHolder) holder;
            loadingViewHolder.progressBar.setIndeterminate(true);
        }


    }

    @Override
    public int getItemCount() {
        return mItems==null? 0:mItems.size();
    }

    public void setLoaded() {
        isLoading = false;
    }
    public void updateAnswers(List<Item> items) {
        mItems.addAll(items);
        notifyDataSetChanged();
    }

    private Item getItem(int adapterPosition) {
        return mItems.get(adapterPosition);
    }

    public interface StreamItemListener {
        void onPostClick(String idVideo, int position, String titleVideo);

    }
    public interface OnLoadMoreListener {
        void onLoadMore();
    }
    private OnLoadMoreListener onLoadMoreListener;
    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.onLoadMoreListener = mOnLoadMoreListener;
    }
}
