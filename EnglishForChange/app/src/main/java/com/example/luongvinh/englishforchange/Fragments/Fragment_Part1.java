package com.example.luongvinh.englishforchange.Fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.luongvinh.englishforchange.Activities.Content_Part_Activity;
import com.example.luongvinh.englishforchange.Objects.Question_Part1_Part2;
import com.example.luongvinh.englishforchange.R;
import com.example.luongvinh.englishforchange.Until.Constan;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by LUONGVINH on 4/27/2017.
 */
public class Fragment_Part1 extends Fragment implements Fragment_Part1_Pager.AnswerChoise {
    private View root;
    private Context mContext;
    private int position;
    private int index;
    private int type;
    private ImageView imageView;
    private TextView tv_Transcrip;
    private String pathPNG;
    private TextView tv_answerA, tv_answerB, tv_answerC, tv_answerD;
    private int answerRight = 0;
    private ArrayList<Question_Part1_Part2> arraylist_p1 =new ArrayList<>();
    private ArrayList<Question_Part1_Part2> arraylist_p2 =new ArrayList<>();;
    private int[] reviewPart;
    private Question_Part1_Part2 answer;
    private Boolean full = false;


    public static Fragment_Part1 newInstance(int position, int index, int type, String pathPNG) {
        Fragment_Part1 fragment = new Fragment_Part1();
        Bundle bundle = new Bundle();
        bundle.putInt(Constan.POSITION_OF_TEST, position);
        bundle.putInt(Constan.PART1_PICTURN_INDEX, index);
        bundle.putInt(Constan.TYPE, type);
        bundle.putString(Constan.PNG_FILE, pathPNG);
        fragment.setArguments(bundle);

        return fragment;

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getContext();
        position = getArguments().getInt(Constan.POSITION_OF_TEST);
        index = getArguments().getInt(Constan.PART1_PICTURN_INDEX);
        pathPNG = getArguments().getString(Constan.PNG_FILE);
        type = getArguments().getInt(Constan.TYPE);
        arraylist_p1 = ((Content_Part_Activity) mContext).getListQuestionPart1();
        arraylist_p2 = ((Content_Part_Activity) mContext).getListQuestionPart2();
        full = ((Content_Part_Activity) mContext).getFull();

    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        root = LayoutInflater.from(mContext).inflate(R.layout.fragment_part1, container, false);
        imageView = (ImageView) root.findViewById(R.id.img_picturn);
        tv_Transcrip = (TextView) root.findViewById(R.id.tv_transcrip);
        tv_answerA = (TextView) root.findViewById(R.id.answerA);
        tv_answerB = (TextView) root.findViewById(R.id.answerB);
        tv_answerC = (TextView) root.findViewById(R.id.answerC);
        tv_answerD = (TextView) root.findViewById(R.id.answerD);
        tv_answerA.setOnClickListener(onClickAnswer);
        tv_answerB.setOnClickListener(onClickAnswer);
        tv_answerC.setOnClickListener(onClickAnswer);
        tv_answerD.setOnClickListener(onClickAnswer);

        if (type == 2) {
            imageView.setVisibility(View.GONE);
            tv_Transcrip.setVisibility(View.VISIBLE);
            RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) tv_Transcrip.getLayoutParams();
            lp.removeRule(RelativeLayout.ALIGN_BOTTOM);
            lp.addRule(RelativeLayout.CENTER_IN_PARENT);
        }


        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (type == 1) {
            imageView.setImageURI(Uri.fromFile(new File(pathPNG)));
            ((Content_Part_Activity) mContext).setAnswerChoise(this);
        } else if (type == 2) {
            ((Content_Part_Activity) mContext).setAnswerChoise(this);

        }


    }

    public View.OnClickListener onClickAnswer = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (type == 1) {
                switch (v.getId()) {
                    case R.id.answerA: {
                        setBackground(v, R.drawable.round_shape_choise, R.color.color_while);
                        setBackground(tv_answerB, R.drawable.round_shape, R.color.colorPrimary);
                        setBackground(tv_answerC, R.drawable.round_shape, R.color.colorPrimary);
                        setBackground(tv_answerD, R.drawable.round_shape, R.color.colorPrimary);
                        ((Content_Part_Activity) mContext).setValueReview(index - 1, 1);//lưu trạng thái người dùng chọn đáp án

                    }
                    break;
                    case R.id.answerB: {
                        setBackground(v, R.drawable.round_shape_choise, R.color.color_while);
                        setBackground(tv_answerA, R.drawable.round_shape, R.color.colorPrimary);
                        setBackground(tv_answerC, R.drawable.round_shape, R.color.colorPrimary);
                        setBackground(tv_answerD, R.drawable.round_shape, R.color.colorPrimary);
                        ((Content_Part_Activity) mContext).setValueReview(index - 1, 2);
                    }
                    break;
                    case R.id.answerC: {
                        setBackground(v, R.drawable.round_shape_choise, R.color.color_while);
                        setBackground(tv_answerB, R.drawable.round_shape, R.color.colorPrimary);
                        setBackground(tv_answerA, R.drawable.round_shape, R.color.colorPrimary);
                        setBackground(tv_answerD, R.drawable.round_shape, R.color.colorPrimary);
                        ((Content_Part_Activity) mContext).setValueReview(index - 1, 3);


                    }
                    break;
                    case R.id.answerD: {
                        setBackground(v, R.drawable.round_shape_choise, R.color.color_while);
                        setBackground(tv_answerB, R.drawable.round_shape, R.color.colorPrimary);
                        setBackground(tv_answerC, R.drawable.round_shape, R.color.colorPrimary);
                        setBackground(tv_answerA, R.drawable.round_shape, R.color.colorPrimary);
                        ((Content_Part_Activity) mContext).setValueReview(index - 1, 4);

                    }
                    break;
                }
            } else if (type == 2) {
                if (full == false) {
                    switch (v.getId()) {
                        case R.id.answerA: {
                            setBackground(v, R.drawable.round_shape_choise, R.color.color_while);
                            setBackground(tv_answerB, R.drawable.round_shape, R.color.colorPrimary);
                            setBackground(tv_answerC, R.drawable.round_shape, R.color.colorPrimary);
                            setBackground(tv_answerD, R.drawable.round_shape, R.color.colorPrimary);
                            ((Content_Part_Activity) mContext).setValueReview(index - 1, 1);//lưu trạng thái người dùng chọn đáp án

                        }
                        break;
                        case R.id.answerB: {
                            setBackground(v, R.drawable.round_shape_choise, R.color.color_while);
                            setBackground(tv_answerA, R.drawable.round_shape, R.color.colorPrimary);
                            setBackground(tv_answerC, R.drawable.round_shape, R.color.colorPrimary);
                            setBackground(tv_answerD, R.drawable.round_shape, R.color.colorPrimary);
                            ((Content_Part_Activity) mContext).setValueReview(index - 1, 2);
                        }
                        break;
                        case R.id.answerC: {
                            setBackground(v, R.drawable.round_shape_choise, R.color.color_while);
                            setBackground(tv_answerB, R.drawable.round_shape, R.color.colorPrimary);
                            setBackground(tv_answerA, R.drawable.round_shape, R.color.colorPrimary);
                            setBackground(tv_answerD, R.drawable.round_shape, R.color.colorPrimary);
                            ((Content_Part_Activity) mContext).setValueReview(index - 1, 3);


                        }
                        break;
                        case R.id.answerD: {
                            setBackground(v, R.drawable.round_shape_choise, R.color.color_while);
                            setBackground(tv_answerB, R.drawable.round_shape, R.color.colorPrimary);
                            setBackground(tv_answerC, R.drawable.round_shape, R.color.colorPrimary);
                            setBackground(tv_answerA, R.drawable.round_shape, R.color.colorPrimary);
                            ((Content_Part_Activity) mContext).setValueReview(index - 1, 4);

                        }
                        break;
                    }

                } else if (full == true) {
                    switch (v.getId()) {
                        case R.id.answerA: {
                            setBackground(v, R.drawable.round_shape_choise, R.color.color_while);
                            setBackground(tv_answerB, R.drawable.round_shape, R.color.colorPrimary);
                            setBackground(tv_answerC, R.drawable.round_shape, R.color.colorPrimary);
                            setBackground(tv_answerD, R.drawable.round_shape, R.color.colorPrimary);
                            ((Content_Part_Activity) mContext).setValueReview(index + 9, 1);//lưu trạng thái người dùng chọn đáp án

                        }
                        break;
                        case R.id.answerB: {
                            setBackground(v, R.drawable.round_shape_choise, R.color.color_while);
                            setBackground(tv_answerA, R.drawable.round_shape, R.color.colorPrimary);
                            setBackground(tv_answerC, R.drawable.round_shape, R.color.colorPrimary);
                            setBackground(tv_answerD, R.drawable.round_shape, R.color.colorPrimary);
                            ((Content_Part_Activity) mContext).setValueReview(index + 9, 2);
                        }
                        break;
                        case R.id.answerC: {
                            setBackground(v, R.drawable.round_shape_choise, R.color.color_while);
                            setBackground(tv_answerB, R.drawable.round_shape, R.color.colorPrimary);
                            setBackground(tv_answerA, R.drawable.round_shape, R.color.colorPrimary);
                            setBackground(tv_answerD, R.drawable.round_shape, R.color.colorPrimary);
                            ((Content_Part_Activity) mContext).setValueReview(index + 9, 3);


                        }
                        break;
                        case R.id.answerD: {
                            setBackground(v, R.drawable.round_shape_choise, R.color.color_while);
                            setBackground(tv_answerB, R.drawable.round_shape, R.color.colorPrimary);
                            setBackground(tv_answerC, R.drawable.round_shape, R.color.colorPrimary);
                            setBackground(tv_answerA, R.drawable.round_shape, R.color.colorPrimary);
                            ((Content_Part_Activity) mContext).setValueReview(index + 9, 4);

                        }
                        break;
                    }
                }
            }


            if ((index == 10 && type == 1) || (index == 30 && type == 2)) {
                //lấy ra mảng chứa các đáp án người dùng chọn
                reviewPart = ((Content_Part_Activity) mContext).getReviewPart1();
                if (full == false) {
                    if(type==1){
                        for (int i = 0; i < arraylist_p1.size(); i++) {
                            answer = arraylist_p1.get(i);
                            if (answer.getAnswer().equals("A") && reviewPart[i] == 1)
                                answerRight = answerRight + 1;
                            if (answer.getAnswer().equals("B") && reviewPart[i] == 2)
                                answerRight = answerRight + 1;
                            if (answer.getAnswer().equals("C") && reviewPart[i] == 3)
                                answerRight = answerRight + 1;
                            if (answer.getAnswer().equals("D") && reviewPart[i] == 4)
                                answerRight = answerRight + 1;
                        }
                    }
                    else if(type==2){
                        for (int i = 0; i < arraylist_p2.size(); i++) {
                            answer = arraylist_p2.get(i);
                            if (answer.getAnswer().equals("A") && reviewPart[i] == 1)
                                answerRight = answerRight + 1;
                            if (answer.getAnswer().equals("B") && reviewPart[i] == 2)
                                answerRight = answerRight + 1;
                            if (answer.getAnswer().equals("C") && reviewPart[i] == 3)
                                answerRight = answerRight + 1;
                            if (answer.getAnswer().equals("D") && reviewPart[i] == 4)
                                answerRight = answerRight + 1;
                        }
                    }

                }
                else if (full = true) {
                    if (type == 1 && arraylist_p1.size() > 0) {
                        for (int i = 0; i < arraylist_p1.size(); i++) {
                            answer = arraylist_p1.get(i);
                            if (answer.getAnswer().equals("A") && reviewPart[i] == 1)
                                answerRight = answerRight + 1;
                            if (answer.getAnswer().equals("B") && reviewPart[i] == 2)
                                answerRight = answerRight + 1;
                            if (answer.getAnswer().equals("C") && reviewPart[i] == 3)
                                answerRight = answerRight + 1;
                            if (answer.getAnswer().equals("D") && reviewPart[i] == 4)
                                answerRight = answerRight + 1;
                        }

                    }
                    else if (type == 2 && arraylist_p2.size() > 0) {
                        for (int i = 0; i < arraylist_p2.size(); i++) {
                            answer = arraylist_p2.get(i);
                            if (answer.getAnswer().equals("A") && reviewPart[i+10] == 1)
                                answerRight = answerRight + 1;
                            if (answer.getAnswer().equals("B") && reviewPart[i+10] == 2)
                                answerRight = answerRight + 1;
                            if (answer.getAnswer().equals("C") && reviewPart[i+10] == 3)
                                answerRight = answerRight + 1;
                            if (answer.getAnswer().equals("D") && reviewPart[i+10] == 4)
                                answerRight = answerRight + 1;
                        }
                    }

                }


                // công việc gửi số câu đúng sang màn hình show result
                if (full == false) {
                    ((Content_Part_Activity) mContext).showFragmentShowResult(answerRight);
                } else {
                    if (type == 1) {
                        ((Content_Part_Activity) mContext).addPoint(answerRight, type);
                        Log.e("Partt1", answerRight + "");
                        ((Content_Part_Activity) mContext).setType(2);
                        ((Content_Part_Activity) mContext).showFragment();
                    } else if (type == 2) {
                        ((Content_Part_Activity) mContext).addPoint(answerRight, type);
                        Log.e("Partt2", answerRight + "");
                        ((Content_Part_Activity) mContext).setType(3);
                        ((Content_Part_Activity) mContext).showFragment();
                    }


                }

            }


        }
    };

    public void setBackground(View view, int background, int colorText) {
        view.setBackgroundDrawable(getResources().getDrawable(background));
        ((TextView) view).setTextColor(mContext.getResources().getColor(colorText));

    }

    @Override
    public void setAnswerChoise(int position) {
        int answerChoise = 0;
        answerChoise = ((Content_Part_Activity) mContext).getValueReview(position);

        switch (answerChoise) {
            case 1: {
                tv_answerA.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.round_shape_choise));
                tv_answerA.setTextColor(mContext.getResources().getColor(R.color.color_while));
            }
            break;
            case 2: {
                tv_answerB.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.round_shape_choise));
                tv_answerB.setTextColor(mContext.getResources().getColor(R.color.color_while));
            }
            break;
            case 3: {
                tv_answerC.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.round_shape_choise));
                tv_answerC.setTextColor(mContext.getResources().getColor(R.color.color_while));
            }
            break;
            case 4: {
                tv_answerD.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.round_shape_choise));
                tv_answerD.setTextColor(mContext.getResources().getColor(R.color.color_while));
            }
            break;
        }
    }


}
