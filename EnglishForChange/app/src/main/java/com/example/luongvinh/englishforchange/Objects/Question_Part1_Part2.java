package com.example.luongvinh.englishforchange.Objects;

/**
 * Created by LeToan on 4/27/2017.
 */

public class Question_Part1_Part2 {
    private String Answer;
    private String Transcription_en;
    private String Transcription_vi;

    public Question_Part1_Part2() {
    }

    public Question_Part1_Part2(String answer, String transcrip_en, String transcrip_vi) {
        this.Answer = answer;
        this.Transcription_en = transcrip_en;
        this.Transcription_vi = transcrip_vi;
    }

    public String getAnswer() {
        return Answer;
    }

    public void setAnswer(String answer) {
        this.Answer = answer;
    }

    public String getTranscription_en() {
        return Transcription_en;
    }

    public void setTranscription_en(String transcription_en) {
        this.Transcription_en = transcription_en;
    }

    public String getTranscription_vi() {
        return Transcription_vi;
    }

    public void setTranscription_vi(String transcription_vi) {
        this.Transcription_vi = transcription_vi;
    }
}
