package com.example.luongvinh.englishforchange.Objects;

/**
 * Created by LUONGVINH on 6/3/2017.
 */

public class Data_Point_Part {
    int type,pos,percent,level;

    public Data_Point_Part(int type, int pos, int percent, int level) {
        this.type = type;
        this.pos = pos;
        this.percent = percent;
        this.level = level;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getPos() {
        return pos;
    }

    public void setPos(int pos) {
        this.pos = pos;
    }

    public int getPercent() {
        return percent;
    }

    public void setPercent(int percent) {
        this.percent = percent;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }
}
