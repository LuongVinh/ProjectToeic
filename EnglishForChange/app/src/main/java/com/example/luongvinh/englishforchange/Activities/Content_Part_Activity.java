package com.example.luongvinh.englishforchange.Activities;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.luongvinh.englishforchange.Adapters.List_ResultDetail_FullTest_Adapter;
import com.example.luongvinh.englishforchange.Fragments.FragmentShowDetailResult;
import com.example.luongvinh.englishforchange.Fragments.Fragment_Part1_Pager;
import com.example.luongvinh.englishforchange.Fragments.Fragment_Part3_Pager;
import com.example.luongvinh.englishforchange.Fragments.Fragment_Part5_Pager;
import com.example.luongvinh.englishforchange.Fragments.Fragment_Part6_Pager;
import com.example.luongvinh.englishforchange.Fragments.Fragment_Part7_Pager;
import com.example.luongvinh.englishforchange.Fragments.Fragment_Show_Result;
import com.example.luongvinh.englishforchange.Fragments.Fragment_Show_Result_Fulltest;
import com.example.luongvinh.englishforchange.Objects.Question_Part1_Part2;
import com.example.luongvinh.englishforchange.Objects.Question_Part3;
import com.example.luongvinh.englishforchange.Objects.Question_Part4;
import com.example.luongvinh.englishforchange.Objects.Question_Part5;
import com.example.luongvinh.englishforchange.Objects.Question_Part6;
import com.example.luongvinh.englishforchange.Objects.Question_Part7;
import com.example.luongvinh.englishforchange.R;
import com.example.luongvinh.englishforchange.Until.Constan;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 * Created by LUONGVINH on 4/27/2017.
 */
public class Content_Part_Activity extends AppCompatActivity {

    private int position;
    protected int typeHomeMenu;
    private int type,level=1;
    private FrameLayout contain;
    private FragmentManager fragmentmanager;
    private int[] reviewPart1 = new int[200];
    private Fragment_Part1_Pager.AnswerChoise answerChoise;
    private Fragment_Part1_Pager fragmentPart1;
    private Fragment_Part3_Pager fragmentPart3;
    private Fragment_Part5_Pager fragmentPart5;
    private Fragment_Part6_Pager fragmentPart6;
    private Fragment_Part7_Pager fragmentPart7;
    private ArrayList<Question_Part1_Part2> listQuestionPart1;
    private ArrayList<Question_Part1_Part2> listQuestionPart2;
    private ArrayList<Question_Part3> listQuestionPart3;
    private ArrayList<Question_Part4> listQuestionPart4;
    private ArrayList<Question_Part5> listQuestionPart5;
    private ArrayList<Question_Part6> listQuestionPart6;
    private ArrayList<Question_Part7> listQuestionPart7;
    private Toolbar toolbar;
    private ImageView imgBack, imv_clock;
    private TextView tv_part, tv_timer;
    private ArrayList<File> listFilePNG;
    private Boolean full = false;
    private RecyclerView rcv;
    private ArrayList<String> arr_part_name = new ArrayList<>();
    private List_ResultDetail_FullTest_Adapter adapter;
    //mảng lưu số câu đúng của từng part trong phần thi full test
    private int[] point = new int[10];
    private CountDownTimer Timer;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_part_activity);
        rcv = (RecyclerView) findViewById(R.id.rcv_detail_result_fulltest);
        arr_part_name.add("Part1");
        arr_part_name.add("Part2");
        arr_part_name.add("Part3");
        arr_part_name.add("Part4");
        arr_part_name.add("Part5");
        arr_part_name.add("Part6");
        arr_part_name.add("Part7");
        adapter = new List_ResultDetail_FullTest_Adapter(this, arr_part_name);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        rcv.setHasFixedSize(true);
        rcv.setLayoutManager(layoutManager);
        rcv.setAdapter(adapter);
        rcv.setVisibility(View.GONE);

        Intent intent = getIntent();
        if (intent != null) {
            position = intent.getBundleExtra("DATA").getInt(Constan.POSITION_OF_TEST);
            type = intent.getBundleExtra("DATA").getInt(Constan.TYPE);
            full = intent.getBundleExtra("DATA").getBoolean(Constan.FULL);
            level = intent.getBundleExtra("DATA").getInt(Constan.LEVEL);
        }
        initActionbar();

        contain = (FrameLayout) findViewById(R.id.contain);
        fragmentmanager = getSupportFragmentManager();
        showFragment();


    }


    // lưu điểm của từng part vào mảng point
    public void addPoint(int p, int t) {
        point[t - 1] = p;
    }

    public int[] getPoint() {
        return point;
    }

    public void setPoint(int[] point) {
        this.point = point;
    }

    public Fragment_Part1_Pager.AnswerChoise getAnswerChoise() {
        return answerChoise;
    }

    public void setAnswerChoise(Fragment_Part1_Pager.AnswerChoise answerChoise) {
        this.answerChoise = answerChoise;
        switch (type) {
            case 1:
                fragmentPart1.setAnswerChoise(this.answerChoise);
                break;
            case 2:
                fragmentPart1.setAnswerChoise(this.answerChoise);
                break;
            case 3:
                fragmentPart3.setAnswerChoise(this.answerChoise);
                break;
            case 4:
                fragmentPart3.setAnswerChoise(this.answerChoise);
                break;
            case 5:
                fragmentPart5.setAnswerChoise(this.answerChoise);
                break;
            case 6:
                fragmentPart6.setAnswerChoise(this.answerChoise);
                break;
            case 7:
                fragmentPart7.setAnswerChoise(this.answerChoise);
                break;
        }

    }


    public void setTextTitleBar(String page) {
        tv_part.setText(page);
    }

    public void initActionbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        imgBack = (ImageView) toolbar.findViewById(R.id.imgBtnBack);
        imv_clock = (ImageView) toolbar.findViewById(R.id.imv_clock);

        tv_part = (TextView) toolbar.findViewById(R.id.tv_title_detail_part);
        tv_timer = (TextView) toolbar.findViewById(R.id.tv_level);
        if (full == true) {
            tv_timer.setVisibility(View.VISIBLE);
            imv_clock.setVisibility(View.VISIBLE);
            tv_timer.setTextColor(Color.RED);
            tv_timer.setText("120:00");
            Timer = new CountDownTimer(7200000, 1000) {
                @Override
                public void onTick(long millisUntilFinished) {
                    String minute = String.valueOf(millisUntilFinished / 60000);
                    String second = String.valueOf((millisUntilFinished % 60000) / 1000);
                    tv_timer.setText(minute + ":" + second);
                }

                @Override
                public void onFinish() {
                    tv_timer.setText("Hết giờ");
                }
            }.start();

        } else {
            tv_timer.setVisibility(View.GONE);
            imv_clock.setVisibility(View.GONE);
        }


        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();


    }


    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (typeHomeMenu == 1 && item.getItemId() == android.R.id.home) {
            if (getSupportFragmentManager().getBackStackEntryCount() > 0)
                getSupportFragmentManager().popBackStack();
            return true;
        } else {
            return false;
        }
    }


    public void setValueReview(int position, int review) {

        this.reviewPart1[position] = review;
    }

    public int getValueReview(int position) {

        return reviewPart1[position];

    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public ArrayList<Question_Part3> getListQuestionPart3() {
        return listQuestionPart3;
    }

    public void setListQuestionPart3(ArrayList<Question_Part3> listQuestionPart3) {
        this.listQuestionPart3 = listQuestionPart3;
    }

    public Question_Part3 getQuestion_Part3(int position) {
        return this.getListQuestionPart3().get(position);
    }

    public ArrayList<Question_Part1_Part2> getListQuestionPart1() {
        return listQuestionPart1;
    }

    public void setListQuestionPart1(ArrayList<Question_Part1_Part2> listQuestionPart1) {
        this.listQuestionPart1 = listQuestionPart1;
    }

    public ArrayList<Question_Part1_Part2> getListQuestionPart2() {
        return listQuestionPart2;
    }

    public void setListQuestionPart2(ArrayList<Question_Part1_Part2> listQuestionPart2) {
        this.listQuestionPart2 = listQuestionPart2;
    }

    public ArrayList<Question_Part4> getListQuestionPart4() {
        return listQuestionPart4;
    }

    public void setListQuestionPart4(ArrayList<Question_Part4> listQuestionPart4) {
        this.listQuestionPart4 = listQuestionPart4;
    }

    public Question_Part4 getQuestion_Part4(int position) {
        return this.getListQuestionPart4().get(position);
    }


    public ArrayList<Question_Part5> getListQuestionPart5() {
        return listQuestionPart5;
    }

    public void setListQuestionPart5(ArrayList<Question_Part5> listQuestionPart5) {
        this.listQuestionPart5 = listQuestionPart5;
    }


    public ArrayList<Question_Part6> getListQuestionPart6() {
        return listQuestionPart6;
    }

    public void setListQuestionPart6(ArrayList<Question_Part6> listQuestionPart6) {
        this.listQuestionPart6 = listQuestionPart6;
    }


    public ArrayList<Question_Part7> getListQuestionPart7() {
        return listQuestionPart7;
    }

    public void setListQuestionPart7(ArrayList<Question_Part7> listQuestionPart7) {
        this.listQuestionPart7 = listQuestionPart7;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    public void showFragment() {

        switch (type) {
            case 1:
                showDetailFragmentPart1();
                break;
            case 2:
                showDetailFragmentPart2();
                break;
            case 3:
                showDetailFragmentPart3();
                break;
            case 4:
                showDetailFragmentPart4();
                break;
            case 5:
                showDetailFragmentPart5();
                break;
            case 6:
                showDetailFragmentPart6();
                break;
            case 7:
                showDetailFragmentPart7();
                break;
        }

    }

    public Boolean getFull() {
        return full;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    private void showDetailFragmentPart1() {
        fragmentPart1 = Fragment_Part1_Pager.newInstance(position, type);
        fragmentPart1.setAnswerChoise(answerChoise);
        fragmentmanager
                .beginTransaction()
                .replace(R.id.contain, fragmentPart1, Fragment_Part1_Pager.class.toString())
                .addToBackStack(Fragment_Part1_Pager.class.toString())
                .commit();
    }

    private void showDetailFragmentPart2() {
        fragmentPart1 = Fragment_Part1_Pager.newInstance(position, type);
        fragmentmanager
                .beginTransaction()
                .replace(R.id.contain, fragmentPart1, Fragment_Part1_Pager.class.toString())
                .addToBackStack(Fragment_Part1_Pager.class.toString())
                .commit();
    }

    public void showDetailFragmentPart3() {
        fragmentPart3 = Fragment_Part3_Pager.newInstance(position, type);

        fragmentmanager
                .beginTransaction()
                .replace(R.id.contain, fragmentPart3, Fragment_Part3_Pager.class.toString())
                .addToBackStack(Fragment_Part3_Pager.class.toString())
                .commit();

    }

    public void showDetailFragmentPart4() {
        fragmentPart3 = Fragment_Part3_Pager.newInstance(position, type);

        fragmentmanager
                .beginTransaction()
                .replace(R.id.contain, fragmentPart3, Fragment_Part3_Pager.class.toString())
                .addToBackStack(Fragment_Part3_Pager.class.toString())
                .commit();

    }

    public void showDetailFragmentPart5() {
        fragmentPart5 = Fragment_Part5_Pager.newInstance(position, type);
        fragmentmanager
                .beginTransaction()
                .replace(R.id.contain, fragmentPart5, Fragment_Part5_Pager.class.toString())
                .addToBackStack(Fragment_Part5_Pager.class.toString())
                .commit();
    }

    public void showDetailFragmentPart6() {
        fragmentPart6 = Fragment_Part6_Pager.newInstance(position, type);
        fragmentmanager
                .beginTransaction()
                .replace(R.id.contain, fragmentPart6, Fragment_Part6_Pager.class.toString())
                .addToBackStack(Fragment_Part6_Pager.class.toString())
                .commit();
    }

    public void showDetailFragmentPart7() {
        fragmentPart7 = Fragment_Part7_Pager.newInstance(position, type);
        fragmentmanager
                .beginTransaction()
                .replace(R.id.contain, fragmentPart7, Fragment_Part7_Pager.class.toString())
                .addToBackStack(Fragment_Part7_Pager.class.toString())
                .commit();
    }

    public void showResultFullTest(int pointlistening, int pointraeding) {
        rcv.setVisibility(View.GONE);
        Fragment_Show_Result_Fulltest fragment_show_result_fulltest = Fragment_Show_Result_Fulltest.newInstance(position, pointlistening, pointraeding);
        fragmentmanager.beginTransaction()
                .replace(R.id.contain, fragment_show_result_fulltest, Fragment_Show_Result_Fulltest.class.toString())
                .addToBackStack(Fragment_Show_Result_Fulltest.class.toString())
                .commit();


    }

    public void showFragmentShowResult(int point) {
        Fragment_Show_Result fragment = Fragment_Show_Result.newInstance(position, type, point);
        fragmentmanager
                .beginTransaction()
                .replace(R.id.contain, fragment, Fragment_Show_Result.class.toString())
                .addToBackStack(Fragment_Show_Result.class.toString())
                .commit();


    }

    public void showFragmentDetailResult() {
        if (full == true) {
            rcv.setVisibility(View.VISIBLE);

        }
        FragmentShowDetailResult fragment = FragmentShowDetailResult.newInstance(type);
        fragmentmanager
                .beginTransaction()
                .replace(R.id.contain, fragment, FragmentShowDetailResult.class.toString())
                .addToBackStack(FragmentShowDetailResult.class.toString())
                .commit();
    }


    public static Boolean unzip(String sourceFile, String destinationFolder) {
        ZipInputStream zis = null;

        try {
            zis = new ZipInputStream(new BufferedInputStream(new FileInputStream(sourceFile)));
            ZipEntry ze;
            int count;
            byte[] buffer = new byte[8192];
            while ((ze = zis.getNextEntry()) != null) {
                String fileName = ze.getName();
                fileName = fileName.substring(fileName.indexOf("/") + 1);
                File file = new File(destinationFolder, fileName);
                File dir = ze.isDirectory() ? file : file.getParentFile();

                if (!dir.isDirectory() && !dir.mkdirs())
                    throw new FileNotFoundException("Invalid path: " + dir.getAbsolutePath());
                if (ze.isDirectory()) continue;
                FileOutputStream fout = new FileOutputStream(file);
                try {
                    while ((count = zis.read(buffer)) != -1)
                        fout.write(buffer, 0, count);
                } finally {
                    fout.close();
                }

            }
        } catch (IOException ioe) {
            return false;
        } finally {
            if (zis != null)
                try {
                    zis.close();
                } catch (IOException e) {

                }
        }
        return true;
    }


    public int[] getReviewPart1() {
        return reviewPart1;
    }


    public Boolean checkFileExit(String nameTest) {
        File file = new File(Environment.getExternalStorageDirectory() + "/EnglishForChange/Easy/" + nameTest + "/Part" + type + "/Part" + type + ".zip");
        if (file.exists()) return true;
        return false;
    }

    public ArrayList<File> getListFilePNG() {
        return listFilePNG;
    }

    public void setListFilePNG(ArrayList<File> listFilePNG) {
        this.listFilePNG = listFilePNG;
    }
}
