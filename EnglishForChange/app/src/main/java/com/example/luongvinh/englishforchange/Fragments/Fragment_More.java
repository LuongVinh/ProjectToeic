package com.example.luongvinh.englishforchange.Fragments;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.luongvinh.englishforchange.Activities.MainActivity;
import com.example.luongvinh.englishforchange.Adapters.ListOption_Adapter;
import com.example.luongvinh.englishforchange.Objects.Option;
import com.example.luongvinh.englishforchange.R;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

/**
 * Created by LUONGVINH on 4/4/2017.
 */
public class Fragment_More extends Fragment {
    private ArrayList<Option> optionArrayList;
    private ListOption_Adapter adapter;
    private ListView lv_option;
    private LoginButton loginButton;
    private CallbackManager callbackManager;
    private FirebaseDatabase firebaseDatabase;
    private DatabaseReference myDATA;
    private String a;
    private Context mContext;
    private ShareDialog shareDialog;
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext=getContext();

        FacebookSdk.sdkInitialize(getActivity().getApplicationContext());
        callbackManager=CallbackManager.Factory.create();
        shareDialog = new ShareDialog(this);

        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        // App code
                    }

                    @Override
                    public void onCancel() {
                        // App code
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        // App code
                    }
                });

          getData();
    }

    private void getData() {

        firebaseDatabase = FirebaseDatabase.getInstance();
        myDATA = firebaseDatabase.getReference("Easy");
        myDATA.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                a = dataSnapshot.getValue().toString();

                Log.e("aaaa",a+"");



            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        /*loginButton = (LoginButton) view.findViewById(R.id.login_button);
        loginButton.setReadPermissions("email");
        loginButton.setFragment(this);
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Toast.makeText(getActivity(), "Đăng nhập thành công", Toast.LENGTH_SHORT).show();
                AccessToken accessToken=loginResult.getAccessToken();
                Profile profile= Profile.getCurrentProfile();
            }

            @Override
            public void onCancel() {
                // App code
            }

            @Override
            public void onError(FacebookException exception) {
                // App code
                Toast.makeText(getActivity(), "Đăng nhập thất bại"+exception+"", Toast.LENGTH_SHORT).show();
                Log.e("err",exception+"");

            }
        });
*/
        ((MainActivity)mContext).initActionbar(true);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //FacebookSdk.sdkInitialize(getActivity());

        View view = inflater.inflate(R.layout.activity_more, container, false);

        lv_option = (ListView) view.findViewById(R.id.lv_option);
        optionArrayList = new ArrayList<>();
        addData();
        adapter = new ListOption_Adapter(getActivity(), R.layout.item_list_option, optionArrayList);
        lv_option.setAdapter(adapter);


        lv_option.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(position==0){

                    if (ShareDialog.canShow(ShareLinkContent.class)) {
                        // Ví dụ với link. Video và ảnh tương tự
                        ShareLinkContent linkContent = new ShareLinkContent.Builder()
                                .setContentTitle("Tự học android")
                                .setContentDescription(
                                        "Tự học android hướng dẫn cách dùng Facebook SDK")
                                .setContentUrl(Uri.parse("https://play.google.com/store/apps/details?id=tienganh.audiobook.android.captocthehienanhviet"))
                                .build();

                        shareDialog.show(linkContent);
                    }

                }
            }
        });




        return view;

    }

    @Override
    public void onResume() {
        super.onResume();
        ((MainActivity)getContext()).setTitleActionBar("Mở Rộng");
    }

    private void addData() {
        optionArrayList.clear();
        Option o1 = new Option("Chia sẻ với bạn bè");
        Option o2 = new Option("Đánh giá ứng dụng");
        Option o3 = new Option("Góp ý với chúng tôi");
        Option o4 = new Option("Ứng dụng khác có thể bạn thích");
        optionArrayList.add(o1);
        optionArrayList.add(o2);
        optionArrayList.add(o3);
        optionArrayList.add(o4);
    }
}
