package com.example.luongvinh.englishforchange.Adapters;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.example.luongvinh.englishforchange.Activities.Content_Part_Activity;
import com.example.luongvinh.englishforchange.R;

import java.util.ArrayList;

/**
 * Created by LUONGVINH on 5/27/2017.
 */

public class List_ResultDetail_FullTest_Adapter extends RecyclerView.Adapter<List_ResultDetail_FullTest_Adapter.ViewHolder> {

    private final ArrayList<String> arr_part_name;
    // Store the context for easy access
    private Context mContext;
    private Boolean full=true;
    private FrameLayout contain;


    public List_ResultDetail_FullTest_Adapter(Context context, ArrayList<String> list) {
        this.arr_part_name = new ArrayList<>(list);
        this.mContext = context;

    }

    @Override
    public List_ResultDetail_FullTest_Adapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View inflatedView = LayoutInflater.from(mContext)
                .inflate(R.layout.item_result_detail_fulltest, viewGroup, false);
        return new ViewHolder(inflatedView, mContext, arr_part_name);

    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        String name = arr_part_name.get(i);
        TextView tv_part_name = viewHolder.tv_part_name;
        tv_part_name.setText(name);

    }



    @Override
    public int getItemCount() {
        return 7;
    }


    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private CardView cv;
        private Context context;
        private TextView tv_part_name;
        private ArrayList<String> m_ArrayList = new ArrayList<>();

        public ViewHolder(View itemView, Context context, ArrayList<String> arrayList) {
            super(itemView);
            this.context = context;
            this.m_ArrayList = arrayList;
            cv = (CardView) itemView.findViewById(R.id.cardview);
            tv_part_name = (TextView) itemView.findViewById(R.id.tv_part_name);
            itemView.setOnClickListener(this);
        }


        @Override
        public void onClick(View v) {
            int  pos= getAdapterPosition();
            ((Content_Part_Activity) context).setType(pos+1);
            ((Content_Part_Activity) context).showFragmentDetailResult();


        }
    }
}

