package com.example.luongvinh.englishforchange.Objects;

/**
 * Created by LUONGVINH on 4/4/2017.
 */
public class Video {
    private int imv_video;
    private String num_video;

    public Video(int imv_video, String num_video) {
        this.imv_video = imv_video;
        this.num_video = num_video;
    }

    public int getImv_video() {
        return imv_video;
    }

    public void setImv_video(int imv_video) {
        this.imv_video = imv_video;
    }

    public String getNum_video() {
        return num_video;
    }

    public void setNum_video(String num_video) {
        this.num_video = num_video;
    }
}
