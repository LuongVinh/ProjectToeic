package com.example.luongvinh.englishforchange.Objects;

/**
 * Created by LUONGVINH on 5/26/2017.
 */

public class Item_List_DetailTest {
    private int icon;
    private String style_name,time,num_question;

    public Item_List_DetailTest(int icon, String style_name, String time, String num_question) {
        this.icon = icon;
        this.style_name = style_name;
        this.time = time;
        this.num_question = num_question;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public String getStyle_name() {
        return style_name;
    }

    public void setStyle_name(String style_name) {
        this.style_name = style_name;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getNum_question() {
        return num_question;
    }

    public void setNum_question(String num_question) {
        this.num_question = num_question;
    }
}
