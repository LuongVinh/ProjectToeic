package com.example.luongvinh.englishforchange.Fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.luongvinh.englishforchange.Activities.Content_Part_Activity;
import com.example.luongvinh.englishforchange.Objects.Data_Point_Part;
import com.example.luongvinh.englishforchange.R;
import com.example.luongvinh.englishforchange.Until.Constan;
import com.github.lzyzsd.circleprogress.ArcProgress;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by LUONGVINH on 5/4/2017.
 */
public class Fragment_Show_Result extends Fragment {
    private View root;
    private int point,size;
    private Context mContext;
    private int type,position;
    private ArcProgress arcProgress;
    private Button btn_Result;
    private RecyclerView rcv;
    private int percent=0,i=0;
    private ArrayList<Data_Point_Part> arr__dataPointPart;
    private int level=1;

    public static Fragment_Show_Result newInstance(int position, int type, int point) {
        Fragment_Show_Result fragment = new Fragment_Show_Result();
        Bundle bundle = new Bundle();
        bundle.putInt(Constan.POSITION_OF_TEST, position);
        bundle.putInt(Constan.TYPE, type);
        bundle.putInt(Constan.POINT, point);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        root = LayoutInflater.from(mContext).inflate(R.layout.fragment_show_result, container, false);
        arcProgress = (ArcProgress) root.findViewById(R.id.arc_progress_result);
        btn_Result = (Button) root.findViewById(R.id.btn_result);
        arr__dataPointPart =new ArrayList<>();
        btn_Result.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ((Content_Part_Activity) mContext).showFragmentDetailResult();

            }
        });
        switch (type) {
            case 1:
                percent=point * 10;
                arcProgress.setProgress(percent);
                break;
            case 2:
                percent=((point * 100) / 30);
                arcProgress.setProgress(percent);
                break;
            case 3:
                percent=((point * 100) / 30);
                arcProgress.setProgress(percent);
                break;
            case 4:
                percent=((point * 100) / 30);
                arcProgress.setProgress(percent);
                break;
            case 5:
                percent=((point * 100) / 40);
                arcProgress.setProgress(percent);
                break;
            case 6:
                percent=((point * 100) / 12);
                arcProgress.setProgress(percent);
                break;
            case 7:
                percent=((point * 100) / 48);
                arcProgress.setProgress(percent);
                break;
        }


        return root;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getContext();
        point = getArguments().getInt(Constan.POINT);
        type = getArguments().getInt(Constan.TYPE);
        position=getArguments().getInt(Constan.POSITION_OF_TEST);

        switch (type) {
            case 1:
                size=10;
                break;
            case 2:
                size=30;
                break;
            case 3:
                size=30;
                break;
            case 4:
                size=30;
                break;
            case 5:
                size=40;
                break;
            case 6:
                size=12;
                break;
            case 7:
                size=48;
                break;
        }
        ((Content_Part_Activity) mContext).setTextTitleBar("Kết Quả");

    }

    @Override
    public void onPause() {
        super.onPause();
        level=((Content_Part_Activity)mContext).getLevel();
        Data_Point_Part p =new Data_Point_Part(type,position,percent,level);
        arr__dataPointPart.add(p);
        Gson gson=new Gson();
        String jsArray=gson.toJson(arr__dataPointPart);



        SharedPreferences pre=getContext().getSharedPreferences("my_data", MODE_PRIVATE);
        SharedPreferences.Editor edit=pre.edit();
        edit.putString("data_percent",jsArray+"");
        edit.commit();
    }

    @Override
    public void onResume() {
        super.onResume();
        SharedPreferences pre = getContext().getSharedPreferences("my_data", MODE_PRIVATE);
        String jArray = pre.getString("data_percent", null);
        if(jArray!=null){
            Type type1 = new TypeToken<ArrayList<Data_Point_Part>>() {}.getType();
            Gson gson = new Gson();
            arr__dataPointPart = gson.fromJson(jArray, type1);
        }

    }
}
