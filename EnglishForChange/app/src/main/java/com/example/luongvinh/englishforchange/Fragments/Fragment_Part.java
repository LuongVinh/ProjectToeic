package com.example.luongvinh.englishforchange.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.luongvinh.englishforchange.Activities.DetailPart_Activity;
import com.example.luongvinh.englishforchange.Activities.MainActivity;
import com.example.luongvinh.englishforchange.Adapters.ListPart_Adapter;
import com.example.luongvinh.englishforchange.Objects.Part;
import com.example.luongvinh.englishforchange.R;
import com.example.luongvinh.englishforchange.Until.Constan;

import java.util.ArrayList;

/**
 * Created by LUONGVINH on 4/4/2017.
 */
public class Fragment_Part extends Fragment {
    private ListPart_Adapter adapter;
    private ArrayList<Part> partArrayList;
    private ListView lv_part;
    private int position=0;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onResume() {
        super.onResume();
        ((MainActivity)getContext()).setTitleActionBar("Luyện thi tiêu chuẩn");
        ((MainActivity)getContext()).initActionbar(true);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_part, container, false);
        lv_part=(ListView) view.findViewById(R.id.lv_part);
        partArrayList=new ArrayList<>();
        addData();
        adapter= new ListPart_Adapter(getActivity(),R.layout.item_list_part,partArrayList);
        lv_part.setAdapter(adapter);
        lv_part.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                position=i;
                Intent intent=new Intent(getContext(),DetailPart_Activity.class);
                Bundle bundle =new Bundle();
                bundle.putInt(Constan.TYPE,(position+1));

                intent.putExtra("DATA",bundle);
                startActivity(intent);

            }
        });



        return view;

    }

    private void addData() {
        partArrayList.clear();
        Part p1= new Part(R.drawable.ic_part_one,"PHOTO","Detail_Part listening and choosing what best describes the photo","Listening");
        Part p2= new Part(R.drawable.ic_part_two,"QUESTION AND RESPONSE","Detail_Part listening and immediately response","Listening");
        Part p3= new Part(R.drawable.ic_part_three,"SHORT TALK","Detail_Part listening and summarizing ideas of a talk","Listening");
        Part p4= new Part(R.drawable.ic_part_four,"SHORT CONVERSATION","Detail_Part listening and summarizing ideas of a conversation","Listening");
        Part p5= new Part(R.drawable.ic_part_five,"INCOMPLETE SENTENCE","Grow up your grammar and vocabulary","Reading");
        Part p6= new Part(R.drawable.ic_part_six,"INCOMPLETE TEXT","Grow up your reading skill and grammar","Reading");
        Part p7= new Part(R.drawable.ic_part_seven,"READING COMPREHENSION","Grow up your reading skill","Reading");
        partArrayList.add(p1);
        partArrayList.add(p2);
        partArrayList.add(p3);
        partArrayList.add(p4);
        partArrayList.add(p5);
        partArrayList.add(p6);
        partArrayList.add(p7);

    }
}
