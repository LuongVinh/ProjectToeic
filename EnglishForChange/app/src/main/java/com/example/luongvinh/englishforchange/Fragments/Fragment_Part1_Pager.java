package com.example.luongvinh.englishforchange.Fragments;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.luongvinh.englishforchange.Activities.Content_Part_Activity;
import com.example.luongvinh.englishforchange.Adapters.ViewPagerPart1Adapter;
import com.example.luongvinh.englishforchange.Objects.Question_Part1_Part2;
import com.example.luongvinh.englishforchange.R;
import com.example.luongvinh.englishforchange.Until.Constan;
import com.example.luongvinh.englishforchange.Until.Utilities;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import static com.example.luongvinh.englishforchange.Activities.Content_Part_Activity.unzip;

/**
 * Created by LUONGVINH on 4/27/2017.
 */
public class Fragment_Part1_Pager extends Fragment {
    private ViewPager pager;
    private ViewPagerPart1Adapter pagerAdapter;
    private int position, isPlaying = 0;
    private View root;
    private Button btn_play;
    private TextView tv_realtime, tv_totaltime;
    private Context mContext;
    private SeekBar mSeekBar;
    private int type;
    private FirebaseStorage storage;
    private StorageReference reference;
    private ProgressBar prg;
    private TextView txt_percent;
    private Dialog dialog;
    private ArrayList<File> listFilePNG = new ArrayList<>();
    private File mp3File;
    private MediaPlayer mediaPlayer;
    private int current;
    private AnswerChoise answerChoise;
    private Handler mHandler;
    private Utilities utils;
    private ArrayList<String> arr_answer_key;
    private ArrayList<Question_Part1_Part2> arr_p1_p2;
    private Boolean full = false;
    private FirebaseDatabase firebaseDatabase;
    private DatabaseReference myDATA;

    public static Fragment_Part1_Pager newInstance(int position, int type) {
        Fragment_Part1_Pager fragment = new Fragment_Part1_Pager();
        Bundle bundle = new Bundle();
        bundle.putInt(Constan.POSITION_OF_TEST, position);
        bundle.putInt(Constan.TYPE, type);
        fragment.setArguments(bundle);
        return fragment;

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getContext();
        storage = FirebaseStorage.getInstance();
        reference = storage.getReference();
        mediaPlayer = new MediaPlayer();
        position = getArguments().getInt(Constan.POSITION_OF_TEST);
        type = getArguments().getInt(Constan.TYPE);
        mHandler = new Handler();
        utils = new Utilities();
        arr_answer_key = new ArrayList<>();
        arr_p1_p2 = new ArrayList<>();
        full = ((Content_Part_Activity) mContext).getFull();

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (root == null) {
            root = LayoutInflater.from(mContext).inflate(R.layout.fragment_part1_pager, container, false);
            pager = (ViewPager) root.findViewById(R.id.view_pager);
            btn_play = (Button) root.findViewById(R.id.btn_play);
            tv_realtime = (TextView) root.findViewById(R.id.tv_realtime);
            tv_totaltime = (TextView) root.findViewById(R.id.tv_totaltime);
            mSeekBar = (SeekBar) root.findViewById(R.id.seekbar);

            if (((Content_Part_Activity) mContext).checkFileExit("Test" + 1)) {
                try {
                    getDataByCache();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
                getmp3ByServer();

            }
            getDataFromServer();

            setTextTitleBar(0);
        }

        //khi thay doi page thi set lai dap an nguoi dung lưu
        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

//                answerChoise.setAnswerChoise(position);
                if (type == 1) {
                    setTextTitleBar(position );
                    answerChoise.setAnswerChoise(position);
                } else if (type == 2) {
                    if (full == true) {
                        setTextTitleBar( position);
                        answerChoise.setAnswerChoise(10 + position);
                    } else {
                        setTextTitleBar(position );
                        answerChoise.setAnswerChoise(position);
                    }
                }

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        btn_play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isPlaying == 0) {
                    btn_play.setBackgroundResource(R.drawable.btn_play);
                    isPlaying = 1;
                    mediaPlayer.pause();
                } else if (isPlaying == 1) {
                    btn_play.setBackgroundResource(R.drawable.btn_pause);
                    isPlaying = 0;
                    mediaPlayer.start();

                }
            }
        });

        return root;
    }

    private void getDataFromServer() {
        firebaseDatabase = FirebaseDatabase.getInstance();
        myDATA = firebaseDatabase.getReference("Easy/1");
        if (type == 1) {
            myDATA.child("Part1").addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    Question_Part1_Part2 question1 = null;
                    for (DataSnapshot data : dataSnapshot.getChildren()) {
                        question1 = data.getValue(Question_Part1_Part2.class);
                        arr_p1_p2.add(question1);
                    }
                    //gán arraylist vừa tải về cho arraylist của activity để có thể sử dụng cho các fragment khác cùng activity
                    ((Content_Part_Activity) mContext).setListQuestionPart1(arr_p1_p2);

                    int i;
                    for (i = 0; i < 10; i++) {
                        Question_Part1_Part2 q = arr_p1_p2.get(i);
                        String a = q.getAnswer();
                        arr_answer_key.add(a);

                    }

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        } else if (type == 2) {

            myDATA.child("Part2").addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    Question_Part1_Part2 question2 = null;
                    for (DataSnapshot data : dataSnapshot.getChildren()) {
                        question2 = data.getValue(Question_Part1_Part2.class);
                        arr_p1_p2.add(question2);
                    }
                    //gán arraylist vừa tải về cho arraylist của activity để có thể sử dụng cho các fragment khác cùng activity
                    ((Content_Part_Activity) mContext).setListQuestionPart2(arr_p1_p2);

                    int i;
                    for (i = 0; i < 30; i++) {
                        Question_Part1_Part2 q = arr_p1_p2.get(i);
                        String a = q.getAnswer();
                        arr_answer_key.add(a);

                    }

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

        }
    }

    private void setTextTitleBar(int current) {
        if (full == false) {
            if (type == 1) {
                ((Content_Part_Activity) mContext).setTextTitleBar(current+1 + "/10");

            } else if (type == 2) {
                ((Content_Part_Activity) mContext).setTextTitleBar(current+1 + "/30");

            }
        } else {
            if (type == 1) {
                ((Content_Part_Activity) mContext).setTextTitleBar(current+1 + "/200");

            } else if (type == 2) {
                ((Content_Part_Activity) mContext).setTextTitleBar(current + 11 + "/200");

            }
        }


    }


    @Override
    public void onResume() {
        super.onResume();
        if (!mediaPlayer.isPlaying()) {
            current = current - 1000;
            if (current >= 0)
                mediaPlayer.seekTo(current);

            mediaPlayer.start();
        }


    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);
    }

    private void getmp3ByServer() {
        showDowloadDialog();

        final String path = Environment.getExternalStorageDirectory() + "/EnglishForChange/Easy/Test" + 1 + "/Part" + type;
        final File dir = new File(path);

        if (!dir.exists()) dir.mkdirs();
        File file = new File(dir, "Part" + type + ".zip");
        reference.child("Easy/Test" + 1 + "/Part" + type + "/Part" + type + ".zip").getFile(file).addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {

                if (unzip((path + "/" + "Part" + type + ".zip"), path)) {

                    try {
                        getDataByCache();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else Toast.makeText(mContext, "Erro unzip", Toast.LENGTH_LONG).show();

            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(mContext, "Erro loading File mp3 " + e.toString(), Toast.LENGTH_LONG).show();


            }
        }).addOnProgressListener(new OnProgressListener<FileDownloadTask.TaskSnapshot>() {
            @Override
            public void onProgress(FileDownloadTask.TaskSnapshot taskSnapshot) {
                int progress = (int) (100 * (float) taskSnapshot.getBytesTransferred() / taskSnapshot.getTotalByteCount());
                prg.setProgress(progress);
                txt_percent.setText(progress + "%");


            }
        });


    }


    public void getDataByCache() throws IOException {
        if (dialog != null) dialog.dismiss();
        getListFilePNG();
        ((Content_Part_Activity) mContext).setListFilePNG(listFilePNG);
        pagerAdapter = new ViewPagerPart1Adapter(getChildFragmentManager(), 1, type, listFilePNG);
        pager.setAdapter(pagerAdapter);
        mediaPlayer.setDataSource(mp3File.getPath());
        mediaPlayer.prepare();
        mediaPlayer.start();
        setEventSeekBar();

    }

    private void setEventSeekBar() {
        mHandler.postDelayed(mUpdateTimeTask, 100);

    }

    private Runnable mUpdateTimeTask = new Runnable() {
        public void run() {
            long totalDuration = 0;
            try {
                totalDuration = mediaPlayer.getDuration();
            } catch (IllegalStateException e) {
                e.printStackTrace();
            }
            long currentDuration = 0;
            try {
                currentDuration = mediaPlayer.getCurrentPosition();
            } catch (IllegalStateException e) {
                e.printStackTrace();
            }
            //Hiển thị tổng thời gian
            tv_totaltime.setText("" + utils.milliSecondsToTimer(totalDuration));
            //Hiển thị thời gian hoàn thành
            tv_realtime.setText("" + utils.milliSecondsToTimer(currentDuration));

            //Thực hiện việc cập nhật progress bar
            int progress = (int) (utils.getProgressPercentage(currentDuration,
                    totalDuration));
            // Log.d("Progress", ""+progress);
            mSeekBar.setProgress(progress);
            //Chạy lại sau 0,1s
            mHandler.postDelayed(this, 100);
        }
    };

    public void getListFilePNG() {

        File file = new File(Environment.getExternalStorageDirectory() + "/EnglishForChange/Easy/Test" + 1 + "/Part" + type);
        File[] list = file.listFiles();
        for (File f : list) {
            String name = f.getName();

            if (name.endsWith(".PNG")) listFilePNG.add(f);
            else if (name.endsWith(".mp3")) mp3File = f;
        }

    }

    private void showDowloadDialog() {
        dialog = new Dialog(mContext);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.WHITE));
        final View inflateView = ((LayoutInflater) ((mContext)).getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.loading_dialog, null, false);
        dialog.setContentView(inflateView);
        prg = (ProgressBar) inflateView.findViewById(R.id.pb);
        txt_percent = (TextView) inflateView.findViewById(R.id.tvProgress);
        dialog.show();
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mediaPlayer.isPlaying()) {
            mediaPlayer.stop();
            //  current = mediaPlayer.getCurrentPosition();

        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mediaPlayer.isPlaying()) mediaPlayer.stop();
    }

    //interface thực hiện công việc set Đáp án đã chọn
    public interface AnswerChoise {
        void setAnswerChoise(int position);
    }

    public void setAnswerChoise(AnswerChoise answerChoise) {
        this.answerChoise = answerChoise;

    }

}
