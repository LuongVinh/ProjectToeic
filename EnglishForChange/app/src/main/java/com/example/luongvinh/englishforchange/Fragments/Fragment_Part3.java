package com.example.luongvinh.englishforchange.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.luongvinh.englishforchange.Activities.Content_Part_Activity;
import com.example.luongvinh.englishforchange.Objects.Question_Part3;
import com.example.luongvinh.englishforchange.Objects.Question_Part4;
import com.example.luongvinh.englishforchange.R;
import com.example.luongvinh.englishforchange.Until.Constan;

import java.util.ArrayList;

/**
 * Created by LUONGVINH on 4/29/2017.
 */
public class Fragment_Part3 extends Fragment implements Fragment_Part1_Pager.AnswerChoise {

    public int position, point = 0;
    private View root;
    private Context mContext;
    private int type;
    private Button btn_A, btn_B, btn_C, btn_D;

    private TextView tv_question;
    private ArrayList<Question_Part3> arrayList_p3;
    private ArrayList<Question_Part4> arrayList_p4;
    private int index;
    private int[] reviewPart;
    private Question_Part3 question_part3;
    private Question_Part4 question_part4;
    private int answerRight = 0;
    private Boolean full = false;

    public static Fragment_Part3 newInstance(int position, int index, int type, ArrayList arr) {
        Fragment_Part3 fragment = new Fragment_Part3();
        Bundle bundle = new Bundle();
        bundle.putInt(Constan.POSITION_OF_TEST, position);
        bundle.putInt(Constan.TYPE, type);
        bundle.putInt(Constan.INDEX, index);
        fragment.setArguments(bundle);

        return fragment;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getContext();
        arrayList_p3 = ((Content_Part_Activity) mContext).getListQuestionPart3();
        arrayList_p4 = ((Content_Part_Activity) mContext).getListQuestionPart4();
        full = ((Content_Part_Activity) mContext).getFull();
        // phân biệt test 1 test 2
        // phân biệt part ở trong 1 test
        type = getArguments().getInt(Constan.TYPE);
        index = getArguments().getInt(Constan.INDEX);
        position = getArguments().getInt(Constan.POSITION_OF_TEST);

    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        root = LayoutInflater.from(mContext).inflate(R.layout.fragment_part3, container, false);
        btn_A = (Button) root.findViewById(R.id.btn_A);
        btn_B = (Button) root.findViewById(R.id.btn_B);
        btn_C = (Button) root.findViewById(R.id.btn_C);
        btn_D = (Button) root.findViewById(R.id.btn_D);
        tv_question = (TextView) root.findViewById(R.id.tv_question);

        setEvent();
        setData(index);
        return root;
    }


    private View.OnClickListener clickAnswer = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (full == false) {
                switch (v.getId()) {
                    case R.id.btn_A: {
                        btn_A.setBackgroundResource(R.drawable.button_your_answer);
                        btn_B.setBackgroundResource(R.drawable.button_answer_normal);
                        btn_C.setBackgroundResource(R.drawable.button_answer_normal);
                        btn_D.setBackgroundResource(R.drawable.button_answer_normal);
                        ((Content_Part_Activity) mContext).setValueReview(index - 1, 1);
                    }
                    break;
                    case R.id.btn_B: {
                        btn_A.setBackgroundResource(R.drawable.button_answer_normal);
                        btn_B.setBackgroundResource(R.drawable.button_your_answer);
                        btn_C.setBackgroundResource(R.drawable.button_answer_normal);
                        btn_D.setBackgroundResource(R.drawable.button_answer_normal);
                        ((Content_Part_Activity) mContext).setValueReview(index - 1, 2);
                    }
                    break;
                    case R.id.btn_C: {
                        btn_A.setBackgroundResource(R.drawable.button_answer_normal);
                        btn_B.setBackgroundResource(R.drawable.button_answer_normal);
                        btn_C.setBackgroundResource(R.drawable.button_your_answer);
                        btn_D.setBackgroundResource(R.drawable.button_answer_normal);
                        ((Content_Part_Activity) mContext).setValueReview(index - 1, 3);
                    }
                    break;
                    case R.id.btn_D: {
                        btn_A.setBackgroundResource(R.drawable.button_answer_normal);
                        btn_B.setBackgroundResource(R.drawable.button_answer_normal);
                        btn_C.setBackgroundResource(R.drawable.button_answer_normal);
                        btn_D.setBackgroundResource(R.drawable.button_your_answer);
                        ((Content_Part_Activity) mContext).setValueReview(index - 1, 4);
                    }
                    break;
                }
            } else if (full == true) {
                if (type == 3) {
                    switch (v.getId()) {
                        case R.id.btn_A: {
                            btn_A.setBackgroundResource(R.drawable.button_your_answer);
                            btn_B.setBackgroundResource(R.drawable.button_answer_normal);
                            btn_C.setBackgroundResource(R.drawable.button_answer_normal);
                            btn_D.setBackgroundResource(R.drawable.button_answer_normal);
                            ((Content_Part_Activity) mContext).setValueReview(index +39, 1);
                        }
                        break;
                        case R.id.btn_B: {
                            btn_A.setBackgroundResource(R.drawable.button_answer_normal);
                            btn_B.setBackgroundResource(R.drawable.button_your_answer);
                            btn_C.setBackgroundResource(R.drawable.button_answer_normal);
                            btn_D.setBackgroundResource(R.drawable.button_answer_normal);
                            ((Content_Part_Activity) mContext).setValueReview(index +39, 2);
                        }
                        break;
                        case R.id.btn_C: {
                            btn_A.setBackgroundResource(R.drawable.button_answer_normal);
                            btn_B.setBackgroundResource(R.drawable.button_answer_normal);
                            btn_C.setBackgroundResource(R.drawable.button_your_answer);
                            btn_D.setBackgroundResource(R.drawable.button_answer_normal);
                            ((Content_Part_Activity) mContext).setValueReview(index +39, 3);
                        }
                        break;
                        case R.id.btn_D: {
                            btn_A.setBackgroundResource(R.drawable.button_answer_normal);
                            btn_B.setBackgroundResource(R.drawable.button_answer_normal);
                            btn_C.setBackgroundResource(R.drawable.button_answer_normal);
                            btn_D.setBackgroundResource(R.drawable.button_your_answer);
                            ((Content_Part_Activity) mContext).setValueReview(index +39, 4);
                        }
                        break;
                    }
                } else if (type == 4) {
                    switch (v.getId()) {
                        case R.id.btn_A: {
                            btn_A.setBackgroundResource(R.drawable.button_your_answer);
                            btn_B.setBackgroundResource(R.drawable.button_answer_normal);
                            btn_C.setBackgroundResource(R.drawable.button_answer_normal);
                            btn_D.setBackgroundResource(R.drawable.button_answer_normal);
                            ((Content_Part_Activity) mContext).setValueReview(index +69, 1);
                        }
                        break;
                        case R.id.btn_B: {
                            btn_A.setBackgroundResource(R.drawable.button_answer_normal);
                            btn_B.setBackgroundResource(R.drawable.button_your_answer);
                            btn_C.setBackgroundResource(R.drawable.button_answer_normal);
                            btn_D.setBackgroundResource(R.drawable.button_answer_normal);
                            ((Content_Part_Activity) mContext).setValueReview(index +69, 2);
                        }
                        break;
                        case R.id.btn_C: {
                            btn_A.setBackgroundResource(R.drawable.button_answer_normal);
                            btn_B.setBackgroundResource(R.drawable.button_answer_normal);
                            btn_C.setBackgroundResource(R.drawable.button_your_answer);
                            btn_D.setBackgroundResource(R.drawable.button_answer_normal);
                            ((Content_Part_Activity) mContext).setValueReview(index +69, 3);
                        }
                        break;
                        case R.id.btn_D: {
                            btn_A.setBackgroundResource(R.drawable.button_answer_normal);
                            btn_B.setBackgroundResource(R.drawable.button_answer_normal);
                            btn_C.setBackgroundResource(R.drawable.button_answer_normal);
                            btn_D.setBackgroundResource(R.drawable.button_your_answer);
                            ((Content_Part_Activity) mContext).setValueReview(index +69, 4);
                        }
                        break;
                    }
                }
            }

            if (index == 30) {
                reviewPart = ((Content_Part_Activity) mContext).getReviewPart1();
                if (full==false){
                    if(type==3){
                        for (int i = 0; i < arrayList_p3.size(); i++) {
                            question_part3 = arrayList_p3.get(i);
                            if (question_part3.getAnswer().equals("A") && reviewPart[i] == 1)
                                answerRight = answerRight + 1;
                            if (question_part3.getAnswer().equals("B") && reviewPart[i] == 2)
                                answerRight = answerRight + 1;
                            if (question_part3.getAnswer().equals("C") && reviewPart[i] == 3)
                                answerRight = answerRight + 1;
                            if (question_part3.getAnswer().equals("D") && reviewPart[i] == 4)
                                answerRight = answerRight + 1;
                        }


                    }else if(type==4){
                        for (int i = 0; i < arrayList_p4.size(); i++) {
                            question_part4 = arrayList_p4.get(i);
                            if (question_part4.getAnswer().equals("A") && reviewPart[i] == 1)
                                answerRight = answerRight + 1;
                            if (question_part4.getAnswer().equals("B") && reviewPart[i] == 2)
                                answerRight = answerRight + 1;
                            if (question_part4.getAnswer().equals("C") && reviewPart[i] == 3)
                                answerRight = answerRight + 1;
                            if (question_part4.getAnswer().equals("D") && reviewPart[i] == 4)
                                answerRight = answerRight + 1;
                        }
                    }
                }
                else if(full=true){
                    if(type==3){
                        for (int i = 0; i < arrayList_p3.size(); i++) {
                            question_part3 = arrayList_p3.get(i);
                            if (question_part3.getAnswer().equals("A") && reviewPart[i+40] == 1)
                                answerRight = answerRight + 1;
                            if (question_part3.getAnswer().equals("B") && reviewPart[i+40] == 2)
                                answerRight = answerRight + 1;
                            if (question_part3.getAnswer().equals("C") && reviewPart[i+40] == 3)
                                answerRight = answerRight + 1;
                            if (question_part3.getAnswer().equals("D") && reviewPart[i+40] == 4)
                                answerRight = answerRight + 1;
                        }
                    }else if(type==4){
                        for (int i = 0; i < arrayList_p4.size(); i++) {
                            question_part4 = arrayList_p4.get(i);
                            if (question_part4.getAnswer().equals("A") && reviewPart[i+70] == 1)
                                answerRight = answerRight + 1;
                            if (question_part4.getAnswer().equals("B") && reviewPart[i+70] == 2)
                                answerRight = answerRight + 1;
                            if (question_part4.getAnswer().equals("C") && reviewPart[i+70] == 3)
                                answerRight = answerRight + 1;
                            if (question_part4.getAnswer().equals("D") && reviewPart[i+70] == 4)
                                answerRight = answerRight + 1;
                        }
                    }
                }

                // công việc gửi số câu đúng sang màn hình show result
                if (full == false) {
                    ((Content_Part_Activity) mContext).showFragmentShowResult(answerRight);
                } else {
                    if (type == 3) {
                        ((Content_Part_Activity) mContext).addPoint(answerRight, type);
                        Log.e("Partt3", answerRight + "");
                        ((Content_Part_Activity) mContext).setType(4);
                        ((Content_Part_Activity) mContext).showFragment();
                    } else if (type == 4) {
                        ((Content_Part_Activity) mContext).addPoint(answerRight, type);
                        Log.e("Partt4", answerRight + "");
                        ((Content_Part_Activity) mContext).setType(5);
                        ((Content_Part_Activity) mContext).showFragment();
                    }


                }

            }
        }
    };

    private void setEvent() {
        btn_A.setOnClickListener(clickAnswer);
        btn_B.setOnClickListener(clickAnswer);
        btn_C.setOnClickListener(clickAnswer);
        btn_D.setOnClickListener(clickAnswer);


    }

    @Override
    public void onResume() {
        super.onResume();

        ((Content_Part_Activity) mContext).setAnswerChoise(this);
    }


    private void setData(int pos) {
        if (type == 3) {
            Question_Part3 q3 = ((Content_Part_Activity) mContext).getQuestion_Part3(pos - 1);

            String[] a = q3.getQue_en().split("\\(");
            tv_question.setText(a[0]);
            btn_A.setText(a[1]);
            btn_B.setText(a[2]);
            btn_C.setText(a[3]);
            btn_D.setText(a[4]);

        } else if (type == 4) {
            Question_Part4 q4 = ((Content_Part_Activity) mContext).getQuestion_Part4(pos - 1);

            String[] a = q4.getQue_en().split("\\(");
            tv_question.setText(a[0]);
            btn_A.setText(a[1]);
            btn_B.setText(a[2]);
            btn_C.setText(a[3]);
            btn_D.setText(a[4]);
        }


    }

    @Override
    public void setAnswerChoise(int position) {
        int answerChoise = ((Content_Part_Activity) mContext).getValueReview(position);
        switch (answerChoise) {
            case 1: {
                btn_A.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.button_your_answer));
            }
            break;
            case 2: {
                btn_B.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.button_your_answer));
            }
            break;
            case 3: {
                btn_C.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.button_your_answer));
            }
            break;
            case 4: {
                btn_D.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.button_your_answer));
            }
            break;
        }


    }


}
