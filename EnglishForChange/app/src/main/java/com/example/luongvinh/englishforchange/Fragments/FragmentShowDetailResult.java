package com.example.luongvinh.englishforchange.Fragments;

import android.content.Context;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.luongvinh.englishforchange.Activities.Content_Part_Activity;
import com.example.luongvinh.englishforchange.Objects.Question_Part1_Part2;
import com.example.luongvinh.englishforchange.Objects.Question_Part3;
import com.example.luongvinh.englishforchange.Objects.Question_Part4;
import com.example.luongvinh.englishforchange.Objects.Question_Part5;
import com.example.luongvinh.englishforchange.Objects.Question_Part6;
import com.example.luongvinh.englishforchange.Objects.Question_Part7;
import com.example.luongvinh.englishforchange.R;
import com.example.luongvinh.englishforchange.Until.Constan;
import com.example.luongvinh.englishforchange.Until.Utilities;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by LUONGVINH on 5/4/2017.
 */
public class FragmentShowDetailResult extends Fragment {

    private ArrayList<Question_Part1_Part2> listQuestionPart12;
    private ArrayList<Question_Part3> listQuestionPart3;
    private ArrayList<Question_Part4> listQuestionPart4;
    private ArrayList<Question_Part5> listQuestionPart5;
    private ArrayList<Question_Part6> listQuestionPart6;
    private ArrayList<Question_Part7> listQuestionPart7;

    private Question_Part1_Part2 questtion1;
    private Question_Part3 question3;
    private Question_Part4 question4;
    private Question_Part5 question5;
    private Question_Part6 question6;
    private Question_Part7 question7;

    private int[] listYourAnswer = new int[200];
    private Context mContext;
    private RelativeLayout rlt2;
    private WebView wv_conversation_en;
    private TextView tv_question_en, tv_question_vi;
    private int type, yourAnswer;
    private int sizeList = 0, current = 0, isPlaying = 0, num_question = 0;
    private Utilities utils;
    private MediaPlayer mediaPlayer;
    private String answer_key;
    private Handler mHandler;
    private File mp3File;
    private String a[] = null;
    private String b[] = null;
    private String conversation;
    private SeekBar mSeekBar;
    private TextView tv_realtime, tv_totaltime;
    private Button btn_play;
    private ArrayList<File> listFilePNG = new ArrayList<>();
    private ImageView imgPNG;
    private Button btn_A_en, btn_B_en, btn_C_en, btn_D_en, btn_A_vi, btn_B_vi, btn_C_vi, btn_D_vi, btn_next, btn_pre;
    private Boolean full = false;

    public static FragmentShowDetailResult newInstance(int type) {

        FragmentShowDetailResult fragmentShowDetailResult = new FragmentShowDetailResult();
        Bundle bundle = new Bundle();
        bundle.putInt(Constan.TYPE, type);
        fragmentShowDetailResult.setArguments(bundle);
        return fragmentShowDetailResult;


    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getContext();
        mHandler = new Handler();
        mediaPlayer = new MediaPlayer();
        utils = new Utilities();
        type = getArguments().getInt(Constan.TYPE);
        full = ((Content_Part_Activity) mContext).getFull();

        switch (type) {
            case 1:

//                listFilePNG = ((Content_Part_Activity) mContext).getListFilePNG();

                listQuestionPart12 = ((Content_Part_Activity) mContext).getListQuestionPart1();
                sizeList = listQuestionPart12.size();
                listYourAnswer = ((Content_Part_Activity) mContext).getReviewPart1();
                File file1 = new File(Environment.getExternalStorageDirectory() + "/EnglishForChange/Easy/Test" + 1 + "/Part" + 1);
                File[] list1 = file1.listFiles();
                for (File f : list1) {
                    String name = f.getName();

                    if (name.endsWith(".mp3")) mp3File = f;
                    else if (name.endsWith(".PNG")) listFilePNG.add(f);
                }
                try {
                    mediaPlayer.setDataSource(mp3File.getPath());
                } catch (IOException e) {
                    e.printStackTrace();
                }
                try {
                    mediaPlayer.prepare();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                mediaPlayer.start();
                mHandler.postDelayed(mUpdateTimeTask, 100);


                break;
            case 2:
                listQuestionPart12 = ((Content_Part_Activity) mContext).getListQuestionPart2();
                sizeList = listQuestionPart12.size();
                listYourAnswer = ((Content_Part_Activity) mContext).getReviewPart1();
                File file2 = new File(Environment.getExternalStorageDirectory() + "/EnglishForChange/Easy/Test" + 1 + "/Part" + 2);
                File[] list2 = file2.listFiles();
                for (File f : list2) {
                    String name = f.getName();

                    if (name.endsWith(".mp3")) mp3File = f;
                }
                try {
                    mediaPlayer.setDataSource(mp3File.getPath());
                } catch (IOException e) {
                    e.printStackTrace();
                }
                try {
                    mediaPlayer.prepare();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                mediaPlayer.start();
                mHandler.postDelayed(mUpdateTimeTask, 100);
                break;
            case 3:
                listQuestionPart3 = ((Content_Part_Activity) mContext).getListQuestionPart3();
                listYourAnswer = ((Content_Part_Activity) mContext).getReviewPart1();
                sizeList = listQuestionPart3.size();
                File file3 = new File(Environment.getExternalStorageDirectory() + "/EnglishForChange/Easy/Test" + 1 + "/Part" + 3);
                File[] list3 = file3.listFiles();
                for (File f : list3) {
                    String name = f.getName();

                    if (name.endsWith(".mp3")) mp3File = f;
                }
                try {
                    mediaPlayer.setDataSource(mp3File.getPath());
                } catch (IOException e) {
                    e.printStackTrace();
                }
                try {
                    mediaPlayer.prepare();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                mediaPlayer.start();
                mHandler.postDelayed(mUpdateTimeTask, 100);
                break;
            case 4:
                listQuestionPart4 = ((Content_Part_Activity) mContext).getListQuestionPart4();
                listYourAnswer = ((Content_Part_Activity) mContext).getReviewPart1();
                sizeList = listQuestionPart4.size();
                File file4 = new File(Environment.getExternalStorageDirectory() + "/EnglishForChange/Easy/Test" + 1 + "/Part" + 4);
                File[] list4 = file4.listFiles();
                for (File f : list4) {
                    String name = f.getName();

                    if (name.endsWith(".mp3")) mp3File = f;
                }
                try {
                    mediaPlayer.setDataSource(mp3File.getPath());
                } catch (IOException e) {
                    e.printStackTrace();
                }
                try {
                    mediaPlayer.prepare();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                mediaPlayer.start();
                mHandler.postDelayed(mUpdateTimeTask, 100);
                break;
            case 5:
                listQuestionPart5 = ((Content_Part_Activity) mContext).getListQuestionPart5();
                listYourAnswer = ((Content_Part_Activity) mContext).getReviewPart1();
                sizeList = listQuestionPart5.size();
                break;
            case 6:
                listQuestionPart6 = ((Content_Part_Activity) mContext).getListQuestionPart6();
                listYourAnswer = ((Content_Part_Activity) mContext).getReviewPart1();
                sizeList = listQuestionPart6.size();
                break;
            case 7:
                listQuestionPart7 = ((Content_Part_Activity) mContext).getListQuestionPart7();
                listYourAnswer = ((Content_Part_Activity) mContext).getReviewPart1();
                sizeList = listQuestionPart7.size();
                break;
        }


    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View root = LayoutInflater.from(mContext).inflate(R.layout.fragment_show_resultdetail, container, false);
        wv_conversation_en = (WebView) root.findViewById(R.id.wv_conversation_en);
        tv_question_en = (TextView) root.findViewById(R.id.tv_question_en);
        tv_question_vi = (TextView) root.findViewById(R.id.tv_question_vi);
        btn_play = (Button) root.findViewById(R.id.btn_play);
        tv_realtime = (TextView) root.findViewById(R.id.tv_realtime);
        tv_totaltime = (TextView) root.findViewById(R.id.tv_totaltime);
        mSeekBar = (SeekBar) root.findViewById(R.id.seekbar);
        rlt2 = (RelativeLayout) root.findViewById(R.id.rlt2);
        btn_A_en = (Button) root.findViewById(R.id.btn_A_en);
        btn_B_en = (Button) root.findViewById(R.id.btn_B_en);
        btn_C_en = (Button) root.findViewById(R.id.btn_C_en);
        btn_D_en = (Button) root.findViewById(R.id.btn_D_en);
        btn_A_vi = (Button) root.findViewById(R.id.btn_A_vi);
        btn_B_vi = (Button) root.findViewById(R.id.btn_B_vi);
        btn_C_vi = (Button) root.findViewById(R.id.btn_C_vi);
        btn_D_vi = (Button) root.findViewById(R.id.btn_D_vi);
        btn_next = (Button) root.findViewById(R.id.btn_next);
        btn_pre = (Button) root.findViewById(R.id.btn_pre);

        imgPNG = (ImageView) root.findViewById(R.id.img_picturn);

        btn_play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isPlaying == 0) {
                    btn_play.setBackgroundResource(R.drawable.btn_play);
                    isPlaying = 1;
                    mediaPlayer.pause();
                } else if (isPlaying == 1) {
                    btn_play.setBackgroundResource(R.drawable.btn_pause);
                    isPlaying = 0;
                    mediaPlayer.start();

                }
            }
        });
        setData(0);


        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (num_question < sizeList-1) {
                    setData(num_question + 1);
                    num_question = num_question + 1;
                } else {
                    Toast.makeText(getContext(), "Hết Rồi Bố Ơi ", Toast.LENGTH_SHORT).show();
                }
            }
        });



            btn_pre.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (num_question > 0){
                        setData(num_question - 1);
                        num_question = num_question - 1;
                    }
                    else {
                        Toast.makeText(getContext(), "Hết Rồi Bố Ơi ", Toast.LENGTH_SHORT).show();

                    }
                }
            });


        return root;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
//        ((Content_Part_Activity)getActivity()).clearBackStackInclusive("vinh"); // tag (addToBackStack tag) should be the same which was used while transacting the F2 fragment
    }
    private Runnable mUpdateTimeTask = new Runnable() {
        public void run() {
            long totalDuration = 0;
            try {
                totalDuration = mediaPlayer.getDuration();
            } catch (IllegalStateException e) {
                e.printStackTrace();
            }
            long currentDuration = 0;
            try {
                currentDuration = mediaPlayer.getCurrentPosition();
            } catch (IllegalStateException e) {
                e.printStackTrace();
            }
            //Hiển thị tổng thời gian
            tv_totaltime.setText("" + utils.milliSecondsToTimer(totalDuration));
            //Hiển thị thời gian hoàn thành
            tv_realtime.setText("" + utils.milliSecondsToTimer(currentDuration));

            //Thực hiện việc cập nhật progress bar
            int progress = (int) (utils.getProgressPercentage(currentDuration,
                    totalDuration));
            // Log.d("Progress", ""+progress);
            mSeekBar.setProgress(progress);
            //Chạy lại sau 0,1s
            mHandler.postDelayed(this, 100);
        }
    };

    @Override
    public void onResume() {
        super.onResume();
        if (!mediaPlayer.isPlaying()) {
            current = current - 1000;
            if (current >= 0)
                mediaPlayer.seekTo(current);


            mediaPlayer.start();
        }


    }

    @Override
    public void onPause() {
        super.onPause();
        if (mediaPlayer.isPlaying()) {
            mediaPlayer.stop();
            //  current = mediaPlayer.getCurrentPosition();

        }
    }


    private void setData(int pos) {
        btn_A_en.setBackgroundResource(R.drawable.button_answer_normal);
        btn_A_vi.setBackgroundResource(R.drawable.button_answer_normal);
        btn_B_vi.setBackgroundResource(R.drawable.button_answer_normal);
        btn_B_en.setBackgroundResource(R.drawable.button_answer_normal);
        btn_C_vi.setBackgroundResource(R.drawable.button_answer_normal);
        btn_C_en.setBackgroundResource(R.drawable.button_answer_normal);
        btn_D_vi.setBackgroundResource(R.drawable.button_answer_normal);
        btn_D_en.setBackgroundResource(R.drawable.button_answer_normal);


        switch (type) {
            case 1:

                questtion1 = listQuestionPart12.get(pos);
                answer_key = questtion1.getAnswer();
                yourAnswer = listYourAnswer[pos];
                a = questtion1.getTranscription_en().split("\\(");
                b = questtion1.getTranscription_vi().split("\\(");
                wv_conversation_en.setVisibility(View.GONE);
                if (full == false) {
                    setTextTitleBar(pos + 1, listQuestionPart12.size());

                } else {
                    setTextTitleBar(pos + 1, 200);

                }

                tv_question_en.setText(a[0]);
                btn_A_en.setText(a[1]);
                btn_B_en.setText(a[2]);
                btn_C_en.setText(a[3]);
                btn_D_en.setText(a[4]);

                tv_question_vi.setText(b[0]);
                btn_A_vi.setText(b[1]);
                btn_B_vi.setText(b[2]);
                btn_C_vi.setText(b[3]);
                btn_D_vi.setText(b[4]);

                break;
            case 2:

                questtion1 = listQuestionPart12.get(pos);
                answer_key = questtion1.getAnswer();
                if (full == true) {
                    yourAnswer = listYourAnswer[pos + 10];
                    setTextTitleBar(pos + 11, 200);

                } else {
                    yourAnswer = listYourAnswer[pos];
                    setTextTitleBar(pos + 1, listQuestionPart12.size());

                }
                a = questtion1.getTranscription_en().split("\\(");
                b = questtion1.getTranscription_vi().split("\\(");
                wv_conversation_en.setVisibility(View.GONE);


                tv_question_en.setText(a[0]);
                btn_A_en.setText(a[1]);
                btn_B_en.setText(a[2]);
                btn_C_en.setText(a[3]);
                btn_D_en.setVisibility(View.GONE);

                tv_question_vi.setText(b[0]);
                btn_A_vi.setText(b[1]);
                btn_B_vi.setText(b[2]);
                btn_C_vi.setText(b[3]);
                btn_D_vi.setVisibility(View.GONE);

                break;
            case 3:

                question3 = listQuestionPart3.get(pos);
                answer_key = question3.getAnswer();
                if (full == true) {
                    yourAnswer = listYourAnswer[pos + 40];
                    setTextTitleBar(pos + 41, 200);


                } else {
                    yourAnswer = listYourAnswer[pos];
                    setTextTitleBar(pos + 1, listQuestionPart3.size());


                }
                a = question3.getQue_en().split("\\(");
                b = question3.getQue_vi().split("\\(");
                conversation = "<strong><u>English</u></strong>" + "<br>" + question3.getCon_en() + "<br>" + "<br>" + "<strong><u>Tiếng Việt</u><strong>" + "<br>" + question3.getCon_vi();
                wv_conversation_en.loadDataWithBaseURL(null, conversation, "text/html", "UTF-8", null);
                tv_question_en.setText(a[0]);
                btn_A_en.setText(a[1]);
                btn_B_en.setText(a[2]);
                btn_C_en.setText(a[3]);
                btn_D_en.setText(a[4]);

                tv_question_vi.setText(b[0]);
                btn_A_vi.setText(b[1]);
                btn_B_vi.setText(b[2]);
                btn_C_vi.setText(b[3]);
                btn_D_vi.setText(b[4]);


                break;
            case 4:

                question4 = listQuestionPart4.get(pos);
                answer_key = question4.getAnswer();
                if (full == true) {
                    yourAnswer = listYourAnswer[pos + 70];
                    setTextTitleBar(pos + 71, 200);

                } else {
                    yourAnswer = listYourAnswer[pos];
                    setTextTitleBar(pos + 1, listQuestionPart4.size());

                }
                a = question4.getQue_en().split("\\(");
                b = question4.getQue_vi().split("\\(");
                conversation = "<strong><u>English</u></strong>" + "<br>" + question4.getCon_en() + "<br>" + "<br>" + "<strong><u>Tiếng Việt</u><strong>" + "<br>" + question4.getCon_vi();
                wv_conversation_en.loadDataWithBaseURL(null, conversation, "text/html", "UTF-8", null);
                tv_question_en.setText(a[0]);
                btn_A_en.setText(a[1]);
                btn_B_en.setText(a[2]);
                btn_C_en.setText(a[3]);
                btn_D_en.setText(a[4]);

                tv_question_vi.setText(b[0]);
                btn_A_vi.setText(b[1]);
                btn_B_vi.setText(b[2]);
                btn_C_vi.setText(b[3]);
                btn_D_vi.setText(b[4]);
                break;
            case 5:


                rlt2.setVisibility(View.GONE);
                question5 = listQuestionPart5.get(pos);
                answer_key = question5.getAnswer();
                if (full == false) {
                    yourAnswer = listYourAnswer[pos];
                    setTextTitleBar(pos + 1, listQuestionPart5.size());


                } else {
                    yourAnswer = listYourAnswer[pos + 100];
                    setTextTitleBar(pos + 101, 200);


                }
                a = question5.getQue_en().split("\\(");
                b = question5.getQue_vi().split("\\(");
                conversation = "<strong><u>Tiếng Việt</u><strong>" + "<br>" + question5.getNote();
                wv_conversation_en.loadDataWithBaseURL(null, conversation, "text/html", "UTF-8", null);
                tv_question_en.setText(a[0]);
                btn_A_en.setText(a[1]);
                btn_B_en.setText(a[2]);
                btn_C_en.setText(a[3]);
                btn_D_en.setText(a[4]);

                tv_question_vi.setText(b[0]);
                btn_A_vi.setText(b[1]);
                btn_B_vi.setText(b[2]);
                btn_C_vi.setText(b[3]);
                btn_D_vi.setText(b[4]);
                break;
            case 6:
                rlt2.setVisibility(View.GONE);
                question6 = listQuestionPart6.get(pos);

                answer_key = question6.getAnswer();
                if (full == false) {
                    yourAnswer = listYourAnswer[pos];
                    setTextTitleBar(pos + 1, listQuestionPart6.size());

                } else {
                    yourAnswer = listYourAnswer[pos + 140];
                    setTextTitleBar(pos + 141, 200);


                }
                a = question6.getQuestion_en().split("\\(");
                b = question6.getQuestion_vi().split("\\(");
                wv_conversation_en.setVisibility(View.GONE);

                tv_question_en.setText(a[0]);
                btn_A_en.setText(a[1]);
                btn_B_en.setText(a[2]);
                btn_C_en.setText(a[3]);
                btn_D_en.setText(a[4]);

                tv_question_vi.setText(b[0]);
                btn_A_vi.setText(b[1]);
                btn_B_vi.setText(b[2]);
                btn_C_vi.setText(b[3]);
                btn_D_vi.setText(b[4]);
                break;
            case 7:

                rlt2.setVisibility(View.GONE);
                question7 = listQuestionPart7.get(pos);


                answer_key = question7.getAnswer();
                if (full == false) {
                    yourAnswer = listYourAnswer[pos];
                    setTextTitleBar(pos + 1, listQuestionPart7.size());


                } else {
                    yourAnswer = listYourAnswer[pos + 152];
                    setTextTitleBar(pos + 153, 200);


                }
                a = question7.getQue_en().split("\\+++");
                b = question7.getQue_vi().split("\\+++");
                String title;
                title = "<strong><u>" + a[0] + "</u><strong>" + "<br>" + a[1];
                wv_conversation_en.loadDataWithBaseURL(null, title, "text/html", "UTF-8", null);
                tv_question_en.setText(a[2]);
                btn_A_en.setText(a[3]);
                btn_B_en.setText(a[4]);
                btn_C_en.setText(a[5]);
                btn_D_en.setText(a[6]);

                tv_question_en.setText(a[2]);
                btn_A_vi.setText(a[3]);
                btn_B_vi.setText(a[4]);
                btn_C_vi.setText(a[5]);
                btn_D_vi.setText(a[6]);
                break;
        }


        if (listFilePNG.size() > 0

                ) {
            imgPNG.setVisibility(View.VISIBLE);
            imgPNG.setImageURI(Uri.fromFile(new File(listFilePNG.get(pos).getPath())));
        }

        switch (yourAnswer) {
            case 1:
                btn_A_en.setBackgroundResource(R.drawable.button_your_answer);
                btn_A_vi.setBackgroundResource(R.drawable.button_your_answer);

                break;
            case 2:
                btn_B_en.setBackgroundResource(R.drawable.button_your_answer);
                btn_B_vi.setBackgroundResource(R.drawable.button_your_answer);

                break;
            case 3:
                btn_C_en.setBackgroundResource(R.drawable.button_your_answer);
                btn_C_vi.setBackgroundResource(R.drawable.button_your_answer);
                break;
            case 4:
                btn_D_en.setBackgroundResource(R.drawable.button_your_answer);
                btn_D_vi.setBackgroundResource(R.drawable.button_your_answer);
                break;
        }

        switch (answer_key) {
            case "A":
                btn_A_en.setBackgroundResource(R.drawable.button_answer_true);
                btn_A_vi.setBackgroundResource(R.drawable.button_answer_true);
                break;
            case "B":
                btn_B_en.setBackgroundResource(R.drawable.button_answer_true);
                btn_B_vi.setBackgroundResource(R.drawable.button_answer_true);
                break;
            case "C":
                btn_C_en.setBackgroundResource(R.drawable.button_answer_true);
                btn_C_vi.setBackgroundResource(R.drawable.button_answer_true);
                break;
            case "D":

                btn_D_en.setBackgroundResource(R.drawable.button_answer_true);
                btn_D_vi.setBackgroundResource(R.drawable.button_answer_true);
                break;

        }


    }

    private void setTextTitleBar(int current, int type) {
        ((Content_Part_Activity) mContext).setTextTitleBar(current + "/" + type);

    }
}
