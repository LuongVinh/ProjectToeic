package com.example.luongvinh.englishforchange.Objects;

/**
 * Created by LUONGVINH on 4/4/2017.
 */
public class Test {
    private  String tv_num_test,tv_diem_nghe,tv_cau_nghe,tv_diem_doc,tv_cau_doc,tv_tong_diem;

    public Test(String tv_num_test, String tv_diem_nghe, String tv_cau_nghe, String tv_diem_doc, String tv_cau_doc, String tv_tong_diem) {
        this.tv_num_test = tv_num_test;
        this.tv_diem_nghe = tv_diem_nghe;
        this.tv_cau_nghe = tv_cau_nghe;
        this.tv_diem_doc = tv_diem_doc;
        this.tv_cau_doc = tv_cau_doc;
        this.tv_tong_diem = tv_tong_diem;
    }

    public String getTv_num_test() {
        return tv_num_test;
    }

    public void setTv_num_test(String tv_num_test) {
        this.tv_num_test = tv_num_test;
    }

    public String getTv_diem_nghe() {
        return tv_diem_nghe;
    }

    public void setTv_diem_nghe(String tv_diem_nghe) {
        this.tv_diem_nghe = tv_diem_nghe;
    }

    public String getTv_cau_nghe() {
        return tv_cau_nghe;
    }

    public void setTv_cau_nghe(String tv_cau_nghe) {
        this.tv_cau_nghe = tv_cau_nghe;
    }

    public String getTv_diem_doc() {
        return tv_diem_doc;
    }

    public void setTv_diem_doc(String tv_diem_doc) {
        this.tv_diem_doc = tv_diem_doc;
    }

    public String getTv_cau_doc() {
        return tv_cau_doc;
    }

    public void setTv_cau_doc(String tv_cau_doc) {
        this.tv_cau_doc = tv_cau_doc;
    }

    public String getTv_tong_diem() {
        return tv_tong_diem;
    }

    public void setTv_tong_diem(String tv_tong_diem) {
        this.tv_tong_diem = tv_tong_diem;
    }
}
