package com.example.luongvinh.englishforchange.Objects;

import java.io.Serializable;

/**
 * Created by LUONGVINH on 4/29/2017.
 */
public class Question_Part3 implements Serializable{
    private String answer;
    private String con_en;
    private String con_vi;
    private String que_en;
    private String que_vi;

    public Question_Part3(String answer, String con_en, String con_vi, String que_en, String que_vi) {
        this.answer = answer;
        this.con_en = con_en;
        this.con_vi = con_vi;
        this.que_en = que_en;
        this.que_vi = que_vi;
    }

    public Question_Part3() {
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getCon_en() {
        return con_en;
    }

    public void setCon_en(String con_en) {
        this.con_en = con_en;
    }

    public String getCon_vi() {
        return con_vi;
    }

    public void setCon_vi(String con_vi) {
        this.con_vi = con_vi;
    }

    public String getQue_en() {
        return que_en;
    }

    public void setQue_en(String que_en) {
        this.que_en = que_en;
    }

    public String getQue_vi() {
        return que_vi;
    }

    public void setQue_vi(String que_vi) {
        this.que_vi = que_vi;
    }
}
