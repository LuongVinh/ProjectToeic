package com.example.luongvinh.englishforchange.Objects;

/**
 * Created by LUONGVINH on 5/13/2017.
 */
public class Question_Part7 {
    private String Answer,Que_en,Que_vi;

    public Question_Part7() {
    }

    public String getAnswer() {
        return Answer;
    }

    public void setAnswer(String answer) {
        Answer = answer;
    }

    public String getQue_en() {
        return Que_en;
    }

    public void setQue_en(String que_en) {
        Que_en = que_en;
    }

    public String getQue_vi() {
        return Que_vi;
    }

    public void setQue_vi(String que_vi) {
        Que_vi = que_vi;
    }
}
