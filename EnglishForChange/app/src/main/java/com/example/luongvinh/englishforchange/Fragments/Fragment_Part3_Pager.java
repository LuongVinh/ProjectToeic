package com.example.luongvinh.englishforchange.Fragments;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.luongvinh.englishforchange.Activities.Content_Part_Activity;
import com.example.luongvinh.englishforchange.Adapters.ViewPagerPart1Adapter;
import com.example.luongvinh.englishforchange.Objects.Question_Part3;
import com.example.luongvinh.englishforchange.Objects.Question_Part4;
import com.example.luongvinh.englishforchange.R;
import com.example.luongvinh.englishforchange.Until.Constan;
import com.example.luongvinh.englishforchange.Until.Utilities;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import static com.example.luongvinh.englishforchange.Activities.Content_Part_Activity.unzip;

/**
 * Created by LUONGVINH on 5/20/2017.
 */

public class Fragment_Part3_Pager extends Fragment {
    private View root;
    private Context mContext;
    private ViewPager pager;
    private ViewPagerPart1Adapter pagerAdapter;
    private ArrayList<String> arr_answer_key;
    public int position, point = 0, isPlaying = 0, current = 0;
    private int type;
    private Button btn_play;
    private TextView tv_realtime, tv_totaltime;
    private FirebaseDatabase firebaseDatabase;
    private DatabaseReference myDATA;
    private ArrayList<Question_Part3> arrayList_p3;
    private ArrayList<Question_Part4> arrayList_p4;
    private File mp3File;
    private MediaPlayer mediaPlayer;
    private Dialog dialog;
    private ProgressBar prg;
    private SeekBar mSeekBar;
    private TextView txt_percent;
    private FirebaseStorage storage;
    private StorageReference reference;
    private String path;
    private Handler mHandler;
    private Utilities utils;
    private Fragment_Part1_Pager.AnswerChoise answerChoise;
    private Boolean full = false;

    public static Fragment_Part3_Pager newInstance(int position, int type) {
        Fragment_Part3_Pager fragment = new Fragment_Part3_Pager();
        Bundle bundle = new Bundle();
        bundle.putInt(Constan.POSITION_OF_TEST, position);
        bundle.putInt(Constan.TYPE, type);
        fragment.setArguments(bundle);

        return fragment;

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getContext();
        storage = FirebaseStorage.getInstance();
        reference = storage.getReference();
        mediaPlayer = new MediaPlayer();
        mHandler = new Handler();
        arrayList_p3 = new ArrayList<>();
        arrayList_p4 = new ArrayList<>();
        arr_answer_key = new ArrayList<>();
        position = getArguments().getInt(Constan.POSITION_OF_TEST);
        type = getArguments().getInt(Constan.TYPE);
        utils = new Utilities();
        full = ((Content_Part_Activity) mContext).getFull();

    }


    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (root == null) {
            root = LayoutInflater.from(mContext).inflate(R.layout.fragment_part3_pager, container, false);
            pager = (ViewPager) root.findViewById(R.id.view_pager);
            btn_play = (Button) root.findViewById(R.id.btn_play);
            tv_realtime = (TextView) root.findViewById(R.id.tv_realtime);
            tv_totaltime = (TextView) root.findViewById(R.id.tv_totaltime);
            mSeekBar = (SeekBar) root.findViewById(R.id.seekbar);

            //check neu ton tai file nghe thi lay question.khong thi lay file nghe xong lay question
            if (((Content_Part_Activity) mContext).checkFileExit("Test" + 1)) {

                getDataFromServer();

            } else {

                getDataMP3ByServer();
            }
            btn_play.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (isPlaying == 0) {
                        btn_play.setBackgroundResource(R.drawable.btn_play);
                        isPlaying = 1;
                        mediaPlayer.pause();
                    } else if (isPlaying == 1) {
                        btn_play.setBackgroundResource(R.drawable.btn_pause);
                        isPlaying = 0;
                        mediaPlayer.start();

                    }
                }
            });
            setTextTitleBar(0);

        }

        //khi thay doi page thi set lai dap an nguoi dung lưu
        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (full == false) {
                    if (type == 3) {
                        setTextTitleBar(position);
                        answerChoise.setAnswerChoise(position);
                    } else if (type == 4) {
                        setTextTitleBar(position);
                        answerChoise.setAnswerChoise(position);
                    }
                } else {
                    if (type == 3) {
                        setTextTitleBar(position);
                        answerChoise.setAnswerChoise(position + 40);
                    } else if (type == 4) {
                        setTextTitleBar(position);
                        answerChoise.setAnswerChoise(70 + position);
                    }
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });


        return root;
    }

    private void getDataMP3ByServer() {
        showDowloadDialog();
        path = Environment.getExternalStorageDirectory() + "/EnglishForChange/Easy/Test" + 1 + "/Part" + type;
        final File dir = new File(path);

        if (!dir.exists()) dir.mkdirs();
        File file = new File(dir, "Part" + type + ".zip");
        reference.child("Easy/Test" + 1 + "/Part" + type + "/Part" + type + ".zip").getFile(file).addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                getDataFromServer();

            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(mContext, "Erro loading File mp3 " + e.toString(), Toast.LENGTH_LONG).show();

            }
        }).addOnProgressListener(new OnProgressListener<FileDownloadTask.TaskSnapshot>() {
            @Override
            public void onProgress(FileDownloadTask.TaskSnapshot taskSnapshot) {
                int progress = (int) (100 * (float) taskSnapshot.getBytesTransferred() / taskSnapshot.getTotalByteCount());
                prg.setProgress(progress);
                txt_percent.setText(progress + "%");


            }
        });

    }

    private void showDowloadDialog() {
        dialog = new Dialog(mContext);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.WHITE));
        final View inflateView = ((LayoutInflater) ((mContext)).getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.loading_dialog, null, false);
        dialog.setContentView(inflateView);
        prg = (ProgressBar) inflateView.findViewById(R.id.pb);
        txt_percent = (TextView) inflateView.findViewById(R.id.tvProgress);
        dialog.show();
    }

    private void getDataFromServer() {
        firebaseDatabase = FirebaseDatabase.getInstance();
        myDATA = firebaseDatabase.getReference("Easy/1");
        if (type == 3) {
            myDATA.child("Part3").addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    Question_Part3 question = null;
                    for (DataSnapshot data : dataSnapshot.getChildren()) {
                        question = data.getValue(Question_Part3.class);
                        arrayList_p3.add(question);
                    }
                    Log.e("vinh", arrayList_p3.size() + "");

                    ((Content_Part_Activity) mContext).setListQuestionPart3(arrayList_p3);

                    int i;
                    for (i = 0; i < 30; i++) {
                        Question_Part3 q = arrayList_p3.get(i);
                        String a = q.getAnswer();
                        arr_answer_key.add(a);

                    }
                    if (unzip((path + "/" + "Part" + type + ".zip"), path)) {

                        try {
                            getDataByCache(type);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    } else {
                        try {
                            getDataByCache(type);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }


                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        } else if (type == 4) {
            myDATA.child("Part4").addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    Question_Part4 question = null;
                    for (DataSnapshot data : dataSnapshot.getChildren()) {
                        question = data.getValue(Question_Part4.class);
                        arrayList_p4.add(question);
                    }
                    Log.e("vinh4", arrayList_p4.size() + "");
                    //gán arraylist vừa tải về cho arraylist của activity để có thể sử dụng cho các fragment khác cùng activity
                    ((Content_Part_Activity) mContext).setListQuestionPart4(arrayList_p4);

                    int i;
                    for (i = 0; i < 30; i++) {
                        Question_Part4 q = arrayList_p4.get(i);
                        String a = q.getAnswer();
                        arr_answer_key.add(a);

                    }
                    if (unzip((path + "/" + "Part" + type + ".zip"), path)) {

                        try {
                            getDataByCache(type);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    } else {
                        try {
                            getDataByCache(type);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }


                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

        }

    }

    private void setTextTitleBar(int current) {
        if (full == false) {
            ((Content_Part_Activity) mContext).setTextTitleBar(current+1 + "/30");

        } else {
            if (type == 3) {
                ((Content_Part_Activity) mContext).setTextTitleBar(current + 41 + "/200");

            } else if (type == 4) {
                ((Content_Part_Activity) mContext).setTextTitleBar(current + 71 + "/200");

            }
        }

    }


    public void getDataByCache(int type) throws IOException {
        if (type == 3) {
            if (dialog != null && arrayList_p3.size() == 30)
                dialog.dismiss();
            getListFilePNG(1, type);
            pagerAdapter = new ViewPagerPart1Adapter(getChildFragmentManager(), 1, type, null);
            pager.setAdapter(pagerAdapter);
            mediaPlayer.setDataSource(mp3File.getPath());
            mediaPlayer.prepare();
            mediaPlayer.start();


        } else if (type == 4) {
            if (dialog != null && arrayList_p4.size() == 30)
                dialog.dismiss();
            getListFilePNG(1, type);
            pagerAdapter = new ViewPagerPart1Adapter(getChildFragmentManager(), 1, type, null);
            pager.setAdapter(pagerAdapter);
            mediaPlayer.setDataSource(mp3File.getPath());
            mediaPlayer.prepare();
            mediaPlayer.start();


        }

        setEventSeekBar();


    }

    private void setEventSeekBar() {
        mHandler.postDelayed(mUpdateTimeTask, 100);

    }

    private Runnable mUpdateTimeTask = new Runnable() {
        public void run() {
            long totalDuration = 0;
            try {
                totalDuration = mediaPlayer.getDuration();
            } catch (IllegalStateException e) {
                e.printStackTrace();
            }
            long currentDuration = 0;
            try {
                currentDuration = mediaPlayer.getCurrentPosition();
            } catch (IllegalStateException e) {
                e.printStackTrace();
            }
            //Hiển thị tổng thời gian
            tv_totaltime.setText("" + utils.milliSecondsToTimer(totalDuration));
            //Hiển thị thời gian hoàn thành
            tv_realtime.setText("" + utils.milliSecondsToTimer(currentDuration));

            //Thực hiện việc cập nhật progress bar
            int progress = (int) (utils.getProgressPercentage(currentDuration,
                    totalDuration));
            // Log.d("Progress", ""+progress);
            mSeekBar.setProgress(progress);
            //Chạy lại sau 0,1s
            mHandler.postDelayed(this, 100);
        }
    };

    public void getListFilePNG(int pos, int type) {

        File file = new File(Environment.getExternalStorageDirectory() + "/EnglishForChange/Easy/Test" + pos + "/Part" + type);
        File[] list = file.listFiles();
        for (File f : list) {
            String name = f.getName();

            if (name.endsWith(".mp3")) mp3File = f;
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        if (!mediaPlayer.isPlaying()) {
            current = current - 1000;
            if (current >= 0)
                mediaPlayer.seekTo(current);


            mediaPlayer.start();
        }


    }

    @Override
    public void onPause() {
        super.onPause();
        if (mediaPlayer.isPlaying()) {
            mediaPlayer.stop();
            current = mediaPlayer.getCurrentPosition();

        }
    }

    public void setAnswerChoise(Fragment_Part1_Pager.AnswerChoise answerChoise) {
        this.answerChoise = answerChoise;
    }
}
