package com.example.luongvinh.englishforchange.dataVideo.remote;


import com.example.luongvinh.englishforchange.dataVideo.model.ListVideoYoutube;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by LeToan on 5/13/2017.
 */

public interface SOService {
    @GET("search?part=snippet&maxResults=10&type=video&key=AIzaSyCEAXRIGSeXpyCEwGveyP9fGssNMF6NTFg")
    Call<ListVideoYoutube> getAllvideos(@Query("q") String qyery,@Query("pageToken") String pageToken);
}
