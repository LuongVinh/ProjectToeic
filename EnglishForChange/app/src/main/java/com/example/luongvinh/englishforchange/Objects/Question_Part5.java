package com.example.luongvinh.englishforchange.Objects;

/**
 * Created by LUONGVINH on 5/11/2017.
 */
public class Question_Part5 {
    private String Answer,Note,Que_en,Que_vi;

    public Question_Part5() {
    }

    public Question_Part5(String answer, String note, String que_en, String que_vi) {
        Answer = answer;
        Note = note;
        Que_en = que_en;
        Que_vi = que_vi;
    }

    public String getAnswer() {
        return Answer;
    }

    public void setAnswer(String answer) {
        Answer = answer;
    }

    public String getNote() {
        return Note;
    }

    public void setNote(String note) {
        Note = note;
    }

    public String getQue_en() {
        return Que_en;
    }

    public void setQue_en(String que_en) {
        Que_en = que_en;
    }

    public String getQue_vi() {
        return Que_vi;
    }

    public void setQue_vi(String que_vi) {
        Que_vi = que_vi;
    }
}
