package com.example.luongvinh.englishforchange.Adapters;

import android.content.Context;
import android.widget.ArrayAdapter;

import com.example.luongvinh.englishforchange.Objects.Question_Part3;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by LUONGVINH on 4/29/2017.
 */
public class ListQuestion_Part3_Adapter  extends ArrayAdapter{
    private Context mContext;
    private ArrayList<Question_Part3> arrayList;
    public ListQuestion_Part3_Adapter(Context context, int resource, List<Question_Part3> list) {
        super(context, resource, list);
        this.mContext=context;
        this.arrayList=new ArrayList<>(list);
    }

}
