package com.example.luongvinh.englishforchange.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.TextView;

import com.example.luongvinh.englishforchange.Activities.Content_Part_Activity;
import com.example.luongvinh.englishforchange.Objects.Question_Part5;
import com.example.luongvinh.englishforchange.R;
import com.example.luongvinh.englishforchange.Until.Constan;

import java.util.ArrayList;

/**
 * Created by LUONGVINH on 5/10/2017.
 */
public class Fragment_Part5 extends Fragment implements Fragment_Part1_Pager.AnswerChoise {
    private View root;
    private Context mContext;
    private WebView wv_conversation_en;
    private int[] reviewPart;
    private TextView tv_question;
    private Button btn_A, btn_B, btn_C, btn_D;
    public int position,type,answerRight = 0;
    private int index;
    private ArrayList<Question_Part5> listquestion_part5;
    private Question_Part5 question_part5;
    private Boolean full=false;



    public static Fragment_Part5 newInstance(int position, int index, int type,  ArrayList arr) {
        Fragment_Part5 fragment = new Fragment_Part5();
        Bundle bundle = new Bundle();
        bundle.putInt(Constan.POSITION_OF_TEST, position);
        bundle.putInt(Constan.TYPE, type);
        bundle.putInt(Constan.INDEX, index);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getContext();
        full=((Content_Part_Activity)mContext).getFull();
        listquestion_part5 = ((Content_Part_Activity) mContext).getListQuestionPart5();
        type = getArguments().getInt(Constan.TYPE);
        index = getArguments().getInt(Constan.INDEX);
        position = getArguments().getInt(Constan.POSITION_OF_TEST);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        root = LayoutInflater.from(mContext).inflate(R.layout.fragment_part5, container, false);
        wv_conversation_en = (WebView) root.findViewById(R.id.wv_conversation_en);
        wv_conversation_en.setVisibility(View.GONE);
        btn_A = (Button) root.findViewById(R.id.btn_A);
        btn_B = (Button) root.findViewById(R.id.btn_B);
        btn_C = (Button) root.findViewById(R.id.btn_C);
        btn_D = (Button) root.findViewById(R.id.btn_D);
        tv_question = (TextView) root.findViewById(R.id.tv_question);

        setEvent();
        setData(index);
        return root;
    }


    private View.OnClickListener clickAnswer = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (full==false){
                switch (v.getId()) {
                    case R.id.btn_A:
                        btn_A.setBackgroundResource(R.drawable.button_your_answer);
                        btn_B.setBackgroundResource(R.drawable.button_answer_normal);
                        btn_C.setBackgroundResource(R.drawable.button_answer_normal);
                        btn_D.setBackgroundResource(R.drawable.button_answer_normal);
                        ((Content_Part_Activity) mContext).setValueReview(index - 1, 1);
                        break;
                    case R.id.btn_B:
                        btn_A.setBackgroundResource(R.drawable.button_answer_normal);
                        btn_B.setBackgroundResource(R.drawable.button_your_answer);
                        btn_C.setBackgroundResource(R.drawable.button_answer_normal);
                        btn_D.setBackgroundResource(R.drawable.button_answer_normal);
                        ((Content_Part_Activity) mContext).setValueReview(index - 1, 2);
                        break;
                    case R.id.btn_C:
                        btn_A.setBackgroundResource(R.drawable.button_answer_normal);
                        btn_B.setBackgroundResource(R.drawable.button_answer_normal);
                        btn_C.setBackgroundResource(R.drawable.button_your_answer);
                        btn_D.setBackgroundResource(R.drawable.button_answer_normal);
                        ((Content_Part_Activity) mContext).setValueReview(index - 1, 3);
                        break;
                    case R.id.btn_D:
                        btn_A.setBackgroundResource(R.drawable.button_answer_normal);
                        btn_B.setBackgroundResource(R.drawable.button_answer_normal);
                        btn_C.setBackgroundResource(R.drawable.button_answer_normal);
                        btn_D.setBackgroundResource(R.drawable.button_your_answer);
                        ((Content_Part_Activity) mContext).setValueReview(index - 1, 4);
                        break;
                }

            }
            else {
                switch (v.getId()) {
                    case R.id.btn_A:
                        btn_A.setBackgroundResource(R.drawable.button_your_answer);
                        btn_B.setBackgroundResource(R.drawable.button_answer_normal);
                        btn_C.setBackgroundResource(R.drawable.button_answer_normal);
                        btn_D.setBackgroundResource(R.drawable.button_answer_normal);
                        ((Content_Part_Activity) mContext).setValueReview(index +99, 1);
                        break;
                    case R.id.btn_B:
                        btn_A.setBackgroundResource(R.drawable.button_answer_normal);
                        btn_B.setBackgroundResource(R.drawable.button_your_answer);
                        btn_C.setBackgroundResource(R.drawable.button_answer_normal);
                        btn_D.setBackgroundResource(R.drawable.button_answer_normal);
                        ((Content_Part_Activity) mContext).setValueReview(index +99, 2);
                        break;
                    case R.id.btn_C:
                        btn_A.setBackgroundResource(R.drawable.button_answer_normal);
                        btn_B.setBackgroundResource(R.drawable.button_answer_normal);
                        btn_C.setBackgroundResource(R.drawable.button_your_answer);
                        btn_D.setBackgroundResource(R.drawable.button_answer_normal);
                        ((Content_Part_Activity) mContext).setValueReview(index +99, 3);
                        break;
                    case R.id.btn_D:
                        btn_A.setBackgroundResource(R.drawable.button_answer_normal);
                        btn_B.setBackgroundResource(R.drawable.button_answer_normal);
                        btn_C.setBackgroundResource(R.drawable.button_answer_normal);
                        btn_D.setBackgroundResource(R.drawable.button_your_answer);
                        ((Content_Part_Activity) mContext).setValueReview(index +99, 4);
                        break;
                }

            }

            if (index == 40) {

                reviewPart = ((Content_Part_Activity) mContext).getReviewPart1();
                if(full==false){
                    for (int i = 0; i < listquestion_part5.size(); i++) {
                        question_part5 = listquestion_part5.get(i);
                        if (question_part5.getAnswer().equals("A") && reviewPart[i] == 1)
                            answerRight = answerRight + 1;
                        if (question_part5.getAnswer().equals("B") && reviewPart[i] == 2)
                            answerRight = answerRight + 1;
                        if (question_part5.getAnswer().equals("C") && reviewPart[i] == 3)
                            answerRight = answerRight + 1;
                        if (question_part5.getAnswer().equals("D") && reviewPart[i] == 4)
                            answerRight = answerRight + 1;
                    }
                    ((Content_Part_Activity)mContext).showFragmentShowResult(answerRight);

                }
                else {
                    for (int i = 0; i < listquestion_part5.size(); i++) {
                        question_part5 = listquestion_part5.get(i);
                        if (question_part5.getAnswer().equals("A") && reviewPart[i+100] == 1)
                            answerRight = answerRight + 1;
                        if (question_part5.getAnswer().equals("B") && reviewPart[i+100] == 2)
                            answerRight = answerRight + 1;
                        if (question_part5.getAnswer().equals("C") && reviewPart[i+100] == 3)
                            answerRight = answerRight + 1;
                        if (question_part5.getAnswer().equals("D") && reviewPart[i+100] == 4)
                            answerRight = answerRight + 1;
                    }
                    ((Content_Part_Activity)mContext).addPoint(answerRight,type);
                    Log.e("Partt5",answerRight+"");
                    ((Content_Part_Activity)mContext).setType(6);
                    ((Content_Part_Activity)mContext).showFragment();
                }
            }
        }

    };

    private void setEvent() {
        btn_A.setOnClickListener(clickAnswer);
        btn_B.setOnClickListener(clickAnswer);
        btn_C.setOnClickListener(clickAnswer);
        btn_D.setOnClickListener(clickAnswer);


    }

    @Override
    public void onResume() {
        super.onResume();

        ((Content_Part_Activity)mContext).setAnswerChoise(this);
    }





    private void setData(int pos) {

        Question_Part5 q0 = listquestion_part5.get(pos-1);
        String[] a = q0.getQue_en().split("\\(");
        tv_question.setText(a[0]);
        btn_A.setText(a[1]);
        btn_B.setText(a[2]);
        btn_C.setText(a[3]);
        btn_D.setText(a[4]);


    }

    @Override
    public void setAnswerChoise(int position) {
        int answerChoise = ((Content_Part_Activity) mContext).getValueReview(position);
        switch (answerChoise) {
            case 1: {
                btn_A.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.button_your_answer));
            }
            break;
            case 2: {
                btn_B.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.button_your_answer));
            }
            break;
            case 3: {
                btn_C.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.button_your_answer));
            }
            break;
            case 4: {
                btn_D.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.button_your_answer));
            }
            break;
        }


    }
}
