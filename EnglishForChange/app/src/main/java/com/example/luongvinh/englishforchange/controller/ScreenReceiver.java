package com.example.luongvinh.englishforchange.controller;


import android.app.KeyguardManager;
import android.app.KeyguardManager.KeyguardLock;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.util.Log;
import android.widget.Toast;

import com.example.luongvinh.englishforchange.R;


public class ScreenReceiver extends BroadcastReceiver {
	private boolean screenOff;
//xử lí sự kiện khi bấm tắt màn hình đt
	@Override
	public void onReceive(Context context, Intent intent) {
		String action = intent.getAction();
		if (action != null && !action.equals("")) {
			Log.e("khanhduy.le", action);
			if (intent.getAction().equals(Intent.ACTION_SCREEN_OFF)) {
				PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
				WakeLock wakeLock = pm.newWakeLock((PowerManager.SCREEN_BRIGHT_WAKE_LOCK | PowerManager.FULL_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP), "TAG");
				wakeLock.acquire();
				Toast.makeText(context, context.getResources().getString(R.string.youtube_terms), Toast.LENGTH_SHORT).show();
			} else if (intent.getAction().equals(Intent.ACTION_SCREEN_ON)) {
				KeyguardManager keyguardManager = (KeyguardManager) context.getSystemService(Context.KEYGUARD_SERVICE); 
				KeyguardLock keyguardLock = keyguardManager.newKeyguardLock("TAG");
				keyguardLock.disableKeyguard();
			} else if (intent.getAction().equals(Intent.ACTION_USER_PRESENT)) {
				//mo khoa
			}
		}
	}

}