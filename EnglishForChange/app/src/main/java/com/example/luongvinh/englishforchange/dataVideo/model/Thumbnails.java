package com.example.luongvinh.englishforchange.dataVideo.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by LeToan on 5/13/2017.
 */

public class Thumbnails implements Serializable{

    @SerializedName("high")
    @Expose
    private High high;



    public High getHigh() {
        return high;
    }

    public void setHigh(High high) {
        this.high = high;
    }

}
