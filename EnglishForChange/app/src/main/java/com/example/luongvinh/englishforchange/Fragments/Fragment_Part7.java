package com.example.luongvinh.englishforchange.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.TextView;

import com.example.luongvinh.englishforchange.Activities.Content_Part_Activity;
import com.example.luongvinh.englishforchange.Objects.Question_Part7;
import com.example.luongvinh.englishforchange.R;
import com.example.luongvinh.englishforchange.Until.Constan;

import java.util.ArrayList;

/**
 * Created by LUONGVINH on 5/13/2017.
 */
public class Fragment_Part7 extends Fragment implements Fragment_Part1_Pager.AnswerChoise {
    private View root;
    private Context mContext;
    private TextView tv_question;
    private Button btn_A, btn_B, btn_C, btn_D;
    public int position, type, answerRight = 0;
    private int index, pointlistening = 0, pointreading = 0;
    private WebView wv_conversation_en;
    private ArrayList<Question_Part7> listquestion_part7;
    private int[] reviewPart, point;
    private Question_Part7 question_part7;
    private Boolean full = false;


    public static Fragment_Part7 newInstance(int position, int index, int type, ArrayList arr) {
        Fragment_Part7 fragment = new Fragment_Part7();
        Bundle bundle = new Bundle();
        bundle.putInt(Constan.POSITION_OF_TEST, position);
        bundle.putInt(Constan.TYPE, type);
        bundle.putInt(Constan.INDEX, index);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getContext();
        listquestion_part7 = ((Content_Part_Activity) mContext).getListQuestionPart7();
        type = getArguments().getInt(Constan.TYPE);
        index = getArguments().getInt(Constan.INDEX);
        position = getArguments().getInt(Constan.POSITION_OF_TEST);
        full = ((Content_Part_Activity) mContext).getFull();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        root = LayoutInflater.from(mContext).inflate(R.layout.fragment_part7, container, false);
        wv_conversation_en = (WebView) root.findViewById(R.id.wv_conversation_en);
        btn_A = (Button) root.findViewById(R.id.btn_A);
        btn_B = (Button) root.findViewById(R.id.btn_B);
        btn_C = (Button) root.findViewById(R.id.btn_C);
        btn_D = (Button) root.findViewById(R.id.btn_D);
        tv_question = (TextView) root.findViewById(R.id.tv_question);

        setEvent();
        setData(index);
        return root;
    }

    private void setTextTitleBar(int current) {
        ((Content_Part_Activity) mContext).setTextTitleBar(current + "/48");
    }


    private View.OnClickListener clickAnswer = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (full == false) {
                switch (v.getId()) {
                    case R.id.btn_A:
                        btn_A.setBackgroundResource(R.drawable.button_your_answer);
                        btn_B.setBackgroundResource(R.drawable.button_answer_normal);
                        btn_C.setBackgroundResource(R.drawable.button_answer_normal);
                        btn_D.setBackgroundResource(R.drawable.button_answer_normal);
                        ((Content_Part_Activity) mContext).setValueReview(index - 1, 1);
                        break;
                    case R.id.btn_B:
                        btn_A.setBackgroundResource(R.drawable.button_answer_normal);
                        btn_B.setBackgroundResource(R.drawable.button_your_answer);
                        btn_C.setBackgroundResource(R.drawable.button_answer_normal);
                        btn_D.setBackgroundResource(R.drawable.button_answer_normal);
                        ((Content_Part_Activity) mContext).setValueReview(index - 1, 2);
                        break;
                    case R.id.btn_C:
                        btn_A.setBackgroundResource(R.drawable.button_answer_normal);
                        btn_B.setBackgroundResource(R.drawable.button_answer_normal);
                        btn_C.setBackgroundResource(R.drawable.button_your_answer);
                        btn_D.setBackgroundResource(R.drawable.button_answer_normal);
                        ((Content_Part_Activity) mContext).setValueReview(index - 1, 3);
                        break;
                    case R.id.btn_D:
                        btn_A.setBackgroundResource(R.drawable.button_answer_normal);
                        btn_B.setBackgroundResource(R.drawable.button_answer_normal);
                        btn_C.setBackgroundResource(R.drawable.button_answer_normal);
                        btn_D.setBackgroundResource(R.drawable.button_your_answer);
                        ((Content_Part_Activity) mContext).setValueReview(index - 1, 4);
                        break;
                }
            } else {
                switch (v.getId()) {
                    case R.id.btn_A:
                        btn_A.setBackgroundResource(R.drawable.button_your_answer);
                        btn_B.setBackgroundResource(R.drawable.button_answer_normal);
                        btn_C.setBackgroundResource(R.drawable.button_answer_normal);
                        btn_D.setBackgroundResource(R.drawable.button_answer_normal);
                        ((Content_Part_Activity) mContext).setValueReview(index + 151, 1);
                        break;
                    case R.id.btn_B:
                        btn_A.setBackgroundResource(R.drawable.button_answer_normal);
                        btn_B.setBackgroundResource(R.drawable.button_your_answer);
                        btn_C.setBackgroundResource(R.drawable.button_answer_normal);
                        btn_D.setBackgroundResource(R.drawable.button_answer_normal);
                        ((Content_Part_Activity) mContext).setValueReview(index + 151, 2);
                        break;
                    case R.id.btn_C:
                        btn_A.setBackgroundResource(R.drawable.button_answer_normal);
                        btn_B.setBackgroundResource(R.drawable.button_answer_normal);
                        btn_C.setBackgroundResource(R.drawable.button_your_answer);
                        btn_D.setBackgroundResource(R.drawable.button_answer_normal);
                        ((Content_Part_Activity) mContext).setValueReview(index + 151, 3);
                        break;
                    case R.id.btn_D:
                        btn_A.setBackgroundResource(R.drawable.button_answer_normal);
                        btn_B.setBackgroundResource(R.drawable.button_answer_normal);
                        btn_C.setBackgroundResource(R.drawable.button_answer_normal);
                        btn_D.setBackgroundResource(R.drawable.button_your_answer);
                        ((Content_Part_Activity) mContext).setValueReview(index + 151, 4);
                        break;
                }
            }

            if (index == 48) {

                reviewPart = ((Content_Part_Activity) mContext).getReviewPart1();

                if (full == false) {
                    for (int i = 0; i < listquestion_part7.size(); i++) {
                        question_part7 = listquestion_part7.get(i);
                        if (question_part7.getAnswer().equals("A") && reviewPart[i] == 1)
                            answerRight = answerRight + 1;
                        if (question_part7.getAnswer().equals("B") && reviewPart[i] == 2)
                            answerRight = answerRight + 1;
                        if (question_part7.getAnswer().equals("C") && reviewPart[i] == 3)
                            answerRight = answerRight + 1;
                        if (question_part7.getAnswer().equals("D") && reviewPart[i] == 4)
                            answerRight = answerRight + 1;
                    }
                    ((Content_Part_Activity) mContext).showFragmentShowResult(answerRight);

                } else if (full == true) {

                    for (int i = 0; i < listquestion_part7.size(); i++) {
                        question_part7 = listquestion_part7.get(i);
                        if (question_part7.getAnswer().equals("A") && reviewPart[i + 152] == 1)
                            answerRight = answerRight + 1;
                        if (question_part7.getAnswer().equals("B") && reviewPart[i + 152] == 2)
                            answerRight = answerRight + 1;
                        if (question_part7.getAnswer().equals("C") && reviewPart[i + 152] == 3)
                            answerRight = answerRight + 1;
                        if (question_part7.getAnswer().equals("D") && reviewPart[i + 152] == 4)
                            answerRight = answerRight + 1;
                    }
                    ((Content_Part_Activity) mContext).addPoint(answerRight, type);
                    Log.e("Partt7", answerRight + "");

                    // lấy điểm của cả bài thi
                    point = ((Content_Part_Activity) mContext).getPoint();
                    for (int j = 0; j < 4; j++) {
                        pointlistening = pointlistening + point[j];
                        Log.e("diem", point[j] + "");
                    }
                    for (int j = 4; j < 7; j++) {
                        pointreading = pointreading + point[j];
                        Log.e("diem", point[j] + "");
                    }
                    ((Content_Part_Activity) mContext).showResultFullTest(pointlistening, pointreading);


                }
            }
        }

    };

    private void setEvent() {
        btn_A.setOnClickListener(clickAnswer);
        btn_B.setOnClickListener(clickAnswer);
        btn_C.setOnClickListener(clickAnswer);
        btn_D.setOnClickListener(clickAnswer);


    }

    @Override
    public void onResume() {
        super.onResume();

        ((Content_Part_Activity) mContext).setAnswerChoise(this);
    }


    private void setData(int pos) {

        Question_Part7 q0 = listquestion_part7.get(pos - 1);

        String[] a = q0.getQue_en().split("\\+++");
        String title;
        title = "<strong><u>" + a[0] + "</u><strong>" + "<br>" + a[1];
        wv_conversation_en.loadDataWithBaseURL(null, title.trim(), "text/html", "UTF-8", null);

        tv_question.setText(a[2]);
        btn_A.setText(a[3]);
        btn_B.setText(a[4]);
        btn_C.setText(a[5]);
        btn_D.setText(a[6]);


    }


    @Override
    public void setAnswerChoise(int position) {
        int answerChoise = ((Content_Part_Activity) mContext).getValueReview(position);
        switch (answerChoise) {
            case 1: {
                btn_A.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.button_your_answer));
            }
            break;
            case 2: {
                btn_B.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.button_your_answer));
            }
            break;
            case 3: {
                btn_C.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.button_your_answer));
            }
            break;
            case 4: {
                btn_D.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.button_your_answer));
            }
            break;
        }


    }

}

