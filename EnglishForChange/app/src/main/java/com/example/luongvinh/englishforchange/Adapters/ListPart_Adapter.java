package com.example.luongvinh.englishforchange.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.luongvinh.englishforchange.Objects.Part;
import com.example.luongvinh.englishforchange.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by LUONGVINH on 4/4/2017.
 */
public class ListPart_Adapter extends ArrayAdapter<Part> {
    private ViewHolder holder;
    private ArrayList<Part> partArrayList;
    private  Context mcontext;
    public ListPart_Adapter(Context context, int resource, List<Part> partList) {
        super(context, resource, partList);
        this.mcontext=context;
        this.partArrayList= new ArrayList<>(partList);
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;

        holder = new ViewHolder();
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) mcontext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.item_list_part, null);

            holder.imv_icon = (ImageView) view.findViewById(R.id.imv_icon);
            holder.tv_name = (TextView) view.findViewById(R.id.tv_name);
            holder.tv_detail = (TextView) view.findViewById(R.id.tv_detail);
            holder.tv_type = (TextView) view.findViewById(R.id.tv_type);

            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();

        }
        Part part = partArrayList.get(position);
        holder.tv_name.setText(part.getName_part());
        holder.tv_type.setText(part.getType_part());
        holder.tv_detail.setText(part.getDetail_part());
        holder.imv_icon.setImageResource(part.getIcon());
        return view;
    }

    static class ViewHolder {
        ImageView imv_icon;
        TextView tv_name;
        TextView tv_detail;
        TextView tv_type;
    }
}
