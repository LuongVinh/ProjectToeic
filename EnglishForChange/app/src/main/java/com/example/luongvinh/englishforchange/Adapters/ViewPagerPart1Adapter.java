package com.example.luongvinh.englishforchange.Adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.Log;

import com.example.luongvinh.englishforchange.Fragments.Fragment_Part1;
import com.example.luongvinh.englishforchange.Fragments.Fragment_Part3;
import com.example.luongvinh.englishforchange.Fragments.Fragment_Part5;
import com.example.luongvinh.englishforchange.Fragments.Fragment_Part6;
import com.example.luongvinh.englishforchange.Fragments.Fragment_Part7;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by LUONGVINH on 4/27/2017.
 */
public class ViewPagerPart1Adapter extends FragmentStatePagerAdapter {

    private int position;
    private int type;
    private ArrayList<File> listFilePNG;


    public ViewPagerPart1Adapter(FragmentManager fm, int position, int type, ArrayList<File> listFilePNG) {
        super(fm);
        this.position = position;
        this.type = type;
        this.listFilePNG = listFilePNG;
    }

    @Override
    public Fragment getItem(int index) {
        Fragment fragment=null;
        switch (type) {
            case 1:
                fragment = Fragment_Part1.newInstance(this.position, (index + 1), type, listFilePNG.get(index).getPath());
                break;
            case 2:
                fragment = Fragment_Part1.newInstance(this.position, (index + 1), type, null);
                break;
            case 3:
                fragment = Fragment_Part3.newInstance(this.position, (index + 1), type, null);
                break;
            case 4:
                fragment = Fragment_Part3.newInstance(this.position, (index + 1), type, null);
                break;
            case 5:
                fragment = Fragment_Part5.newInstance(this.position, (index + 1), type, null);
                break;
            case 6:
                fragment = Fragment_Part6.newInstance(this.position, (index + 1), type, null);
                break;
            case 7:
                fragment = Fragment_Part7.newInstance(this.position, (index + 1), type, null);
                break;
        }

        return fragment;
    }

    @Override
    public int getCount() {
        switch (type) {
            case 1:
                return 10;

            case 2:
                return 30;

            case 3:
                return 30;

            case 4:
                return 30;

            case 5:
                return 40;

            case 6:
                return 12;


        }
        return 48;
    }

}
